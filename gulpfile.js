var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var minifycss = require( 'gulp-minify-css' );
var gutil = require('gulp-util');
var watch = require('gulp-watch');
var sourcemaps = require('gulp-sourcemaps');
var setStartPath = 'src/content/cf-viiv/viiv-exchange/master/home.html';
var setThemePath = 'src/etc/designs/zg/viiv-exchange/desktop';

gulp.task('default', ['compile-sass', 'serve', 'watch']);
gulp.task('serve', function () {

    browserSync.init({
        server: {
            baseDir: "./"
        }
        , startPath: setStartPath
    });
    gulp.watch(setThemePath + '/sass/**/*.scss', ['sass-watch']);
});

// ... variables
var autoprefixerOptions = {
    browsers: ['last 3 versions', '> 5%', 'Firefox ESR']
};

gulp.task('compile-sass', function () {
    return gulp.src(setThemePath + '/sass/**/*.scss')
        //.pipe(sourcemaps.init())
        .pipe(plumber({errorHandler: onError}))
        .pipe(sass().on('error', function (error) {
            gutil.log('\nline: ' + error.line + '\ncolumn: ' + error.column + '\n' + error.message);
            gutil.log('SASS BUILD FAILED');
        }))
        //.pipe(sourcemaps.write())
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(gulp.dest(setThemePath + '/css')).on('end', function () {
            gutil.log('SASS BUILD FINISHED');
        });
    //.pipe( minifycss())
    //.pipe(gulp.dest(setThemePath + '/css')).on('end', function () {
    //    gutil.log('MINIFICATION CSS BUILD FINISHED');
    //});
});
gulp.task('browser-sync-reload', function () {
    browserSync.reload();
});
gulp.task('watch', function () {
    gulp.watch(setThemePath + '/sass/**/*.scss', ['sass-watch']);
});
gulp.task('sass-watch', ['compile-sass'], function (done) {
    browserSync.reload();
    done();
});
var onError = function (err) {
    gutil.log(gutil.colors.red(err.toString()));
    this.emit('end');
};