/* CF JS - v1.0.0 */
var cf = (function (document, $ , _) {
    'use strict';
    var api = {};

    api.utils = {};
    
    /**
    *** Veeva Country mode implementation
    **/
    
    api.veevaSiteMode = "single"; /* For Single country mode use "single" and For multiple country mode use "multiple" */
    
    api.veevaBuId = "";
    
    api.getAccountInfo = function () {
        if(api.veevaSiteMode == "multiple"){
            gigya.accounts.getAccountInfo({
                callback: api.getAccountInfoResponse
            });
        }        
    };
    
    api.getAccountInfoResponse = function (response) {        
        if(response.errorCode == 0){
            api.veevaBuId = response.data.ADDRESS.COUNTRY; /* Bu Id field from profile needs to be updated here */
        }
    };

    /**
    *** CSRF Token Implementation
    **/
    api.loadCsrfToken = function (cssSelector) {
        $.get(window.location.pathname.split('.')[0]+'.token.json').success(function(json){
            var csrfToken = '';
            if(json.token){
                csrfToken = json.token;
            }

            $(cssSelector).append('<input id="cq_csrf_token" name=":cq_csrf_token" type="hidden" value="'+csrfToken+'"/>');
        });
    };

    $('.userBox .logged-in form').each(function(){
        api.loadCsrfToken('.userBox .logged-in form');
    });

    api.utils.browser = (function () {
        "use strict";
        
        var matched, browser;
        
        var uaMatch = function (ua) {
            ua = ua.toLowerCase();
            
            var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
                /(webkit)[ \/]([\w.]+)/.exec(ua) ||
                    /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
                        /(msie) ([\w.]+)/.exec(ua) ||
                            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
                                [];
            
            return {
                browser: match[1] || "",
                version: match[2] || "0"
            };
        };
        
        matched = uaMatch(navigator.userAgent);
        browser = {};
        
        if (matched.browser) {
            browser[matched.browser] = true;
            browser.version = matched.version;
        }
        
        // Chrome is Webkit, but Webkit is also Safari.
        if (browser.chrome) {
            browser.webkit = true;
        } else if (browser.webkit) {
            browser.safari = true;
        }
        return browser;
    })();

    return api;
}(document, Cog.jQuery(),_));



(function($){
    $('.sso-link .richText-content a').click(function(e){
        e.preventDefault();
        var element = $(this);
        var currentHref = $(this).attr('href');
        $.get(window.location.pathname.split('.')[0]+'.sso.json').success(function(data){
            currentHref+='?data[UID]='+data.UID+'&data[UIDSignature]='+data.UIDSignature+'&data[signatureTimestamp]='+data.signatureTimestamp+'&sso=true';
            window.location.href = currentHref;
        });
    });
    
}(Cog.jQuery()));
