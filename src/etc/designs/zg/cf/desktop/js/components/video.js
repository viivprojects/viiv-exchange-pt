/**
 * Video
 */

(function($, document) {
	"use strict";

	var api = {},
		features = [
			"playpause",
			"progress",
			"current",
			"duration",
			"tracks",
			"volume",
			"fullscreen"
		];

	var eventsToTrack = ["play", "pause", "ended", "progress"],
		progressPercentsToTrack = [25, 50, 75],
        eventHandler,
        progressEventsAttacher,
        createProgressBreakpoints,
        fireVideoEvent,
        dataEventName,
        dataGzgEvent,
		dataGzgscEvent,
        dataBrandName,
        dataBrandId,
		dataTrackingName;

	/**
     * Adds events' listeners to mediaelement
     *
     * @param element {object} mediaelement to add events
     */
    eventHandler = function (element) {
		var	$videocontainer = $(element).parents(".video.component").find('.video-video-container');
        var elementAttrs = {},
            title = $(element).parents(".video.component").find(".video-heading").text(),
            src = element.getAttribute("src"),
            i;
			elementAttrs.dataGzgEvent = $videocontainer.data('gzgevent');
			elementAttrs.dataGzgscEvent = $videocontainer.data('gzgscevent');
			elementAttrs.dataBrandId = $videocontainer.data('brandid');
			elementAttrs.dataTrackingName = $videocontainer.data('trackingname');

        elementAttrs.elementTitle = title;
        elementAttrs.elementSrc = /([^/]+$)/.exec(src)[0]; // All characters after last /


        for (i = 0; i < eventsToTrack.length; i++) {
            if (eventsToTrack[i] === "progress") {
                progressEventsAttacher(element, elementAttrs);
            } else {
                element.addEventListener(eventsToTrack[i], function (e) {
                    fireVideoEvent(elementAttrs, e);
                });
            }
        }
    };

	/**
     * Fires event for tracking purposes
     *
     * @param elementAttrs {object} element attributes to be sent
     * @param e {object} event
     * @param progressValue {string} value of progress
     */
    fireVideoEvent = function (elementAttrs, e, progressValue) {
        var eventType = e.type,
            gzgEvent = elementAttrs.dataGzgEvent,
            gzgscEvent = elementAttrs.dataGzgscEvent,
            brandId = elementAttrs.dataBrandId,
			trackingName = elementAttrs.dataTrackingName;
        if (progressValue) {
            eventType += progressValue;
        }
        if(typeof trackingName != 'undefined' && trackingName != null && trackingName != ''){
            Cog.fireEvent("videoEventTracker", "eventToTrack", _.extend({
                eventType: eventType,
                gzgEvent: gzgEvent,
                gzgscEvent: gzgscEvent,
                brandId: brandId,
                trackingName: trackingName
            }, elementAttrs));
        }
    };

	/**
     * Creates Array of Objects with progress breakpoints
     *
     * @returns progressBreakpoints {array} of objects with progress breakpoints
     */
    createProgressBreakpoints = function () {
        var progressBreakpoints = [],
            i;

        for (i = 0; i < progressPercentsToTrack.length; i++) {

            progressBreakpoints.push({
                progress: progressPercentsToTrack[i]
            });
        }

        return progressBreakpoints;
    };

	/**
     * Adds events listener on progress and send event on specific progress value
     *
     * @param element {object} DOM element of video
     * @param elementAttrs {object} element attributes to be sent
     */
    progressEventsAttacher = function (element, elementAttrs) {
        var progressBreakpoints = createProgressBreakpoints();

        element.addEventListener("timeupdate", function (e) {
            var videoPlayed = parseInt(((element.currentTime / element.duration) * 100)),
                i;

            // loop through all progress breakpoints
            // check if played progress is reached and if isWatched flag set
            for (i = 0; i < progressBreakpoints.length; i++) {

                if (videoPlayed > progressBreakpoints[i].progress) {
                    fireVideoEvent(elementAttrs, e, progressBreakpoints[i].progress);
                    progressBreakpoints.splice(i, 1);
                }
            }

        });

    };

	function Video($el) {
		this.$el = $el;
		this.$video = $el.find("video");
		var $videocontainer = $el.find(".video-video-container");
		dataGzgEvent = $videocontainer.data('gzgevent');
		dataGzgscEvent = $videocontainer.data('gzgscevent');
        dataBrandId = $videocontainer.data('brandid');
		dataTrackingName = $videocontainer.data('trackingname');

		this.initialize();
		this.bindEvents();
	}

	Video.prototype = {
		initialize: function() {
			this.$video.mediaelementplayer({
				features: features,
				enableAutosize: false,
				plugins: ["flash", "youtube"],
				pluginPath: api.external.settings.themePath + "/assets/swf/",
				flashName: "flashmediaelement.swf",
				videoHeight: this.$video.attr("height"),
				success: this.onMejsSuccess.bind(this)
			});
		},

		bindEvents: function() {
			Cog.addListener("overlay", "close", this.onOverlayClose, {scope: this, disposable: true});
		},

		onOverlayClose: function() {
			if (this.$el.parents(".overlay-container").size()) {
				this.mediaElement.stop();
				this.mediaElement.remove();
				Cog.finalize(this.$el);
			}
		},

		onMejsSuccess: function(mediaElement) {
			this.mediaElement = mediaElement;
			eventHandler(mediaElement);
		}
	};

	api.onRegister = function(scope) {
		new Video(scope.$scope);
	};

	Cog.registerComponent({
		name: "videoplayer",
		api: api,
		selector: ".video",
		requires: [
			{
				name: "utils.settings",
				apiId: "settings"
			}
		]
	});
})(Cog.jQuery());
