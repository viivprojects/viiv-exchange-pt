(function ($, _) {
	'use strict';
	var api = {};

	api.onRegister = function ($loadMore) {
		$loadMore.$scope.each(function () {
			var $submit = $(this).find(".loadMore-button"),
				$response = $(this).find(".ajaxResponse"),
				$clear = $(this).find(".loadMore-clear");
			if ($submit && $submit.length > 0) {
				$submit.on("click", function(){
					if ($submit.data("active")) {
						$.ajax({
							type : 'GET',
							dataType : 'html',
							url : $submit.data("path") + "?t=" + Date.now() + '&clear=' + $clear.val(),
							data : {
	
							},
							success : function(response) {
								$clear.val("false")
								$response.append(response);
							}
						})
					} else {
						alert("Load more works only in WCM DISABLED mode");
					}
				});
			}

			$loadMore.addClass('initialized');
		});

	};

Cog.registerComponent({
    name: "loadMore",
    selector: ".component.loadMore",
    api: api
});

})(Cog.jQuery(),_);