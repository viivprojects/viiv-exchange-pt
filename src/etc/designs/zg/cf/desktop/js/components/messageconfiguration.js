(function($) {
    var api = {}, dataMessage="";  
    api.onRegister = function(elements) { 
	    var $messageconfiguration = elements.$scope;
	    $messageconfiguration.each(function() {
            $(this).on("click", ".sendMessage", api.onSendMsgrClick); 
            $(this).on("change", ".messagaeConfigurationContent #messageModule", api.onShowMessage); 
        }); 
    };

  	api.onSendMsgrClick = function (event, triggered) {
  		var content = $(".messagaeConfigurationContent");         
  		var configPath = content.data("pagePath");
        var messageSender = content.data("adminId");
        var successMsg = content.data("successMessage");
        var failureMsg = content.data("failureMessage");
        var entityName = content.data("entityName"); 
        var categoryCode = content.data("categoryCode"); 
		var messageRecipient = $('.messageconfiguration #messageRecipient').val();
        var messageModule = $('.messageconfiguration #messageModule').val(); 

		$.ajax({
			type: 'POST',
            url: configPath,
            data: {"senderId":messageSender,"recipientId":String(messageRecipient),"messageId":messageModule, "selector":"create", "entityName":entityName, "categoryCode":categoryCode},
            success: function(data) {  
                var messageTextResponse=""; 
                if(data == "true"){ 
                     messageTextResponse="<p class='msgResSuccess'>"+successMsg+"</p>";
                } else{ 
					 messageTextResponse="<p class='msgResFailure'>"+failureMsg+"</p>";
                }
				$('.messageconfiguration .responseMessageText').html(messageTextResponse);
            },
            dataType: 'html'
        });
	 };
	    api.onShowMessage = function (event, triggered) {   		 
		 var moduleID = $(this).val();  
         var content = $(".messagaeConfigurationContent");         
         var emailTemp = content.data("emailTemplate")+"/"+moduleID+"/_jcr_content/template.html";  
            if(moduleID != "pleaseselect"){ 
				$('.messageconfiguration .sendMessage').removeAttr("disabled");
            } else{
				$('.messageconfiguration .sendMessage').attr("disabled","disabled");
                $('.messageconfiguration .emailTemplateContainer').html("");
            }   
            $.ajax({
				url: emailTemp,
                data:{},
				success: function(data) {     
					dataMessage = data;
					$('.messageconfiguration .emailTemplateContainer').html(data);		
				},
				dataType: 'html'
       		}); 
        };

	Cog.registerComponent({
	    name: "messageconfiguration",
	    api: api,
	    selector: ".messageconfiguration"	
	});
	
})(Cog.jQuery());
