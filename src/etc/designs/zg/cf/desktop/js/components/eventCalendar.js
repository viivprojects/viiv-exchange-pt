(function($) {
    var api = {};
    var k;
    var nEvent;
    var categoryMap = {};
    var venueMap = {};
    var typeMap = {};
    var calendarDisplay;
    var categoryLabel;
    var locationLabel;
    var typeLabel;
    var validationFlag = true;
    var resultsError;

    function dateFormatChange(dateString) {
        var newDate = dateString.replace(/-/g, ',');
        var date = new Date(newDate);
        var curr_date = ("0" + date.getDate()).slice(-2)
        var curr_month = ("0" + (date.getMonth() + 1)).slice(-2)
        var curr_year = date.getFullYear();
        var startDate = curr_year + "-" + curr_month + "-" + curr_date;
        return startDate;
    }

    api.onRegister = function(elements) {


        var $eventCalendar = elements.$scope;
        if ($eventCalendar.length > 0) {
            var content = $(".eventCalendarBox");
            var configPath = content.data("pagePath");
            calendarDisplay = content.data("calendarDisplay");
            categoryLabel = content.data("categoryLabel");
            locationLabel = content.data("locationLabel");
            typeLabel = content.data("typeLabel");
            resultsError = content.data("resultsError");
            locale = content.data("localeValue");

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: configPath,
				data : {'eventLocaleCode':locale,'entityName':'eventCalendar'},
                success: function(data) {
                    var jsonString = JSON.stringify(data);
                    var obj = $.parseJSON(jsonString);
                    var myArray = [];
                    var i = 0;
                    $(obj).each(function(i, response) {

                        var eventCode = $(this)[0].eventCode;
                        var startDate = $(this)[0].A_EVNT_SDATE;
                        var endDate = $(this)[0].A_EVNT_EDATE;
                        var myObject = new Object();
                        myObject.id = eventCode;
                        myObject.type = $(this)[0].A_EVNT_TYPE;
                        myObject.title = $(this)[0].A_EVNT_NAME;
                        myObject.start = dateFormatChange(startDate);
                        myObject.end = dateFormatChange(endDate);
                        myObject.location = $(this)[0].A_EVNT_VENUE;
                        myObject.category = $(this)[0].A_EVNT_THERAPY;
                        myObject.website = $(this)[0].A_EVNT_MKTURL;
                        myObject.frequency = $(this)[0].A_EVNT_SCH_FREQ;
                        myObject.status = $(this)[0].A_STATUS;
                        myObject.address = $(this)[0].A_EVNT_LOC_ADDR;
                        myObject.sponsor = $(this)[0].A_EVNT_SPONSOR;
                        myObject.agenda = $(this)[0].A_DMS_AGENDA;
                        myObject.addinfo = $(this)[0].A_DMS_ADDINFO;
						myObject.isRegistered = $(this)[0].A_EVNT_ISSUB;

                        var isSub = $(this)[0].A_EVNT_ISSUB;
                        if (isSub === "Yes") {
                            myObject.color = "#04B431";
                        } else {
                            myObject.color = "#f36633";
                        }
                        myArray.push(myObject);
                        i = i + 1;

                    });
                    nEvent = myArray;

                    if (calendarDisplay) {
                        $('#eventCalendar').fullCalendar({
                            theme: true,
                            header: {
                                left: 'prev',
                                center: 'title',
                                right: 'next'
                            },
                            events: nEvent
                        });
                    }

                    $(".filterByCategory").append(
                        '<option value="NULL">' + categoryLabel + '</option>');
                    $(".filterByLocation").append(
                        '<option value="NULL">' + locationLabel + '</option>');
                    $(".filterByType").append(
                        '<option value="NULL">' + typeLabel + '</option>');

                    $(nEvent).each(function(i, response) {
                        categoryMap[response.category] = response.category;
                        venueMap[response.location] = response.location;
                        typeMap[response.type] = response.type;
                    });

                    $.each(categoryMap, function(key, value) {
                        $(".filterByCategory").append(
                            '<option value="' + key + '">' + value + '</option>');
                    });
                    $.each(venueMap, function(key, value) {
                        if (!(typeof value === "undefined")) {
                            $(".filterByLocation").append(
                                '<option value="' + key + '">' + value + '</option>');
                        }
                    });
                    $.each(typeMap, function(key, value) {
                        $(".filterByType").append(
                            '<option value="' + key + '">' + value + '</option>');
                    });
                }
            });

            $(".eventCalendarSubmit").on("click", api.onSearchSubmitClick);
            $(".eventCalendarClear").on("click", api.onSearchClearClick);
            $(".selectStartDate").datepicker();
            $(".selectEndDate").datepicker();
            var listTemplate = $(".eventListTemplate").html(),
                output = "";
            _.templateSettings = {
                interpolate: /\{\{(.+?)\}\}/g,
                evaluate: /\{\%(.+?)\%\}/g
            };
            _.forEach(nEvent, function(event) {

                output += _.template(listTemplate, event);
            });

            $('.eventResultContainer').html(output);
        }
    };
    api.onValidationFailure = function(errorMessage) {
        $('.eventCalendarSearchSection').prepend(
            "<p class='errorText'>" + errorMessage + "</p>");
        validationFlag = false;

    };
    api.onSearchSubmitClick = function(event, triggered) {
        $('.searchResultContainer').html('');
        validationFlag = true;
        $('.event-results-label').hide();
        if (calendarDisplay) {
            $('#eventCalendar').fullCalendar("removeEvents");
        }

        $('.eventCalendarSearchSection p.errorText').html(' ');
        var selectedCategory, selectedLocation, eventStartDate, eventEndDate, newStartDate, newEndDate, sortBy, selectedType;
        // ZGjQuery('#eventCalendar').fullCalendar("addEventSource",k);
        sortBy = $('option:selected', '.sortByParam').val();
        selectedCategory = $('option:selected', '.filterByCategory')
            .val();
        selectedLocation = $('option:selected', '.filterByLocation')
            .val();
        selectedType = $('option:selected', '.filterByType')
            .val();
        eventStartDate = $('.selectStartDate').val();
        eventEndDate = $('.selectEndDate').val();
        newStartDate = new Date(eventStartDate);
        newEndDate = new Date(eventEndDate);
        var validationMsg;
        if (selectedCategory === "NULL" && selectedLocation === "NULL" && selectedType === "NULL" && eventStartDate === "" && eventEndDate === "") {
            validationMsg = $(".eventCalendarBox").attr("data-validationError");
            api.onValidationFailure.call(self, validationMsg);
            event.stopImmediatePropagation();
        }
        if (eventStartDate === "" && eventEndDate !== "") {
            validationMsg = $(".eventCalendarBox").attr("data-startDateError");
            api.onValidationFailure.call(self, validationMsg);
            event.stopImmediatePropagation();
        }
        if (eventStartDate !== "" && eventEndDate === "") {
            validationMsg = $(".eventCalendarBox").attr("data-endDateError");
            api.onValidationFailure.call(self, validationMsg);
            event.stopImmediatePropagation();
        }
        if (newEndDate < newStartDate) {
            validationMsg = $(".eventCalendarBox").attr("data-dateError");
            api.onValidationFailure.call(self, validationMsg);
            event.stopImmediatePropagation();
        }

        $.each(nEvent,
            function(idx, obj) {
                var eventDate = obj.start;
                var venue = obj.location;
                var category = obj.category;
                var type = obj.type;
                var newEventDate = new Date(eventDate);
                nEvent[idx]["display"] = "no";

                if ((newEventDate > newStartDate && newEventDate < newEndDate) && (selectedCategory === category) && (selectedLocation === venue) && (selectedType === type)) {

                    nEvent[idx]["display"] = "yes";
                }
                if ((selectedCategory === "NULL") && (selectedLocation === "NULL") && (selectedType === "NULL") && (newEventDate > newStartDate && newEventDate < newEndDate)) {

                    nEvent[idx]["display"] = "yes";
                }
                if ((selectedCategory === "NULL") && (selectedLocation === venue) && (selectedType === "NULL") && (eventStartDate === "") && (eventEndDate === "")) {

                    nEvent[idx]["display"] = "yes";
                }
                if ((selectedCategory === category) && (selectedLocation === "NULL") && (selectedType === "NULL") && (eventStartDate === "") && (eventEndDate === "")) {

                    nEvent[idx]["display"] = "yes";
                }

                if ((selectedCategory === "NULL") && (selectedLocation === "NULL") && (selectedType === type) && (eventStartDate === "") && (eventEndDate === "")) {

                    nEvent[idx]["display"] = "yes";
                }

                if ((selectedCategory === "NULL") && (selectedLocation === venue) && (selectedType === "NULL") && (newEventDate > newStartDate && newEventDate < newEndDate)) {

                    nEvent[idx]["display"] = "yes";

                }

                if ((selectedCategory === category) && (selectedLocation === "NULL") && (selectedType === "NULL") && (newEventDate > newStartDate && newEventDate < newEndDate)) {

                    nEvent[idx]["display"] = "yes";
                }
                if ((selectedCategory === "NULL") && (selectedLocation === "NULL") && (selectedType === type) && (newEventDate > newStartDate && newEventDate < newEndDate)) {

                    nEvent[idx]["display"] = "yes";
                }
                if (selectedCategory === category && selectedLocation === venue && selectedType === type) {
                    nEvent[idx]["display"] = "yes";
                }
                if (selectedCategory === category && selectedLocation === "NULL" && selectedType === type) {
                    nEvent[idx]["display"] = "yes";
                }
                if (selectedCategory === "NULL" && selectedLocation === venue && selectedType === type) {
                    nEvent[idx]["display"] = "yes";
                }
                if (selectedCategory === category && selectedLocation === venue && selectedType === "NULL") {
                    nEvent[idx]["display"] = "yes";
                }
            });
        k = nEvent;
        if (sortBy === "Alphabetically") {
            k.sort(SortByName);
        } else {
            k.sort(SortByDate);
        }
        if (calendarDisplay) {
            $('#eventCalendar').fullCalendar("addEventSource", k);
        }
        var template = $(".eventCalendarTemplate"),
            output = "";
        var clonedTemplate = template.clone();

        _.templateSettings = {
            interpolate: /\{\{(.+?)\}\}/g,
            evaluate: /\{\%(.+?)\%\}/g
        };
        var hasResult = false;
        _.forEach(k, function(event) {
            if (event.display === "yes") {
                hasResult = true;
                for (var key in event) {
                    if (event.hasOwnProperty(key)) {
                        if (clonedTemplate.find('div[dmp-data="' + key + '"]').length > 0) {

                            if (event[key] == null || event[key].length === 0) {

                                clonedTemplate = clonedTemplate.find(
                                    'div[dmp-data="' + key + '"]').html('').parents('.eventCalendarTemplate:eq(0)');
                            }

                            clonedTemplate = clonedTemplate.find('h4').css('color', event.color).parents('.eventCalendarTemplate:eq(0)');
                        }
					}
				}
            $('.event-results-label').show();
                if(event.isRegistered === "Yes"){                	
                	clonedTemplate.find('.eventDetails .unsubscribe').show();
                	clonedTemplate.find('.eventDetails .register').hide();
                }
                else if(event.isRegistered === "No"){                	
                	clonedTemplate.find('.eventDetails .register').show();
                	clonedTemplate.find('.eventDetails .unsubscribe').hide();
                } 
                output += _.template(clonedTemplate.html(), event);

            } else {
                if (calendarDisplay) {
                    $('#eventCalendar').fullCalendar('removeEvents',
                        event.id);
                }
            }

        });
        if (validationFlag && output === "") {
            output = resultsError;
        }
        $('.searchResultContainer').html(output);
    };

    function SortByName(x, y) {
        return ((x.title === y.title) ? 0 : ((x.title > y.title) ? 1 : -1));
    }

    function SortByDate(x, y) {
        return ((x.start === y.start) ? 0 : ((x.start > y.start) ? 1 : -1));
    }

    api.onSearchClearClick = function(event, triggered) {
        $("input[type=text]").val("");
        $(".filterByCategory").val("");
        $(".filterByLocation").val("");
        $(".filterByType").val("");
        $('.searchResultContainer').html('');
        $('.event-results-label').hide();
    };

    Cog.registerComponent({
        name: "eventCalendar",
        api: api,
        selector: ".eventCalendar"
    });

})(Cog.jQuery());