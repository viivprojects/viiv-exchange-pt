(function($) {
	var api = {};
	api.onRegister = function(elements) {
		var portalId = $('.eventDetailsData').attr("data-portal-id");
		//if (elements.$scope.length > 0) {
			var content = $(".eventDetailsData"), includeRelatedEvents="", includePastRelatedEvents="";
			var configPath = content.data("pagePath");
			var eventId = content.data("eventId");
			var relatedEvents = content.data("relatedEvents");
			var pastEvents = content.data("pastEvents");
			var tagRequirement = content.data("tagRequirement");
			var pathname = window.location.pathname;
			pathname=pathname.replace('.html','');

			if(relatedEvents)
			{
            includeRelatedEvents = relatedEvents;
			}
			if(pastEvents)
			{
            includePastRelatedEvents = pastEvents;
			}
			if(tagRequirement === "")
	        {
	        tagRequirement = "any";
	        }    
			
            $.ajax({
				type : 'GET',
				url : pathname+'.sso.json',
                data : {},
				success : function(data) {
                $.ajax(
                        {
                         type : 'GET',
				         url : configPath+'.adobeconnectlist.json',

                         data : {"sco-id" : eventId, "portal-id": portalId, UID:data.UID, UIDSignature:data.UIDSignature,signatureTimestamp:data.signatureTimestamp, "include-related-events": includeRelatedEvents ,"include-past-related-events": includePastRelatedEvents},
                             success : function(data) {

					Cog.component.eventdetails.updateEventDetailsTemplate(JSON.stringify(data));
				},
				dataType : 'json'

                        }
                    );


				},
                    error : function(data){
                         $.ajax(
                        {
                         type : 'GET',
				         url : configPath+'.adobeconnectlist.json',

                         data : {"sco-id" : eventId, "portal-id": portalId, "include-related-events": includeRelatedEvents ,"include-past-related-events": includePastRelatedEvents},	
                         
                             success : function(data) {

					Cog.component.eventdetails.updateEventDetailsTemplate(JSON.stringify(data));
				},
				dataType : 'json'

                        }
                    );


                    },
				dataType : 'json'
			});
		//}

	};

    Cog.registerComponent({
        name: "eventcontainer",
        api: api,
        selector: ".eventcontainer"
    });

})(Cog.jQuery());
