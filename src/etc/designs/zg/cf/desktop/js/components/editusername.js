(function($) {
    var api = {}; 
    api.onRegister = function(elements) { 
    	var $editusername = elements.$scope;
        $editusername.each(function() {
          $(this).on("click", "button", api.onEditUsernamesubmitClick);
          $(this).on("load", cf.loadCsrfToken('.editUserNameForm.form'));
        }); 
    };
    api.onEditUsernamesubmitClick = function(event, triggered) {
         var newUserName = $(".editUserNameForm #editUserName").val();    	
         var currentPassword = $(".editUserNameForm #editUserPassword").val();
         var csrfToken = $(".editUserNameForm #cq_csrf_token").val();
		 
		 if(newUserName === "" || currentPassword === "") {
            $(".editUserNameFormFailure").show();
			$(".editUserNameFormFailure p").html("Please enter username and password.");
         }
		 else {
			 var resPath = $(".editUserNameForm").attr("data-res-path"); 
			 $(".editUserNameFormFailure p").html("");
			 $(".editUserNameFormSuccess").hide();

			 $.ajax({
				url: resPath,
				type:"POST",
				data: {"newusername":newUserName,"currentpassword":currentPassword,":cq_csrf_token":csrfToken},
				success: function(data){ 
				var status=JSON.stringify(data); 
				if(status === "true")
					{
					 $(".editUserNameFormSuccess").show();
					}else{
						var errorCode=data.generalErrors[0].code;
						var errorDesc=$(".editUserNameForm").attr("data-code-error-"+errorCode); 
						$(".editUserNameFormFailure").show();
						$(".editUserNameFormFailure p").append(errorDesc);
					}
				},
				dataType: 'json'
			 });
		 }
    };  
   

Cog.registerComponent({
    name: "editusername",
    api: api,
    selector: ".editusername"
});
 })(Cog.jQuery());
