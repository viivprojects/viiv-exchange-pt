/**
 * Rating and Reviews
 */

(function ($) {
    "use strict";

    var api = {};
    api.init = function(element) {

        var streamId = "";
        var $ratingsandreviews = element;
        var content = $ratingsandreviews.find(".reviewPageDetails");
        var pageTitle = content.data("pageTitle");
        var pageDescription = content.data("pageDescription");
        var pagePath = content.data("pagepath");
        streamId = content.data("streamId");
        var providers = $ratingsandreviews.find("#gigyaReviewsDiv").closest(".reviewPageDetails").attr("data-social-providers");
        var categoryId = content.data("categoryId");

        var apiKey = content.data("apiKey");
        var imagePath = content.data("imagePath");
        var gigyaUrl = content.data("gigyaUrl");
        if (streamId != null) { 
            var conf = {
                APIKey: apiKey,
                enabledProviders: providers
            } 
            var image = {
                type: "image",
                src: imagePath,
                href: pagePath
            }
            var action = new gigya.socialize.UserAction();
            action.setLinkBack(pagePath);
            action.addActionLink(pagePath, pagePath);
            action.setTitle(pageTitle);
            action.setDescription(pageDescription);
            action.addMediaItem(image); 

            var reviewParams = {
                categoryID: categoryId,
                streamID: streamId,
                version:2,
                containerID: 'gigyaReviewsDiv'
            }
            gigya.comments.showCommentsUI(conf,reviewParams);
            var ratingParams = {
                categoryID: categoryId,
                streamID: streamId,
                containerID: 'gigyaRatingDiv', 
                linkedCommentsUI: 'gigyaReviewsDiv'
            }
            gigya.comments.showRatingUI(conf,ratingParams); 
        }
    };


Cog.register({
    name: "ratingsandreviews",
    api: api,
    selector: ".ratingsandreviews"
});

        })(Cog.jQuery());