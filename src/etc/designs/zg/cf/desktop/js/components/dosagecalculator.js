(function($) {
	
    var api = {}; 
    var lang = $('html').attr('lang');
    api.onRegister = function(element) {

	    var $dosageCalculator = element.$scope;
	    $dosageCalculator.each(function() {
            $(this).on("click", ".dosageCalculatorInput button", api.onDosagesubmitClick);
        });

        $(".dosageOptionGroup input").keypress(function (e) { 
            if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {       
                               return false;
            }
        });

      	$(".dosageDatepicker").datepicker();

        $.datepicker.setDefaults( $.datepicker.regional[ "" ] ); 
    	$(".dosageDatepicker").datepicker($.datepicker.regional[lang]); 
    	 
    	api.hideDosageRecomendation.call(self); 
    }; 
    api.onDosagesubmitClick = function(event, triggered) {
		showResult = new Boolean(true);
        $(".dosageCalculatorVBox").html("");
		api.hideDosageRecomendation.call(self); 
        showResult = api.dosageValidator.call(self); 
        if (showResult === true) {
            api.dosageResultCalculation.call(self);
        }
    }; 
	api.hideDosageRecomendation= function(event, triggered){
		$(".dosageCalculatorResultsContainer .dosageCalculatorRecommend").each(function(index) {
			$(this).hide();
			$(this).attr("data-match","Success-values:");
		});
		$(".dosageOptionGroup .dosageError").each(function(index) {
            $(this).remove();  
		});

	};
    api.dosageResultCalculation = function(event, triggered) {  
    	var inputCount=  $(".dosageCalculatorInput").data("countValue");
    	$(".dosageCalculatorResultsContainer .dosageNoResults").remove();	
        var fieldDataLabel,fieldVal, inputItem, dosageInputArray = []; 
        var currentDate = new Date(), resultFound= new Boolean(false),count=0; 
		 $(".dosageCalculatorInput input").each(function(index) {
	 		fieldDataLabel=$(this).attr("data-filter-label"); 
	 		fieldVal= $(this).val(); 
			inputItem=fieldDataLabel; 
			if(fieldDataLabel === "month"){
				dosageBirthDate = new Date(fieldVal); 
				$inputItem = (currentDate.getFullYear() - dosageBirthDate.getFullYear()) * 12 
				+ (currentDate.getMonth() - 			dosageBirthDate.getMonth()); 
        		$inputItem = parseInt($inputItem); 
			}
			else{
				$inputItem=parseInt(fieldVal);
			} 
			dosageInputArray.push({inputLabel:inputItem,inputValue:$inputItem}); 
		 }); 
 		$.each(dosageInputArray, function( index, value ) { 
			$(".dosageCalculatorResultsContainer .dosageCalculatorRecommend").each(function(indexNew) {  
				var minValueLabel="data-min-"+value.inputLabel; 
				var minVal=parseInt($(this).attr(minValueLabel)); 
				var maxValueLabel="data-max-"+value.inputLabel; 
				var maxVal=parseInt($(this).attr(maxValueLabel)); 
				if((value.inputValue>=minVal)&&(value.inputValue<=maxVal)){ 
					var inputData=$(this).attr("data-match"); 
					  $(this).attr("data-match",inputData+","+value.inputLabel); 
				} 
			});
		}); 
 		
 		
	    
		$(".dosageCalculatorResultsContainer .dosageCalculatorRecommend").each(function(index) {
			count = $(this).attr("data-match").split(',').length;
			if(count === inputCount){
				$(this).show();
			    resultFound=true;
			}
		});  
		if(resultFound === false){
			$("#default").show();
		}
    };
    api.dosageValidator = function(event, triggered) {
        $(".dosageCalculatorInput input").each(function(index) {
            var dosageInput = $(this).val(); 
            $(this).removeClass("dosageCalculatorVFail");
            if (dosageInput === "") {
                $(this).addClass("dosageCalculatorVFail");
                $(this).parent().after("<p class='dosageError'>"+$(".dosageCalculatorInput").data("errorMessage")+"</p>");
                showResult = false;
            }  
			else{
			 $(this).parent().after("<p class='dosageError'>&nbsp;</p>");
			}
        });
		if(showResult){
			 $(".dosageOptionGroup p.dosageError").each(function(index) { 
				 $(this).remove();
			 });
		}
        return showResult;
    };

Cog.registerComponent({
    name: "dosageCalculator",
    api: api,
    selector: ".dosageCalculator"
});
})(Cog.jQuery());
