/**
* Video Gallery
*/

(function ($) {
    "use strict";

    var api = {},
        eventsToTrack = ["play", "pause", "ended", "progress"],
        progressPercentsToTrack = [25, 50, 75],
        eventHandler,
        progressEventsAttacher,
        createProgressBreakpoints,
        fireVideoEvent,
        dataEventName,
        dataGzgEvent,
        dataGzgscEvent,
        dataBrandName,
        dataBrandId,
        dataTrackingName,
        refs = {
            componentName: "videoGallery",
            thumbnailsContainerSelector: ".videoGallery-thumbnails",
            thumbnailsListContainerSelector: ".videoGallery-list",
            thumbnailsArrowsSelector: ".videoGallery-arrow",
            descriptionSelector: ".videoGallery-description",
            titleSelector: ".videoGallery-heading",
            videoWrapper: ".videoGallery-video"
        };

    /**
    * Adds events' listeners to mediaelement
    *
    * @param element {object} mediaelement to add events
    */
    eventHandler = function (element) {

        var	$videocontainer = $(element).parents(".videoGallery").find('.videoGallery-video');
        var elementAttrs = {},
            title = $(element).parents(".video.component").find(".video-heading").text(),
            src = element.getAttribute("src"),
            i;

        elementAttrs.elementTitle = title;
        elementAttrs.elementSrc = /([^/]+$)/.exec(src)[0]; // All characters after last /

        
        for (i = 0; i < eventsToTrack.length; i++) {

            if (eventsToTrack[i] === "progress") {
                progressEventsAttacher(element, elementAttrs);
            } else {

                element.addEventListener(eventsToTrack[i], function (e) {

                    fireVideoEvent(element,elementAttrs, e);
                });
            }
        }
    };
    
    /**
    * Fires event for tracking purposes
    *
    * @param elementAttrs {object} element attributes to be sent
    * @param e {object} event
    * @param progressValue {string} value of progress
    */
    fireVideoEvent = function (element,elementAttrs, e, progressValue) {

		var	$videocontainer = $(element).parents(".videoGallery").find('.videoGallery-video');

		elementAttrs.dataGzgEvent = $videocontainer.data('gzgevent');
        elementAttrs.dataGzgscEvent = $videocontainer.data('gzgscevent');
        elementAttrs.dataBrandId = $videocontainer.data('brandid');
        elementAttrs.dataTrackingName = $videocontainer.data('trackingname');

        var eventType = e.type,
            gzgEvent = elementAttrs.dataGzgEvent,
            gzgscEvent = elementAttrs.dataGzgscEvent,
            brandId = elementAttrs.dataBrandId,
            trackingName = elementAttrs.dataTrackingName;

        if (progressValue) {
            eventType += progressValue;
        }
		
        	Cog.fireEvent("videoEventTracker", "eventToTrack", _.extend({
            eventType: eventType,
            gzgEvent: gzgEvent,
            gzgscEvent: gzgscEvent,
            brandId: brandId,
            trackingName: trackingName
        }, elementAttrs));
    };

    /**
    * Creates Array of Objects with progress breakpoints
    *
    * @returns progressBreakpoints {array} of objects with progress breakpoints
    */
    createProgressBreakpoints = function () {

        var progressBreakpoints = [],
            i;
        
        for (i = 0; i < progressPercentsToTrack.length; i++) {
            
            progressBreakpoints.push({
                progress: progressPercentsToTrack[i]
            });
        }

        return progressBreakpoints;
    };

    /**
    * Adds events listener on progress and send event on specific progress value
    *
    * @param element {object} DOM element of video
    * @param elementAttrs {object} element attributes to be sent
    */
    progressEventsAttacher = function (element, elementAttrs) {
        var progressBreakpoints = createProgressBreakpoints();

        element.addEventListener("timeupdate", function (e) {
            var videoPlayed = parseInt(((element.currentTime / element.duration) * 100)),
                i;
            
            // loop through all progress breakpoints
            // check if played progress is reached and if isWatched flag set
            for (i = 0; i < progressBreakpoints.length; i++) {
                
                if (videoPlayed > progressBreakpoints[i].progress) {
                    fireVideoEvent(elementAttrs, e, progressBreakpoints[i].progress);
                    progressBreakpoints.splice(i, 1);
                }
            } 
        });        
    };

    api.onRegister = function (scope) {
        var $this = scope.$scope,
            $video = $this.find("video"),
            $videoplayer = $(this),
           	$videocontainer = $this.find(".videoGallery-video"),
            $thumbnailsContainer = $this.find(refs.thumbnailsContainerSelector),
            $thumbnailsListContainer =
            $thumbnailsContainer.find(refs.thumbnailsListContainerSelector),
            $arrows = $this.find(refs.thumbnailsArrowsSelector),
            listContainerWidth = $thumbnailsListContainer.width(),
            $descriptionContainer = $this.find(refs.descriptionSelector),
            $titleContainer = $this.find(refs.titleSelector),
            $thumbnailsList = $thumbnailsListContainer.find("ul"),
            $thumbnailsItems = $thumbnailsList.find("li"),
            thumbnailWidth = $thumbnailsItems.eq(0).outerWidth(true),
            listOuterWidth = $thumbnailsItems.length * thumbnailWidth,
            autoPlayNext = $this.find(refs.videoWrapper).data("autoplay-next-video"),
            gzgevent = $this.find(refs.videoWrapper).data("gzgevent");
        
			//set width od container based of amount of thumbnails
			$thumbnailsList.css("width", listOuterWidth);

			dataGzgEvent = $videocontainer.data('gzgevent');
			dataGzgscEvent = $videocontainer.data('gzgscevent');
			dataBrandId = $videocontainer.data('brandid');
			dataTrackingName = $videocontainer.data('trackingname');

        new MediaElementPlayer($video[0], {
            features: ["playpause", "progress", "current",
                       "duration", "tracks", "volume", "fullscreen"],
            enableAutosize: true,
            //enableAutosize: false,
            //hideVideoControlsOnLoad: true,
            //videoHeight: $video.attr("height"),
            plugins: ["flash","youtube"],
            pluginPath: api.external.settings.themePath + "/assets/swf/",
            flashName: "flashmediaelement.swf",
            success: function (mediaElement) {
			var group=$(mediaElement).parents(".videoGallery").find('.videoGallery-video').data("group");

            function reloadYoutube(data) {
				$titleContainer.text(data.title);
            	$descriptionContainer.text(data.description);
				var videoUrl=data.src[0].src;
            	var videoId = videoUrl.substr(videoUrl.length-11,Math.max(videoUrl.length,11));
           	 	mediaElement.pluginApi.loadVideoById(videoId);            	
				mediaElement.pluginApi.stopVideo();
        	}
            //set and load new source to player
            function reloadVideo(data) {
            data.mediaElement.pause();

            //change video, base on current format
            data.mediaElement.setSrc(data.src);
            data.mediaElement.load();
            
            if (data.poster) {
            var $mediaElement = $(data.mediaElement);
            $mediaElement.attr("poster", data.poster);
            $mediaElement.parents(".mejs-inner").find(".mejs-poster").css({
            "background-image": "url('" + data.poster + "')",
            "display": "block"
        });
    }
    data.mediaElement.pause();
    if (data.autoplay) {
        data.mediaElement.play();
    }
    
    //change texts of title and description
    $descriptionContainer.text(data.description);
    $titleContainer.text(data.title);
}
 
 //on click at thumbnail, load new video
 $thumbnailsItems.on("click", function () {
    
    var $this = $(this),
        src = provideArrayOfSources($this),
        title = $this.data("title"),
        description = $this.data("description"),
        poster = $this.find("img").attr("src");
    if(mediaElement.pluginType == "youtube")//if the video is youtube video
    {      
        $titleContainer.text(title);
        $descriptionContainer.text(description);
        var videoUrl=src[0].src;
        var videoId = videoUrl.substr(videoUrl.length-11,Math.max(videoUrl.length,11));
        mediaElement.pluginApi.loadVideoById(videoId);
    }
    else// if the video is native video(mp4/webm/...)
    {        
		reloadVideo({
			mediaElement: mediaElement,
			src: src, 
			autoplay: true,
			title: title,
			poster: poster,
			description: description
		});
    }
    //change active state to clicked thumbnail
    $thumbnailsItems.removeClass("is-active");
    $this.addClass("is-active");
});

//after finished video, play the next one from thumbnails list

mediaElement.addEventListener("ended", function () {

    //get next video data
    var $thisThumbnail = $thumbnailsItems.filter(".is-active").eq(0),
        $nextThumbnail = $thumbnailsItems.filter(".is-active").eq(0).next(),
        title = $nextThumbnail.data("title"),
        description = $nextThumbnail.data("description"),
        poster = $nextThumbnail.find("img").attr("src"),
	    brandid= $thisThumbnail.data("brandid"),
        trackingname= $thisThumbnail.data("trackingname"),
        gzgevent= $thisThumbnail.data("gzgevent"),
        gzgscevent= $thisThumbnail.data("gzgscevent");
		if(group=="cf")
		{ 
			$(refs.videoWrapper).data('trackingname',trackingname);
			$(refs.videoWrapper).data('brandid',brandid);
			$(refs.videoWrapper).data('gzgscevent',gzgscevent);
			$(refs.videoWrapper).data('gzgevent',gzgevent);
			$(refs.videoWrapper).attr('data-trackingname',trackingname);
			$(refs.videoWrapper).attr('data-brandid',brandid);
			$(refs.videoWrapper).attr('data-gzgscevent',gzgscevent);
			$(refs.videoWrapper).attr('data-gzgevent',gzgevent); 
		}
    //only if current video is not last on
    // thumbnails list - load the new one

    if ($nextThumbnail.length) {
		var src = provideArrayOfSources($nextThumbnail);
        if(mediaElement.pluginType == "youtube")//if the video is youtube video
        {      
            if(autoPlayNext==true)
            {
                var videoUrl=src[0].src;
                $titleContainer.text(title);
                $descriptionContainer.text(description);
                var videoId = videoUrl.substr(videoUrl.length-11,Math.max(videoUrl.length,11));
                mediaElement.pluginApi.loadVideoById(videoId);
            }
            else
            {
                reloadYoutube({
                    mediaElement: mediaElement,
                    src: src,             
                    title: title,
                    description: description
                });
            }
        }
    else// if the video is native video(mp4/webm/...)
    {       
        reloadVideo({
            mediaElement: mediaElement,
            src: src,
            autoplay: autoPlayNext,
            title: title,
            poster: poster,
            description: description
        });
    }	
        //re-set active states
        $thumbnailsItems.removeClass("is-active");
        $nextThumbnail.addClass("is-active");
    }
});

function recalculateListWith() {
    listContainerWidth = $thumbnailsListContainer.width();
    $thumbnailsList.css({left: 0});
    $arrows.filter(".videoGallery-arrow-left").addClass("is-disabled");
    $arrows.filter(".videoGallery-arrow-right").removeClass("is-disabled");
}

$(window).resize(_.throttle(recalculateListWith, 150));

//scrolling logic
$arrows.click("click", function () {
    //stop previous animation if exists
    $thumbnailsList.stop(false, true);

    var $this = $(this),
        //parse left css property
        left = parseInt($thumbnailsList.css("left"), 10),

        //1 - right, -1 - left
        vector = $this.hasClass("videoGallery-arrow-left") ? 1 : -1,

        //calculate next position of list
        nextLeft = left + (thumbnailWidth * vector),

            //calculate position of the end of list
            maxLeft = listOuterWidth - listContainerWidth +
                (listContainerWidth % thumbnailWidth);

    //reset states
    $arrows.removeClass("is-disabled");

    //check if list didn't reach the beginning or end
    if (nextLeft <= 0 && (-1) * nextLeft <= maxLeft) {
        $thumbnailsList.animate({left: nextLeft});
    } else {
        //disable clicked arrow
        $this.addClass("is-disabled");
    }

    //check if this is last possible move
    nextLeft += thumbnailWidth * vector;
    if (!(nextLeft <= 0 && (-1) * nextLeft <= maxLeft)) {
        //disable clicked arrow
        $this.addClass("is-disabled");
    }
});
	if(group == "cf")
	{
		eventHandler(mediaElement);
	}
}
});

		$(".videoGallery > .component-content")
.not(".videoGallery > .component-content.initialized").each(function () {

    var $videoplayer = $(this),
        $video = $videoplayer.find("video"),
        $videocontainer = $videoplayer.find(".videoGallery-video"),
        features = [
            "playpause",
            "progress",
            "current",
            "duration",
            "tracks",
            "volume",
            "fullscreen"
        ];

    dataGzgEvent = $videocontainer.data('gzgevent');
    dataGzgscEvent = $videocontainer.data('gzgscevent');
    dataBrandId = $videocontainer.data('brandid');
    dataTrackingName = $videocontainer.data('trackingname');

    $videoplayer.addClass("initialized");
    $video.css("max-height","");
});
};

function provideArrayOfSources(domElement) {
    var data = $(domElement).data();

        // build array of sources and types
        var sources = [
            {src: data.srcMp4, type: "video/mp4"},
            {src: data.srcOgg, type: "video/ogg"},
            {src: data.srcWebm, type: "video/webm"},
			{src: data.srcYoutube, type: "video/youtube"}
        ];
    // remove item if source is falsy
    return _.filter(sources, "src");
}

Cog.registerComponent({
    name: "videoGallery",
    api: api,
    selector: ".videoGallery",
    requires: [
        {
            name: "utils.settings",
            apiId: "settings"
        }
    ]
});

}(Cog.jQuery()));
