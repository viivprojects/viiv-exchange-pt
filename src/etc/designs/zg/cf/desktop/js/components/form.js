(function ($, document, _) {
    "use strict";
    /**
     * Polyfill for Object create should be moved to utils
     */
    if (typeof Object.create !== 'function') {
        Object.create = function (o, props) {
            function F() {
            }

            F.prototype = o;

            if (typeof(props) === 'object') {
                for (var prop in props) {
                    if (props.hasOwnProperty((prop))) {
                        F[prop] = props[prop];
                    }
                }
            }
            return new F();
        };
    }

    /**
     * Creates the form object
     *
     * @constructor
     */
    function Form() {
        Form.prototype.init();
    }

    /**
     * Set elements and configuration
     *
     * @param $el {object} jquery object of form
     */
    Form.prototype.setEls = function ($el) {
        this.$el = $el;
        var submitButton = this.$el.find("[type='submit']");
        this.config = {
            successClass: 'success',
            clientErrorClass: 'validation-error',
            serverErrorClass: 'error',
            mandatoryMessage: 'This field is mandatory.',
            submissionInProgressText:submitButton.attr("data-submission-in-progress-text") ? submitButton.attr("data-submission-in-progress-text") : submitButton.text(),
            submitButtonText: submitButton.text()
        };

        this.$messageSuccess = this.$el.find('.wsform-success');
        this.$messageFailure = this.$el.find('.wsform-failure');
        this.$form = $el.find('form');
    };


    /**
     * Make ajax request for this.$form
     *
     * @param dataSerialization {string} serialized data from form
     * @param doneCallback {function} action to be done after done AJAX request
     */
    Form.prototype.makeAjaxRequest = function (dataSerialization, doneCallback) {
        var url = this.$form.data('actionUrl'),
            serverResponse = doneCallback || this.onServerResponse,
            formPost,
            submitButton = this.$form.find('[type="submit"]');

        submitButton.attr("disabled",true)[0].childNodes[0].data=this.config.submissionInProgressText;
        formPost = $.post(url, dataSerialization);
        this.beenSubmitted = true;

        formPost.done(_.bind(serverResponse, this))
            .fail(_.bind(this.onServerValidationFailure, this));

    };

    /**
     * Handles whole AJAX call
     */
    Form.prototype.handleAjaxCall = function (){
        var data = this.getSerializationData();
            this.makeAjaxRequest(data);
    };

    /**
     * Adds class on failure
     */
    Form.prototype.onClientValidationFailure = function () {
        this.$el.addClass(this.config.clientErrorClass);
    };

    /**
     * Validates form based on mandatory input fields
     *
     * @returns {boolean} is form valid
     */
    Form.prototype.isClientValid = function () {
        var isValid = true,
            self = this;

        this.addCustomMandatoryMessage();

        // Regular inputs
        this.$el.find('.mandatory .original-input').each(function () {
            var $input = $(this),
                $inputHint = $input.siblings('.wsform-hint'),
                message = '',
                inputValue;

            if($input.is("[type='radio']")){
                var inputsRadio = $input.parents(".wsform-controls").find("input[type='radio']");

                $inputHint = $input.parent().siblings(".wsform-hint");
                inputValue = false;
                _.find(inputsRadio, function(el){
                    if($(el).is(":checked")){
                        inputValue = true;
                    }
                });

            }
            else if($input.is("select")){
                if($input.val()){
                    inputValue = true;
                }
            }
            else {
                inputValue = $input.is(':checkbox') ? $input.prop('checked') : $input.val().length;
            }

            $inputHint.removeClass('has-message');

            if (!inputValue) {
                message = self.config.mandatoryMessage;
                isValid = false;
                $inputHint.addClass('has-message');
            }
            $inputHint.html(message);
        });

        // Confirmation inputs
        this.$el.find('.confirmation .original-input').each(function () {
            var $input = $(this),
                $inputHint = $input.siblings('.wsform-hint'),
                patternSelector = '#' + $input.data('fieldtobeconfirmed').replace('+', '\\+'),
                $pattern = self.$el.find(patternSelector);

                if ($input.val() !== $pattern.val()) {
                    $inputHint.show();
                    isValid = false;
                }
        });

        return isValid;
    };

    /**
     * Create custom serialization or default from jQuery
     *
     * @returns {string} serialized data from form
     */
    Form.prototype.getSerializationData = function () {
        return this.$form.serialize();
    };

    /**
     * Checks if redirect is needed
     *
     * @param isSuccess {boolean}
     */
    Form.prototype.checkRedirect = function (isSuccess) {
        var $message = isSuccess ? this.$messageSuccess : this.$messageFailure;
        if ($message && $message.find('.redirect').length !== 0) {
            window.location.href = $message.find('.redirect').attr('href');
        }
        if ($message && $message.find('.lightbox').length !== 0) {
            var link = $message.find('.lightbox')[0];

            // Cross browser link click
            if (link.click) {
                link.click();
            } else if (document.createEventObject) {
                try {
                    var e = document.createEventObject();
                    $(link).change();
                    link.fireEvent('onclick', e);
                } catch(e) {
                }
            } else if (document.createEvent) {
                var e = document.createEvent('MouseEvents');
                e.initEvent('click', true, true);
                link.dispatchEvent(e);
            }

            $message.hide();
        }
    };

    /**
     * Fix for IE <= 8 not supporting :focus styling.
     * Needed for focused field border.
     */
    Form.prototype.applyCssFixes = function () {
        if (cf.utils.browser.msie && cf.utils.browser.version <= 8) {
            this.$el.find('input[type=text],textarea,select').bind('focus blur', function () {
                $(this).toggleClass('input-focus-border-fix');
            });
        }
    };

    /**
     * Fix implementing maxlength behaviour for textareas in browsers that
     * don`t support HTML5`s maxlength attribute.
     */
    Form.prototype.maxlengthPolyfill = function () {
        this.$el.find('textarea[maxlength]').bind('change keyup', function () {
            var $this = $(this),
                maxlength = $this.attr('maxlength'),
                val = $this.val();

            if (val.length > maxlength) {
                $this.val(val.substring(0, maxlength));
            }
        });
    };

    /**
     * Placeholder polyfill
     */
    Form.prototype.placeholderPolyfill = function () {

        var self = this;
        this.originalFields = [];

        // store original inputs in array for ie8/ie9 placeholder polyfill exception

        this.$el.find('input, textarea, select').each(function(){
            self.originalFields.push($(this));

        });
        if(this.$el.find('input, textarea').placeholder().attr('placeholder')){
            this.$el.find('input, textarea').placeholder().focus().blur();
        }

        this.placeholderFix();


    };

    Form.prototype.placeholderFix = function () {
        $.each(this.originalFields, function () {
            $(this).addClass("original-input");
        });
    };

    /**
     * Generic function to be overwritten by form
     *
     * @returns {boolean}
     */
    Form.prototype.onSubmit = function (e) {
        e.preventDefault();
        throw 'Custom submit function not added';
    };

    /**
     * Generic function for server response
     */
    Form.prototype.onServerResponse = function () {
        throw 'Custom server response not added';
        this.$form.find("[type='submit']").attr("disabled",false)[0].childNodes[0].data=this.config.submitButtonText;
    };

    /**
     * Clears Validation on DOM Element
     */
    Form.prototype.clearValidation = function () {
        this.$el.removeClass(this.config.successClass)
            .removeClass(this.config.clientErrorClass)
            .removeClass(this.config.serverErrorClass);

        this.$messageSuccess.hide();
        this.$messageFailure.hide();
        this.$el.find('.wsform-hint:not(.no-clear)').empty();
        this.$el.find('.wsform-hint.no-clear').hide();
    };

    /**
     * Handle general errors display
     *
     *
     * @param data {object} data with errors
     */
    Form.prototype.handleGeneralErrors = function (data) {
        var $errorList = this.$messageFailure;

        $errorList.empty();

        _.forEach(data.generalErrors, function (item) {
            var message = item.code + ': ' + item.description;
            var $errorItem = $('<p />', {
                text: message
            });

            if (!Boolean(data.debug)) {
                $errorItem.hide();
            }

            $errorList.append($errorItem);
        });

    };

    /**
     * Handles server validation failure
     *
     * @param data
     */
    Form.prototype.onServerValidationFailure = function (data) {
        this.beenSubmitted = false;
        this.$el.addClass(this.config.serverErrorClass);
        this.$form.find("[type='submit']").attr("disabled",false)[0].childNodes[0].data=this.config.submitButtonText;

        if (data && data.generalErrors.length && data.debug) {
            this.handleGeneralErrors(data);
        }

        if (!data.operationSuccessful) {
            this.$messageFailure.show();
        }

        this.checkRedirect(false);
    };

    /**
     * Handles success AJAX response
     */
    Form.prototype.onSuccess = function () {
        this.$messageSuccess.show();
        this.checkRedirect(true);
        this.$el.addClass(this.config.successClass);

        Cog.fireEvent("submitEventTracker", "eventToTrack", {
            gzgEvent: this.$el.find('[type=submit]').data('gzgevent'),
            gzgscEvent: this.$el.find('[type=submit]').data('gzgscevent'),
            brandId: this.$el.find('[type=submit]').data('brandid'),
            trackingName: this.$el.find('[type=submit]').data('trackingname')
        });

    };

    /**
     * Initializes confirmation component
     */
    Form.prototype.confirmationInit = function () {
        var $confirmation = this.$el.find('.confirmation input');

        $confirmation.each(function () {
            var $input = $(this),
                $message = $('<div>').addClass('wsform-hint').addClass('no-clear').text($input.data('errormessage'));

            $input.after($message.hide());
        });
    };

    /**
     * Initializes datepicker, sets its events and lang
     */
    Form.prototype.datepickerInit = function () {
        var $datepicker = this.$form.find('input[type=datepicker]'),
        	lang = $datepicker.attr('data-language'),
            dateFormat = $datepicker.attr('data-format');

        $datepicker.datepicker({
            dateFormat: dateFormat
        });

        $datepicker.attr('readonly', true);

        $.datepicker.setDefaults($.datepicker.regional[lang === 'en' ? '' : lang]);
    };

    /**
     * Adds custom mandatory message on config
     * if such exists in DOM
     */
    Form.prototype.addCustomMandatoryMessage = function(){
        if(this.$form.data('mandatoryFieldMessage')){
            this.config.mandatoryMessage = this.$form.data('mandatoryFieldMessage');
        }
    };

    /**
     * Check if form can be submitted due to be blocked
     * a) because button has been clicked
     * b) callback function is not valid
     *
     * @param callback {function} action to be taken after standard check
     * @returns {boolean} whether form can be submitted
     */
    Form.prototype.canSubmit = function (callback){
        var canSubmit = !this.beenSubmitted;

        if (_.isFunction(callback)) {
            canSubmit = callback();
        }

        return canSubmit;
    };

    /**
     * Stop propagation of event
     *
     * @param ev
     */
    Form.prototype.stopEvent = function (ev) {
        ev.preventDefault();
        ev.stopPropagation();
    };

    /**
     * Initalization of form component
     */
    Form.prototype.init = function () {

        this.applyCssFixes();
        this.maxlengthPolyfill();
        this.placeholderPolyfill();

        if(this.$form.find('input[type=datepicker]').length) {
            this.datepickerInit();
        }

        if(this.$form.find('.confirmation').length) {
            this.confirmationInit();
        }
        this.$form.on('submit', _.bind(this.onSubmit, this));
    };

    var api = {};
    
    Cog.component.form = Form;
    
    Cog.registerComponent({
        name: "form",
        selector: ".form",
        api: api
    });
})(Cog.jQuery(),document,_);
