(function($) {
	var api = {};
	var eventResponse = "";
    var relatedEventUrls = '{}';
    var pastEventUrls = '{}';
	var eventKeyDocsOutput = "", eventSpeakerOutput = "", eventItineraryDayItemOutput = "", relatedEventsOutput = "",pastEventsOutput = "",tagsOutput="",dayDate="",dayDescription="",tagValue="";
	var registrationMode = "";
	api.onRegister = function(elements) {
		var $currentElements = elements.$scope;
       	$currentElements
				.each(function() {
					$(this)
							.on(
									"click",
									".eventDetails-container button.eventDetails-eventRegister-button",
									api.onRegisterClick);
					$(this)
							.on(
									"click",
									".eventDetails-container button.eventDetails-eventUnRegister-button",
									api.onRegisterClick);
					$(this)
							.on(
									"click",
									".eventDetails-container button.eventDetails-eventLaunch-button",
									api.onLaunchEventClick);
					$(this)
            				.on(
		            				"click",
		            				".eventDetails-container .eventDetails-eventNotLoggedIn a.eventDetails-login-register",
		            				api.onLoginClick);
				});
	};
	
	api.onLoginClick = function(){
        var eventDetails = {};
		eventDetails.scoId = eventResponse.sco_id;
        eventDetails.portalId = $('.eventDetails-container').attr("data-portal-id");
        Cog.Cookie.create("event-registration",eventDetails);
        var loginUrl = $(this).attr('href');
        var resourcePath = window.location.pathname + window.location.search;
        var loginAction = loginUrl + "eventId=" + eventResponse.sco_id + encodeURIComponent("&resource=" + resourcePath);
        $(this).attr('href',loginAction)
	}
	
	api.eventRegistrationStatus = function(){
        var registrationStatus = api.getParameterByName('registrationStatus');
        dataSuccess = "<p class='eventDetails-reg-success'>Registered Sucessfully!!</p>";
        dataFailure = "<p class='eventDetails-reg-failure'>Registeration failure!!</p>"; 
        if (registrationStatus === "success") {
            $('div.eventDetails-eventRegister').prepend(dataSuccess);
        } else if(registrationStatus === "failure") {     
            $('div.eventDetails-eventRegister').prepend(dataFailure);
        }
    };

	api.onLaunchEventClick = function() {
		var portalId = $('.eventDetails-container').attr("data-portal-id");
		var content = $(".eventDetails-container");
		var configPath = content.data("pagePath");
		var launchUrl = eventResponse.url_path;
		$.ajax({
			type : 'GET',
			url : configPath + '.sso.json',
			data : {},
			success : function(data) {
				window.open(launchUrl + "?portal-id=" + portalId + "&UID="
						+ data.UID + "&UIDSignature=" + data.UIDSignature + "&signatureTimestamp="
						+ data.signatureTimestamp, "_blank");
			},
			dataType : 'json'
		});

	};

	api.onRegisterClick = function(e) {
		
		e.preventDefault();
		var portalId = $('.eventDetails-container').attr("data-portal-id");
	    var registerUrl = $('.eventDetails-container a.eventDetails-eventRegister').attr('href');
	    var registerAction = registerUrl + "eventId=" + eventResponse.sco_id;	    
		var content = $(".eventDetails-container"), actionAttr = "", dataSuccess = "", dataFailure = "";
		var configPath = content.data("pagePath");
		var eventId = eventResponse.sco_id;
		var action = $(this).data("action");
		$(".eventDetails-reg-success").html("");
		$(".eventDetails-reg-failure").html("");
		if (action === "register") {
			actionAttr = "gsk.event-register";
			dataSuccess = "<p class='eventDetails-reg-success'>Registered Sucessfully!!</p>";
			dataFailure = "<p class='eventDetails-reg-failure'>Registeration failure!!</p>";
		} else if (action === "unregister") {
			actionAttr = "gsk.event-unregister";
			dataSuccess = "<p class='eventDetails-reg-success'>UnRegistered Sucessfully!!</p>";
			dataFailure = "<p class='eventDetails-reg-failure'>Unregistration failure!!</p>";
		}
		if(registrationMode === "one-click" || action === "unregister"){
			$.ajax({
				type : 'GET',
				url : configPath + '.sso.json',
				data : {},
				success : function(data) {
					$.ajax({
						type : 'GET',
						url : configPath + '.adobeconnectlist.json',
						data : {
							"action" : actionAttr,
							"sco-id" : eventId,
							"portal-id" : portalId,
							UIDSignature : data.UIDSignature,
							UID : data.UID,
							signatureTimestamp : data.signatureTimestamp
						},
						success : function(data) {
							if (data.results._status.code === "ok") {
								$('.eventDetails-eventRegister').prepend(
										dataSuccess);
							} else {
								$('.eventDetails-eventRegister').prepend(
										dataFailure);
							}
							location.reload();
						},
						dataType : 'json'
					});
				},
				dataType : 'json'
			});
		}    
		else if(registrationMode === "free-form" && action === "register"){
		    var resourcePath = window.location.pathname + window.location.search;  
		    window.location = registerAction + "&resource="+resourcePath;
		    $('.eventDetails-eventRegister').prepend(dataSuccess);  
		}  
	};

	api.updateEventDetailsTemplate = function(data) {        
        var content = $('.eventDetails-container');
        var eventInformation = $('.eventDetailsInfoTemplate').data(
		"eventInformation");
		var eventRegistration = $('.eventDetailsRegisterTemplate').data(
				"eventRegistration");
		var eventItinerary = $('.eventDetailsItineraryItemTemplate').data(
				"eventItinerary");
		var eventKeydocuments = $('.eventDetailsKeyDocumentsTemplate').data(
				"eventKeydocuments");
		var eventLocation = $('.eventDetailsLocationTemplate').data(
				"eventLocation");
		var eventPast = $('.eventDetailsPastRelatedEventsTemplate').data(
				"eventPast");
		var eventRelated = $('.eventDetailsRelatedEventsTemplate').data(
				"eventRelated");
		var eventRelatedMinutes = $('.eventDetailsRelatedEventsTemplate').data(
				"relatedMinutes");
		var eventSpeakers = $('.eventDetailsSpeakersTemplate').data(
				"eventSpeakers");
		var eventTags = $('.eventDetailsTagsTemplate').data(
				"eventTags");

        var eventDateFormat = $('.eventDetailsInfoTemplate').data(
				"dateFormat");
		var itineraryDateFormat = $('.eventDetailsItineraryItemTemplate')
				.data("itineraryDate");
		var relatedDateFormat = $('.eventDetailsRelatedEventsTemplate')
				.data("relateddateFormat");
		var pastDateFormat = $('.eventDetailsPastRelatedEventsTemplate')
				.data("pastdateFormat");	
		var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!==64){t=t+String.fromCharCode(r)}if(a!==64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

        if(!$.isEmptyObject($('.eventDetailsRelatedEventsTemplate').data('relatedeventUrls'))){ 
        	relatedEventUrls = Base64.decode($('.eventDetailsRelatedEventsTemplate').data('relatedeventUrls'));
        } 
		
		 if(!$.isEmptyObject($('.eventDetailsPastRelatedEventsTemplate').data('pasteventUrls'))){ 
	        	pastEventUrls = Base64.decode($('.eventDetailsPastRelatedEventsTemplate').data('pasteventUrls'));
	        }		 
		 var jsonResponse = $.parseJSON(data);
		 var individualResVal = "";
		 var fullResponse = jsonResponse['results'];
        var eventDetailsInfoTemplate = content
				.find(".eventDetailsInfoTemplate").html(), eventDetailsSpeakersTemplate = content
				.find(".eventDetailsSpeakersTemplate").html(), eventDetailsItineraryItemTemplate = content
				.find(".eventDetailsItineraryItemTemplate").html(), eventDetailsKeyDocumentsTemplate = content
				.find(".eventDetailsKeyDocumentsTemplate").html(), eventDetailsLocationTemplate = content
				.find(".eventDetailsLocationTemplate").html(), eventDetailsRegisterTemplate = content
				.find(".eventDetailsRegisterTemplate").html(), eventDetailsUnRegisterTemplate = content
				.find(".eventDetailsUnRegisterTemplate").html(), eventDetailsNotLoggedInTemplate = content
				.find(".eventDetailsNotLoggedInTemplate").html(), eventDetailsLaunchTemplate = content
				.find(".eventDetailsLaunchTemplate").html(), eventDetailsTagsTemplate = content
				.find(".eventDetailsTagsTemplate").html(), eventDetailsRelatedEventsTemplate = content
				.find(".eventDetailsRelatedEventsTemplate").html(), eventDetailsPastRelatedEventsTemplate = content
				.find(".eventDetailsPastRelatedEventsTemplate").html(),eventTagsOutput = "", eventRelatedEventsOutput = "", eventPastRelatedEventsOutput = "", eventInfoOutput = "", itineraryOutput = "", itineraryDayOutput = "", keyDocOutput = "", speakersOutput = "", locationOutput = "";
		_.templateSettings = {
			interpolate : /\{\{(.+?)\}\}/g,
			evaluate : /\{\%(.+?)\%\}/g
		};

		$.each(fullResponse, function(key, value) {
			if(key === "registration_mode"){
	            if(value !== '' && value !== undefined){
	                registrationMode = value;
	            }
	        }
			
			if (key === "event_info" && eventInformation === "eventInformation") {
                if (value !== "") {

					individualResVal = value;
					eventResponse = value;
					api.updateEventDetailsInfoTemplate(individualResVal,
							eventInfoOutput, eventDetailsInfoTemplate,
							eventDateFormat);
				} 
			} else if (key === "itinerary" && eventItinerary === "itinerary") {

				if (value !== "") {
					individualResVal = value;
					api.updateEventDetailsItineraryInfoTemplate(
							individualResVal, itineraryOutput,
							itineraryDayOutput,
							eventDetailsItineraryItemTemplate,
							itineraryDateFormat);

				}
			} else if (key === "speakers" && eventSpeakers === "speakers") {

				if (value !== '') {
					individualResVal = value;
					api.updateEventDetailsSpeakersInfoTemplate(
							individualResVal, speakersOutput,
							eventDetailsSpeakersTemplate);

				}
			} else if (key === "key_documents" && eventKeydocuments === "keyDocuments") {
				if (value !== "") {
					individualResVal = value;
					api.updateEventDetailsKeyDocumentsInfoTemplate(
							individualResVal, keyDocOutput,
							eventDetailsKeyDocumentsTemplate);

				}
			} else if (key === "_location" && eventLocation === "location") {
				if (value !== "") {
					individualResVal = value;
					api.updateEventDetailsLocationInfoTemplate(
							individualResVal, locationOutput,
							eventDetailsLocationTemplate);

				}
			} else if (key === "is_user_logged_in" && eventRegistration === "eventRegistration") {
				if (value !== '') {
					individualResVal = value;
					api.updateEventDetailsNotLoggedInfoTemplate(fullResponse,
							individualResVal, eventDetailsNotLoggedInTemplate,
							eventDetailsRegisterTemplate,
							eventDetailsUnRegisterTemplate,
							eventDetailsLaunchTemplate);

				}

			} else if (key === "tags" && eventTags === "tags") {
				if (value !== "") {
					individualResVal = value;
					api.updateEventDetailsTagsTemplate(
							individualResVal, eventDetailsTagsTemplate,eventTagsOutput);

				}

			} else if (key === "related_events" && eventRelated === "relatedEvents") {
				if (value !== "") {
					individualResVal = value;
					api.updateEventDetailsRelatedEventsTemplate(
							individualResVal, eventRelatedEventsOutput,
							eventDetailsRelatedEventsTemplate,
							relatedDateFormat,eventRelatedMinutes);

				}

			} else if (key === "past_related_events" && eventPast === "pastRelatedEvents") {
				if (value !== "") {
					individualResVal = value;
					api.updateEventDetailsPastRelatedEventsTemplate(
							individualResVal, eventPastRelatedEventsOutput,
							eventDetailsPastRelatedEventsTemplate,
							pastDateFormat);

				}

            }
		});     

}; 
	api.updateEventDetailsTagsTemplate = function(individualResVal, eventDetailsTagsTemplate,eventTagsOutput) {
		var local = individualResVal;
		$('.eventDetailsTagsInfo ul').each(function(){
            var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsTagsTemplate').html();
            $.each(local, function(key, value) {
			if (key === "tag") {
				individualResVal = value;
				for (i = 0; i < individualResVal.length; i++) {						
						try {
							individualResVal[i].tag_id = individualResVal[i].tag_id?individualResVal[i].tag_id:'';
							individualResVal[i].tag_name = individualResVal[i].tag_name?individualResVal[i].tag_name:'';

                            tagValue = ((individualResVal[i].tag_name).split("::"));
                            if(tagValue.length === 1)
                            {individualResVal[i].tag_name = tagValue[0];
                            }else
                            {individualResVal[i].tag_name = tagValue[1];
                            }

							tagsOutput += _.template(
									template,
									individualResVal[i]);
						} catch (e) {
						}

						eventTagsOutput = eventTagsOutput
								+ "<div class='eventDetailsTagsSet"
								+ i + "'>" + tagsOutput + "</div>";

						tagsOutput = "";
					}
				}
		});        
        $(this).html(eventTagsOutput);
        eventTagsOutput = "";
		});
	};

	api.updateEventDetailsRelatedEventsTemplate = function(individualResVal,
			eventRelatedEventsOutput, eventDetailsRelatedEventsTemplate,
			relatedDateFormat,eventRelatedMinutes) {
		var local = individualResVal;
		$('.eventDetails-relatedEvents ul').each(function(){
            var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsRelatedEventsTemplate').html();
            $.each(local, function(key, value) {
			if (key === "event") {
				individualResVal = value;
				for (i = 0; i < individualResVal.length; i++) {

						if (individualResVal[i].date_begin && !isNaN(new Date(individualResVal[i].date_begin).getTime())) {
							individualResVal[i].date_begin = api
									.convertUTCDateToLocalDate(new Date(
											individualResVal[i].date_begin),
											relatedDateFormat);
						}
						if (individualResVal[i].date_end && !isNaN(new Date(individualResVal[i].date_end).getTime())) {
							individualResVal[i].date_end = api
									.convertUTCDateToLocalDate(new Date(
											individualResVal[i].date_end),
											relatedDateFormat);
						}
						if (individualResVal[i].lang) {
							individualResVal[i].lang = api
									.getLanguageName(individualResVal[i].lang);
						}
                      _.forEach(JSON.parse(relatedEventUrls), function (eachUrl){
			                if(individualResVal[i].sco_id === eachUrl.text){
                              individualResVal[i].relatedEventDetailsPage = eachUrl.path+'.html?eventId='+individualResVal[i].sco_id;
			                    return false;  
			                }
			            });
                      
                      if(!individualResVal[i].relatedEventDetailsPage){
  						individualResVal[i].relatedEventDetailsPage = '#';
  			            }                      
			           try {
							individualResVal[i].event_info = individualResVal[i].event_info?individualResVal[i].event_info:'';
							individualResVal[i].event_info = individualResVal[i].event_info.replace(/\\n/g,'<br>').replace(/\\/g,'');	
							individualResVal[i]._name = individualResVal[i]._name?individualResVal[i]._name:'';
							individualResVal[i].date_begin = individualResVal[i].date_begin?individualResVal[i].date_begin:'';
							individualResVal[i].date_end = individualResVal[i].date_end?individualResVal[i].date_end:'';
							individualResVal[i].location_name = individualResVal[i].location_name?individualResVal[i].location_name:'';
							individualResVal[i].lang = individualResVal[i].lang?individualResVal[i].lang:'';
							individualResVal[i].description = individualResVal[i].description?individualResVal[i].description:'';
							individualResVal[i].description = individualResVal[i].description.replace(/\\n/g,'<br>').replace(/\\/g,'');
							individualResVal[i].event_banner_small = individualResVal[i].event_banner_small?individualResVal[i].event_banner_small:'';
                            individualResVal[i].event_banner_large = individualResVal[i].event_banner_large?individualResVal[i].event_banner_large:'';
                            individualResVal[i].event_category = individualResVal[i].event_category?individualResVal[i].event_category:'';
                            individualResVal[i].prescribing_info_url = individualResVal[i].prescribing_info_url?individualResVal[i].prescribing_info_url:'';
                            individualResVal[i].prescribing_info_name = individualResVal[i].prescribing_info_name?individualResVal[i].prescribing_info_name:'';
                            individualResVal[i].prescribing_info_file_size = individualResVal[i].prescribing_info_file_size?individualResVal[i].prescribing_info_file_size:'';
                            individualResVal[i].duration_minutes = individualResVal[i].duration_minutes?individualResVal[i].duration_minutes:'';
                            individualResVal[i].minutes_before_launch = individualResVal[i].minutes_before_launch?individualResVal[i].minutes_before_launch:'';
                            individualResVal[i].regulatory_approval_code = individualResVal[i].regulatory_approval_code?individualResVal[i].regulatory_approval_code:'';

                         if (individualResVal[i].event_category === "live") { 
                             individualResVal[i].webinarEvent = "showWebinarEvent";
                         }else{
                             individualResVal[i].webinarEvent = "showEventLocation";
                         }

                         if(eventRelatedMinutes != null && eventRelatedMinutes == true)
                         {                         
                         var liveTimeMinutes = parseInt(individualResVal[i].minutes_before_launch) + parseInt(individualResVal[i].duration_minutes);
                         if (individualResVal[i].minutes_before_launch <= 0 && liveTimeMinutes > 0) {
                            individualResVal[i].liveEvent = "showLiveEvent";
						 }else{
                            individualResVal[i].liveEvent = "hideLiveEvent";
                         }
                         }else{
                        	 var liveTimeHours = parseInt(individualResVal[i].hours_before_launch) + parseInt(individualResVal[i].duration);
                             if (individualResVal[i].hours_before_launch <= 0 && liveTimeHours > 0) {
                                individualResVal[i].liveEvent = "showLiveEvent";
    						 }else{
                                individualResVal[i].liveEvent = "hideLiveEvent";
                             } 
                         }

							relatedEventsOutput += _.template(
									template,
									individualResVal[i]);
						} catch (e) {
						}

						eventRelatedEventsOutput = eventRelatedEventsOutput
								+ "<li class='eventDetailsRelatedEventsSet"
								+ i + "'>" + relatedEventsOutput + "</li>";

						relatedEventsOutput = "";
					}
				}
		});		
		 $(this).html(eventRelatedEventsOutput);
		 eventRelatedEventsOutput = "";
		});
	};

	api.updateEventDetailsPastRelatedEventsTemplate = function(
			individualResVal, eventPastRelatedEventsOutput,
			eventDetailsPastRelatedEventsTemplate, pastDateFormat) {
		var local = individualResVal;
		$('.eventDetails-pastRelatedEvents ul').each(function(){
            var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsPastRelatedEventsTemplate').html();
            $.each(local, function(key, value) {
							if (key == 'event') {
								individualResVal = value;
								for (i = 0; i < individualResVal.length; i++) {

										if (individualResVal[i].date_begin && !isNaN(new Date(individualResVal[i].date_begin).getTime())) {
											individualResVal[i].date_begin = api
													.convertUTCDateToLocalDate(
															new Date(
																	individualResVal[i].date_begin),
															pastDateFormat);
										}
										if (individualResVal[i].date_end && !isNaN(new Date(individualResVal[i].date_end).getTime())) {
											individualResVal[i].date_end = api
													.convertUTCDateToLocalDate(
															new Date(
																	individualResVal[i].date_end),
															pastDateFormat);
										}
										if (individualResVal[i].lang) {
											individualResVal[i].lang = api
													.getLanguageName(individualResVal[i].lang);
										}
										
										_.forEach(JSON.parse(pastEventUrls), function (eachUrl){
							                if(individualResVal[i].sco_id == eachUrl.text){
				                              individualResVal[i].pastEventDetailsPage = eachUrl.path+'.html?eventId='+individualResVal[i].sco_id;
							                    return false;  
							                }
							            });
										
										if(!individualResVal[i].pastEventDetailsPage){
											individualResVal[i].pastEventDetailsPage = '#';
								            }										
										try {
											individualResVal[i].event_info = individualResVal[i].event_info?individualResVal[i].event_info:'';
											individualResVal[i].event_info = individualResVal[i].event_info.replace(/\\n/g,'<br>').replace(/\\/g,'');
											individualResVal[i]._name = individualResVal[i]._name?individualResVal[i]._name:'';
											individualResVal[i].date_begin = individualResVal[i].date_begin?individualResVal[i].date_begin:'';
											individualResVal[i].date_end = individualResVal[i].date_end?individualResVal[i].date_end:'';
											individualResVal[i].location_name = individualResVal[i].location_name?individualResVal[i].location_name:'';
											individualResVal[i].lang = individualResVal[i].lang?individualResVal[i].lang:'';
											individualResVal[i].description = individualResVal[i].description?individualResVal[i].description:'';
											individualResVal[i].description = individualResVal[i].description.replace(/\\n/g,'<br>').replace(/\\/g,'');
											individualResVal[i].event_banner_small = individualResVal[i].event_banner_small?individualResVal[i].event_banner_small:'';
                                            individualResVal[i].event_banner_large = individualResVal[i].event_banner_large?individualResVal[i].event_banner_large:'';
                                            individualResVal[i].event_category = individualResVal[i].event_category?individualResVal[i].event_category:'';
                                            individualResVal[i].prescribing_info_url = individualResVal[i].prescribing_info_url?individualResVal[i].prescribing_info_url:'';
                                            individualResVal[i].prescribing_info_name = individualResVal[i].prescribing_info_name?individualResVal[i].prescribing_info_name:'';
                                            individualResVal[i].prescribing_info_file_size = individualResVal[i].prescribing_info_file_size?individualResVal[i].prescribing_info_file_size:'';
                                            individualResVal[i].duration_minutes = individualResVal[i].duration_minutes?individualResVal[i].duration_minutes:'';
                                            individualResVal[i].minutes_before_launch = individualResVal[i].minutes_before_launch?individualResVal[i].minutes_before_launch:'';
                                            individualResVal[i].regulatory_approval_code = individualResVal[i].regulatory_approval_code?individualResVal[i].regulatory_approval_code:'';

                                             if (individualResVal[i].event_category == 'live') { 
                             individualResVal[i].webinarEvent = "showWebinarEvent";
                         }else{
                             individualResVal[i].webinarEvent = "showEventLocation";
                         }
											pastEventsOutput += _
													.template(
															template,
															individualResVal[i]);
										} catch (e) {}

										eventPastRelatedEventsOutput = eventPastRelatedEventsOutput
												+ "<li class='eventDetailsPastRelatedEventsSet"
												+ i
												+ "'>"
												+ pastEventsOutput
												+ "</li>";

										pastEventsOutput = "";
									}
								}
						});		
		 $(this).html(eventPastRelatedEventsOutput);
		 eventPastRelatedEventsOutput = "";
		});
	};

	api.updateEventDetailsNotLoggedInfoTemplate = function(fullResponse,
			individualResVal, eventDetailsNotLoggedInTemplate,
			eventDetailsRegisterTemplate, eventDetailsUnRegisterTemplate,
			eventDetailsLaunchTemplate) {		
		
		if (individualResVal.logged_in == 'false') {
			if(fullResponse.event_status_info != undefined && fullResponse.event_status_info._status == "in-progress")
            {
                 $('.eventDetails-eventAnonymous').each(function(){
	              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsNotLoggedInTemplateLiveEvent').html();
	              $(this).html(template);
	          });
            }
            else
            {
				$('.eventDetails-eventAnonymous').each(function(){
	              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsNotLoggedInTemplate').html();
	              $(this).html(template);
	          });
			}
		} else {
			var eventStatus = fullResponse.event_status_info;
                var regEnabled = fullResponse.event_info.is_registration_limit_enabled?fullResponse.event_info.is_registration_limit_enabled:'';
			var permObj = fullResponse.permission;
                var numRegUsers = fullResponse.event_info.number_registered_users?fullResponse.event_info.number_registered_users:'';
                var regLimit = fullResponse.event_info.registration_limit?fullResponse.event_info.registration_limit:'';

                if (regEnabled == "true" && parseInt(numRegUsers) >= parseInt(regLimit)) {
					$('.eventDetails-eventRegister').each(function(){
			              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsFullTemplate').html();
			              $(this).html(template);
			          });				
				}else if(eventStatus != undefined && eventStatus._status == "in-progress")
			{
				$('.eventDetails-eventLaunch').each(function(){
		              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsLaunchTemplate').html();
		              $(this).html(template);
		          });
			}else if (permObj == undefined && eventStatus != undefined && eventStatus._status == "pre-event") {
				$('.eventDetails-eventRegister').each(function(){
		              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsRegisterTemplate').html();
		              $(this).html(template);
		          });
			}else if (permObj != undefined && permObj.permission_id == "view") {
				$('.eventDetails-eventUnRegister').each(function(){
		              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsUnRegisterTemplate').html();
		              $(this).html(template);
		          });
				} else if (permObj != undefined && permObj.permission_id == "invited") {
					$('.eventDetails-eventRegister').each(function(){
			              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsInvitedRegisterTemplate').html();
			              $(this).html(template);
			          });
				}else if (permObj != undefined && permObj.permission_id == "denied" && permObj.permission_subcode == "hcp-access-limit-reached") {
					  $('.eventDetails-eventRegister').each(function(){
		              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsFullTemplate').html();
		              $(this).html(template);
		          });
				}else if (permObj != undefined && permObj.permission_id == "denied" && permObj.permission_subcode == "hcp-pending-validation-in-brandedge") {
					  $('.eventDetails-eventRegister').each(function(){
			              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsDeniedRegisterTemplate').html();
			              $(this).html(template);
			          });
				}else if (permObj != undefined && permObj.permission_id == "pending") {				
					$('.eventDetails-eventUnRegister').each(function(){
			              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsPendingUnRegisterTemplate').html();
			              $(this).html(template);
			          });
				}else if (permObj != undefined && permObj.permission_id == "participant") {				
					$('.eventDetails-eventUnRegister').each(function(){
			              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsParticipantUnRegisterTemplate').html();
			              $(this).html(template);
			          });
				}
				else if(permObj != undefined && permObj.permission_id == "denied" && permObj.permission_subcode == "user-not-on-invitation-list") {				
					$('.eventDetails-eventRegister').each(function(){
			              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsInviteOnlyDeniedRegisterTemplate').html();
			              $(this).html(template);
			          });
				}
			}
			api.eventRegistrationStatus();
		}
	
	api.updateEventDetailsInfoTemplate = function(individualResVal,
			eventInfoOutput, eventDetailsInfoTemplate, eventDateFormat) {

		if (individualResVal.date_begin && !isNaN(new Date(individualResVal.date_begin).getTime())) {
            individualResVal.time_begin = new Date(individualResVal.date_begin).toLocaleTimeString();
			individualResVal.date_begin = api.convertUTCDateToLocalDate(
					new Date(individualResVal.date_begin), eventDateFormat);
		}
		if (individualResVal.date_end && !isNaN(new Date(individualResVal.date_end).getTime())) {
			individualResVal.time_end = new Date(individualResVal.date_end).toLocaleTimeString();
			individualResVal.date_end = api.convertUTCDateToLocalDate(new Date(
					individualResVal.date_end), eventDateFormat);

		}
		if (individualResVal.lang) {
			individualResVal.lang = api.getLanguageName(individualResVal.lang);
		}
		try {
			individualResVal.event_info = individualResVal.event_info?individualResVal.event_info:'';
			individualResVal.event_info = individualResVal.event_info.replace(/\\n/g,'<br>').replace(/\\/g,'');
			individualResVal._name = individualResVal._name?individualResVal._name:'';
			individualResVal.date_begin = individualResVal.date_begin?individualResVal.date_begin:'';
			individualResVal.date_end = individualResVal.date_end?individualResVal.date_end:'';
			individualResVal.time_begin = individualResVal.time_begin?individualResVal.time_begin:'';
			individualResVal.time_end = individualResVal.time_end?individualResVal.time_end:'';
			individualResVal.duration_minutes = individualResVal.duration_minutes?individualResVal.duration_minutes:'';
			individualResVal.location_name = individualResVal.location_name?individualResVal.location_name:'';
			individualResVal.lang = individualResVal.lang?individualResVal.lang:'';
			individualResVal.description = individualResVal.description?individualResVal.description:'';
			individualResVal.description = individualResVal.description.replace(/\\n/g,'<br>').replace(/\\/g,'');
			individualResVal.event_banner_small = individualResVal.event_banner_small?individualResVal.event_banner_small:'';
            individualResVal.event_banner_large = individualResVal.event_banner_large?individualResVal.event_banner_large:'';
            individualResVal.prescribing_info_url = individualResVal.prescribing_info_url?individualResVal.prescribing_info_url:'';
            individualResVal.prescribing_info_name = individualResVal.prescribing_info_name?individualResVal.prescribing_info_name:'';
            individualResVal.prescribing_info_file_size = individualResVal.prescribing_info_file_size?individualResVal.prescribing_info_file_size:'';
            individualResVal.regulatory_approval_code = individualResVal.regulatory_approval_code?individualResVal.regulatory_approval_code:''; 
            
            if (individualResVal.event_category == 'live') { 
                individualResVal.webinarEvent = "showWebinarEvent";
            }else{
                individualResVal.webinarEvent = "showEventLocation";
            }

            var liveTime = parseInt(individualResVal.hours_before_launch) + parseInt(individualResVal.duration);
            if (individualResVal.hours_before_launch <= 0 && liveTime > 0) {
               individualResVal.liveEvent = "showLiveEvent";
			 }
            
		} catch (e) {

		}		
		 $('.eventDetailsInfo').each(function(){
             var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsInfoTemplate').html();
             var eventInfoOutput = _.template(template,individualResVal);
             $(this).html(eventInfoOutput);
         });		
	};

	api.updateEventDetailsItineraryInfoTemplate = function(individualResVal,
			itineraryOutput, itineraryDayOutput,
			eventDetailsItineraryItemTemplate,itineraryDateFormat) {
		var local = individualResVal;
		$('.eventDetailsItineraryInfo').each(function(){
            var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsItineraryItemTemplate').html();
            $.each(local, function(key, value) {
					if (key == 'day') {
								individualResVal = value;
								for (i = 0; i < individualResVal.length; i++) {

										if (individualResVal[i].date_scheduled && !isNaN(new Date(individualResVal[i].date_scheduled).getTime())) {
											individualResVal[i].date_scheduled = api
													.convertUTCDateToLocalDate(
															new Date(
																	individualResVal[i].date_scheduled),
															itineraryDateFormat);
										}
										if (individualResVal[i].date_scheduled) {
										dayDate = individualResVal[i].date_scheduled;
										}
										if (individualResVal[i].description) {
										individualResVal[i].description = individualResVal[i].description.replace(/\\n/g,'<br>').replace(/\\/g,'');		
										dayDescription = individualResVal[i].description;
										}
										for (j = 0; j < individualResVal[i].item.length; j++) {

											if (individualResVal[i].item[j].start_time && !isNaN(new Date(individualResVal[i].item[j].start_time).getTime())) {
												individualResVal[i].item[j].start_time = api
														.convertUTCTimeToLocalTime(
																new Date(
																		individualResVal[i].item[j].start_time),
																itineraryDateFormat);
											}
											if (individualResVal[i].item[j].end_time && !isNaN(new Date(individualResVal[i].item[j].end_time).getTime())) {
												individualResVal[i].item[j].end_time = api
														.convertUTCTimeToLocalTime(
																new Date(
																		individualResVal[i].item[j].end_time),
																itineraryDateFormat);
											}											
											try {
												individualResVal[i].item[j].start_time = individualResVal[i].item[j].start_time?individualResVal[i].item[j].start_time:'';
												individualResVal[i].item[j].end_time = individualResVal[i].item[j].end_time?individualResVal[i].item[j].end_time:'';
												individualResVal[i].item[j].description = individualResVal[i].item[j].description?individualResVal[i].item[j].description:'';
												 individualResVal[i].item[j].description = individualResVal[i].item[j].description.replace(/\\n/g,'<br>').replace(/\\/g,'');
												itineraryOutput += _
														.template(
																template,
																individualResVal[i].item[j]);												
											} catch (e) {
											}
										}

										eventItineraryDayItemOutput = eventItineraryDayItemOutput
												+ "<li class='eventDetailsItineraryData"
												+ "'>"
												+ "<div class='eventDetails-itineraryDayDate'><h5>"
												+ dayDate
												+ "</h5></div>"
												+ "<div class='eventDetailsItineraryItem'>"
												+ "<div class='eventDetails-itineraryDayDescription'><p>"
												+ dayDescription
												+ "</p></div>"
												+ itineraryOutput
												+ "</div>"
												+ "</li>";

										itineraryOutput = "";
									}

								}
						});		
		 $(this).html(eventItineraryDayItemOutput);
		 eventItineraryDayItemOutput = "";
		});
	};

	api.updateEventDetailsSpeakersInfoTemplate = function(individualResVal,
			speakersOutput, eventDetailsSpeakersTemplate) {
		var local = individualResVal;
		$('.eventDetailsSpeakerInfo').each(function(){
            var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsSpeakersTemplate').html();
            $.each(local, function(key, value) {
			individualResVal = value;
			if (key == 'speaker') {
				individualResVal = value;
					for (i = 0; i < individualResVal.length; i++) {						 
						try {
							individualResVal[i].photo_url = individualResVal[i].photo_url?individualResVal[i].photo_url:'';
							individualResVal[i]._name = individualResVal[i]._name?individualResVal[i]._name:'';
							individualResVal[i].title = individualResVal[i].title?individualResVal[i].title:'';
							individualResVal[i].description = individualResVal[i].description?individualResVal[i].description:'';
							individualResVal[i].description = individualResVal[i].description.replace(/\\n/g,'<br>').replace(/\\/g,'');
							speakersOutput += _.template(template,individualResVal[i]);							
						} catch (e) {
						}
						eventSpeakerOutput = eventSpeakerOutput
								+ "<li class='eventDetailsSpeaker'>"
								+ speakersOutput + "</li>";
						speakersOutput = "";
					}
				}
		});
            $(this).html(eventSpeakerOutput);
            $("li.eventDetailsSpeaker:odd").addClass("odd");
    		$("li.eventDetailsSpeaker:even").addClass("even");
            eventSpeakerOutput="";
		});
	};

	api.updateEventDetailsKeyDocumentsInfoTemplate = function(individualResVal,
			keyDocOutput, eventDetailsKeyDocumentsTemplate) {
        	var local = individualResVal;
			$('.eventDetailsKeyDocumentsInfo').each(function(){
                var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsKeyDocumentsTemplate').html();
                $.each(local, function(key, value) {
	  			if (key == 'key_document') {
	  				individualResVal = value;
	  				for (i = 0; i < individualResVal.length; i++) {
	  						try {
	  							individualResVal[i]._name = individualResVal[i]._name?individualResVal[i]._name:'';
	  							individualResVal[i].document_url = individualResVal[i].document_url?individualResVal[i].document_url:'';
	  							individualResVal[i].description = individualResVal[i].description?individualResVal[i].description:'';
	  							individualResVal[i].description = individualResVal[i].description.replace(/\\n/g,'<br>').replace(/\\/g,'');
	  							individualResVal[i].file_size = individualResVal[i].file_size?individualResVal[i].file_size:'';
	  							individualResVal[i].file_type = individualResVal[i].file_type?individualResVal[i].file_type:'';
	  							keyDocOutput +=  _.template(template,individualResVal[i]);
	  						} catch (e) {
	  						}
	  						eventKeyDocsOutput = eventKeyDocsOutput
	  								+ "<div class='eventDetailsKeyDocumentsSet" + i
	  								+ "'>" + keyDocOutput + "</div>";
	  						keyDocOutput = "";

	  					}				
	  			}

	          });
	              $(this).html(eventKeyDocsOutput);
                eventKeyDocsOutput="";
		});
	};

	api.updateEventDetailsLocationInfoTemplate = function(individualResVal,
			locationOutput, eventDetailsLocationTemplate) {
		try {
			individualResVal._name = individualResVal._name?individualResVal._name:'';
			individualResVal.address = individualResVal.address?individualResVal.address:'';
			individualResVal.latitude = individualResVal.latitude?individualResVal.latitude:'';
			individualResVal.longitude = individualResVal.longitude?individualResVal.longitude:'';
			individualResVal.phone_number = individualResVal.phone_number?individualResVal.phone_number:'';
			individualResVal.photo_url = individualResVal.photo_url?individualResVal.photo_url:'';			
		} catch (e) {
		}		
		  $('.eventDetailsLocationInfo').each(function(){
              var template = $(this).parents('.eventDetails-container:eq(0)').find('.eventDetailsLocationTemplate').html();
              var locationOutput = _.template(template,individualResVal);
              $(this).html(locationOutput);
          });

	};

	api.convertUTCDateToLocalDate = function(date, eventDateFormat) {

		var returnDate = $.datepicker.formatDate(eventDateFormat, date);

		return returnDate;
	};

	api.convertUTCTimeToLocalTime = function(date, eventDateFormat) {

		var returnDate = date.toLocaleTimeString();

		return returnDate;
	};
	
	api.getParameterByName = function(name){
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};

	api.getLanguageName = function(key) {
		var isoLangs = {
			"zh" : {
				"name" : "Chinese",
				"nativeName" : "中文 (Zhōngwén), 汉语, 漢語"
			},
			"en" : {
				"name" : "English",
				"nativeName" : "English"
			},
			"fr" : {
				"name" : "French",
				"nativeName" : "français, langue française"
			},
			"de" : {
				"name" : "German",
				"nativeName" : "Deutsch"
			},
			"it" : {
				"name" : "Italian",
				"nativeName" : "Italiano"
			},
			"ja" : {
				"name" : "Japanese",
				"nativeName" : "日本語 (にほんご／にっぽんご)"
			}
		}

		key = key.slice(0, 2);
		var lang = isoLangs[key];
		return lang ? lang.name : key;
	};
	
	Cog.component.eventdetails = api;

Cog.registerComponent({
        name: "eventdetails",
        api: api,
        selector: ".eventdetails"
    });

})(Cog.jQuery());
