/*global _, ZG, ZGjQuery */
(function ($, _) {
    'use strict';

    var api = {};

    /**
     * Create Login
     *
     * @param $el
     * @constructor
     */
    function LoginForm($el) {
        this.setEls($el);
        this.init();
    }

    /**
     * Extend LoginForm with form component
     *
     * @type {ZG.component.form.prototype}
     */
    LoginForm.prototype = Object.create(Cog.component.form.prototype, LoginForm.prototype);

    /**
     * On submit function
     *
     * @param ev
     * @returns {boolean}
     */
    LoginForm.prototype.onSubmit = function (ev) {

        if(!this.canSubmit()){
            return false;
        }

        this.clearValidation();

        if (!this.isClientValid()) {
            this.onClientValidationFailure();
        } else {
            this.handleAjaxCall();
        }

        this.stopEvent(ev);
    };

    /**
     * Handles server response
     *
     * @param data {object}
     */
    LoginForm.prototype.onServerResponse = function (data) {
        this.beenSubmitted = false;

        if (data.redirectTarget) {
            window.location.replace(data.redirectTarget);
        }
    };

    /**
     * Handles server validation failure
     *
     * @param data
     */
    LoginForm.prototype.onServerValidationFailure = function (data) {
        this.beenSubmitted = false;
        this.$el.addClass(this.config.serverErrorClass);
        this.$messageFailure.show();

        this.checkRedirect(false);
    };


    api.forms = []; 
    api.onRegister = function ($formsscope) {
    	var $forms = $formsscope.$scope;
    	$forms.each(function () {
            var $form = $(this),
                loginForm = new LoginForm($form);

            api.forms.push(loginForm);

            $form.addClass('initialized');
        });
    };


Cog.registerComponent({
    name: 'login',
    selector: '.component.login',
    api: api,
    requires: [
			{
				name: "wsForm",
				apiId: "wsForm"
			}
		]
});

})(Cog.jQuery(),_);