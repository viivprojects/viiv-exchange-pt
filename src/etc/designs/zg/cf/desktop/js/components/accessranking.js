(function($) {
	var api = {};

	api.onRegister = function(elements) {

        var $accessRanking = elements.$scope;

        if ($accessRanking.length > 0) {
          $('.accessRankingResults').each(function(){    

            $('.accessRankingResults').html();
            $('.excludeUrlOuterDiv').hide();
			var content = $(this).prev().prev(); 
			var length = content.data("excludeurlLength");
            var configPage = content.data("configPath");
            var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!==64){t=t+String.fromCharCode(r)}if(a!==64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

				var eachUrl = [];
                eachUrl.push($(this).siblings('.excludeUrlOuterDiv').children().text().trim());
                var currentElement = $(this);

			$.ajax({
						cache : true,
						type : 'GET',
						dataType : "json",
						url : configPage,
                        context:currentElement,
						data : {},
						success : function(data) {							
                            var obj = $.parseJSON(data);
                            var pageUrl = [];
                            var pageName = [];
                            var i;
                            var ele = this; 
							$(obj).each(function(i, response) {
                              var eachData = ($(this)[0].report.data);
							  $(eachData).each(function(j,k) {
                                  pageUrl.push($(this)[0].url);
                                  pageName.push($(this)[0].name);
                              });
                            });
                            var j,k,l;

                            for(j=0;j<pageUrl.length;j++){
                             for(k=0;k<eachUrl.length;k++){
                                 if(eachUrl[k] !== ''){   
                                if(pageUrl[j].indexOf(eachUrl[k]) !== -1) {
                                    pageUrl[j] = "";
                                    pageName[j]= "";
                                  }  
                                 }
                                }
                              }                      
                            for(l=0;l<pageUrl.length;l++){
                                if(!(pageUrl[l] === "")){
                                var decodedString = Base64.decode(pageName[l]); 

								$(ele).children('ol.accessRankingList').append('<li><a href=\''+pageUrl[l]+'\'>'+decodedString+'</a></li>');
                                }
                              }

						    }

					});
            });
		}
	};

Cog.registerComponent({
	name : "accessranking",
	api : api,
	selector : ".accessranking"
});

  })(Cog.jQuery());