(function($) {
    'use strict';
    var api = {};
    var consentArray = [];
    var dcrArray = [];
    api.screensets = [];
    api.apikey = '';
    api.serviceurl = '';
    api.redirectTarget = '';
    api.datastorePattern = 'data.ds.';
    api.userEmail;
    api.userAvailable = false;
    api.lookUpValidationStatus = 'false';
    api.allowLoginStatus = true;
    api.newLoginId = '';
    api.formError = false;
    api.veevaBuId = "";
    api.onRegister = function(element) {
        var $component = element.$scope;
        api.config = $component.data();
        var customButtonArray = [];
        var customButtonValues = {};
        customButtonValues.type = api.config.loginType;
        customButtonValues.providerName = api.config.providerName;
        customButtonValues.idpName = api.config.idpName;
        customButtonValues.iconURL = api.config.iconUrl;
        customButtonValues.logoURL = api.config.logoUrl;
        customButtonValues.lastLoginIconURL = api.config.lastLoginIconUrl;
        customButtonValues.position = api.config.position;
        customButtonArray.push(customButtonValues);
        var gigyaParams = {};
        gigyaParams.screenSet = api.config.screenSet;
        gigyaParams.mobileScreenSet = api.config.mobileScreenSet;
        gigyaParams.startScreen = api.config.startScreen;
        gigyaParams.containerID = api.config.componentId;
        gigyaParams.customLang = window.gigyaCustomLang[gigyaParams.containerID];
        gigyaParams.onAfterSubmit = this.postSubmitAction;
        gigyaParams.onBeforeSubmit = this.beforeSubmitAction;
        gigyaParams.onAfterScreenLoad = this.addExternalActionLocation;
        gigyaParams.onError = this.submitError;
        gigyaParams.lang = api.config.lang;
        gigyaParams.onFieldChanged = this.onFieldChange;
        if (api.config.formType == 'login') {
            gigyaParams.customButtons = customButtonArray;
        }
        var screenset = {};
        screenset.params = gigyaParams;
        screenset.component = $component;
        window.targetResource = api.config.resource;
        this.screensets.push(screenset);
        $(this).on('load', cf.loadCsrfToken('.gigya-raas'));
        api.componentPath = $component.data('resourcePath');
    };
    api.init = function(elements) {
        if (elements.length > 0) {
            this.loadGigyaLibrary();
        }
    };
    api.loadGigyaLibrary = function() {
        if (window.loadedGigya && window.loadedGigyaJS) {
            api.initializeGigyaScreens();
        } else if (!window.loadedGigya) {
            window.loadedGigya = true;
            $.getScript(api.config.serviceUrl + '?apikey=' + api.config.apiKey, function() {
                window.loadedGigyaJS = true;
                api.initializeGigyaScreens();
                cf.getAccountInfo();
            });
        }
    };
    api.initializeGigyaScreens = function() {
        var $screens = this.screensets;
        var $loginHandler = this.loginEventHandler;
        var $callback = this.callback;
        if (Cog.Cookie.read('logoutGigya')) {
            gigya.accounts.logout();
            Cog.Cookie.erase('logoutGigya')
        }
        api.getAccountInfo();
        $screens.forEach(function(element) {
            var screensetMethod = api.config.screensetMethod
            if (screensetMethod === 'staticCacheScreenset') {
                /******* include screen set in page and use for showscreenset call ******/
                var jsonObj = $.parseJSON(JSON.stringify(api.config.screensetCache));
                api.postGetScreenSetProcess(jsonObj);
            } else if (screensetMethod === 'dynamicCacheScreenset') {
                /****** Ajax call required to fetch screen set from page cache *******/
                var csrfToken = $('.gigya-raas #cq_csrf_token').val();
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: api.config.currentNode + '.cachescreenset.json',
                    data: {
                        'screenSetID': api.config.screenSet
                    },
                    success: function(data) {
                        api.postGetScreenSetProcess(data);
                    }
                });
            } else{
                if ($(element.component).find('a.gigya-raas-link').length > 0) {
                    $(element.component).find('a.gigya-raas-link').click(function(e) {
                        e.preventDefault();
                        var params = element.params;
                        delete params.containerID;
                        gigya.accounts.showScreenSet(params);
                    });
                } else {
                    if (Cog.Cookie.read('gigyaLogout')) {
                        gigya.accounts.logout();
                    }
                    gigya.accounts.showScreenSet(element.params);
                }
            }
            gigya.accounts.addEventHandlers({
                onLogin: $loginHandler
            });
        });
    };
    api.postGetScreenSetProcess = function(responseObj) {
        if (typeof responseObj != 'undefined') {
            var screenSet = responseObj;
            var siteLanguage = 'default';

            if(api.config.lang != null && !(api.config.lang === 'master')){
               siteLanguage = api.config.lang;
            }
            if(typeof (screenSet.translations[siteLanguage]) == 'undefined'){
                siteLanguage = 'default';
            }
            $('.gigya-raas').append('<div style="display:none;">' + screenSet.html + '</div>');
            $('.gigya-raas').append('<style>' + screenSet.css + '</style>');

            gigya.accounts.showScreenSet({
                containerID: api.config.componentId,
                screenSet: api.config.screenSet,
                startScreen: api.config.startScreen,
                customLang: screenSet.translations[siteLanguage],
                onAfterSubmit : this.postSubmitAction,
                onBeforeSubmit : this.beforeSubmitAction,
                onAfterScreenLoad : this.addExternalActionLocation,
                onBeforeScreenLoad : this.onBeforeScreenLoad,
                onError : this.submitError
            });
        } else {
           // design your error deisplay with (responseObj.errorDetails)
        }
    };
    api.loginEventHandler = function(eventObj) {
        if (eventObj.newUser == true) {
            api.updateDataStoreOnRegistration(eventObj);
        } // TODO configuration in component

        if ((api.config.formType == 'registration' && api.config.autoLogin == true) || api.config.formType == 'login') {
            if (api.allowLoginStatus) {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: window.location.pathname.split('.')[0] +
                        '/j_security_check?resource=' + targetResource,
                    data: {
                        'data': eventObj
                    },
                    success: function(data) {
                        window.location.href = data.redirectTarget;
                    }
                });
            }
        }
        Cog.fireEvent('gigyaEventHandler', 'userProfileChanged', eventObj.profile);
    };
    api.onFieldChange = function(eventObj) {
        if (eventObj.field == 'email' && eventObj.isValid == true) {
            var idParams = {};
            idParams.loginID = eventObj.value;
            idParams.callback = api.loginIdAvailablity;
            api.userEmail = eventObj.value;
            gigya.accounts.isAvailableLoginID(idParams);
        }
    };
    api.loginIdAvailablity = function(response) {
        if (response.status == 'OK' && response.isAvailable == false) {
            $('.hidein-profile-screen').slideUp();
            api.userAvailable = true;
        }
    };
    api.submitError = function(eventObj) {
        if (api.config.formType == 'change-email') {
            api.formError = true;
        }
    };
    api.beforeSubmitAction = function(eventObj) {
        if (api.config.formType == 'datastore') {
            api.updateDataStore(eventObj);
        } else if (api.config.formType == 'webshop') {
            api.updateWebshop(eventObj);
            return false;
        } else if (api.config.formType == 'change-email') {
            api.formError = false;
            api.newLoginId = eventObj.formData['loginID'];
        } else if (api.config.formType == 'profile') {
            if ($('.gigyaraas .consent-update').length > 0) {
                gigya.accounts.getAccountInfo({
                    callback: api.prepareConsentData,
                    include: 'profile',
                    extraProfileFields: 'phones'
                });
            }
        }
        if ($('.gigyaraas .delete-user-account').length > 0) {
            api.deleteUserProfile(eventObj);
            return false;
        }
    };
    api.prepareConsentData = function(eventObj) {
        consentArray = [];
        $('.gigyaraas .consent-update').each(function() {
            var classPrint = $(this).attr('class').split(' ');
            var consentType;
            var consentValue;
            for (var i = 0; i < classPrint.length; i++) {
                var matchconsentType = /consentType-/;
                if (matchconsentType.test(classPrint[i])) {
                    consentType = classPrint[i].split('-')[1].replace('_', ' ');
                }
                var matchconsentvalue = /consentValue-/;
                if (matchconsentvalue.test(classPrint[i])) {
                    consentValue = classPrint[i].split('-')[1];
                }
            }
            var consentValueList = consentValue.split('.');
            var consentValueLen = consentValueList.length;
            var consentVal = eventObj;
            for (var i = 0; i < consentValueLen; i++) {
                consentVal = typeof consentVal[consentValueList[i]] == 'undefined' ? 'NO DATA' : consentVal[consentValueList[i]];
            }
            consentArray.push({
                gigyaId: eventObj.UID,
                consentType: consentType,
                channelValue: consentVal,
                optStatus: $(this).find('input[type=checkbox]').is(':checked')
            });
        });
    };
    api.putConsentData = function(eventObj) {
        if (eventObj.response.status == 'OK' && eventObj.response.errorCode == 0) {
            var csrfToken = $('.gigya-raas #cq_csrf_token').val();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: window.location.pathname.split('.')[0] + '.putconsent.json',
                data: {
                    ':cq_csrf_token': csrfToken,
                    'consent': JSON.stringify(consentArray),
                    'buId' : cf.veevaBuId
                },
                success: function(data) {}
            });
        }
    };
    api.deleteUserProfile = function(obj) {
        var csrfToken = $('.gigya-raas #cq_csrf_token').val();
        var path = window.location.pathname.split('.')[0];
        $.ajax({
            type: 'POST',
            url: path + '.hardDelete.html',
            data: {
                'datastoreType': 'DeleteAccountData',
                ':cq_csrf_token': csrfToken
            },
            success: function(data) {
                window.location.pathname = targetResource;
            }
        });
    };
    api.changeEmail = function(eventObj) {
        var csrfToken = $('.gigya-raas #cq_csrf_token').val();
        if (api.newLoginId != '' && api.formError != true) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: window.location.pathname.split('.')[0] + '.changeloginemail.json',
                data: {
                    ':cq_csrf_token': csrfToken,
                    'newEmail': api.newLoginId
                },
                success: function(data) {
                    var params = {
                        screenSet: api.config.screenSet,
                        containerID: api.config.componentId,
                        startScreen: api.config.successScreenSet
                    };
                    gigya.accounts.showScreenSet(params);
                }
            });
        }
    };
    api.updateWebshop = function(eventObj) {
        var webshopData = {};
        _.each(eventObj.formData, function(value, key) {
            var splitdata = key.split('.');
            webshopData[splitdata[1]] = value;
        });
        var productList;
        var quantityList;
        var loadValue = Cog.component.webshop.readWebshopCookie();
        var loadJson = JSON.parse(loadValue);
        _.forEach(loadJson, function(product, productId) {
            productList = typeof productList == 'undefined' ? productId : productList + ',' + productId;
            quantityList = typeof quantityList == 'undefined' ? product.count : quantityList + ',' + product.count;
        });
        if (webshopData != null) {
            var csrfToken = $('.gigya-raas #cq_csrf_token').val();
            webshopData[':cq_csrf_token'] = csrfToken;
            webshopData['product'] = productList;
            webshopData['quantity'] = quantityList;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: window.location.pathname.split('.')[0] + '.createOrder.json',
                data: webshopData,
                success: function(data) {
                    if (data.order_number != '') {
                        Cog.Cookie.erase(Cog.component.webshop.cookieName);
                        api.trackEvent('', '', cf.veevaBuId);
                    }
                    var params = {
                        screenSet: api.config.screenSet,
                        containerID: api.config.componentId,
                        startScreen: api.config.successScreenSet
                    };
                    gigya.accounts.showScreenSet(params);
                }
            });
        }
    };
    api.updateDataStore = function(eventObj) {
        var datastoreType;
        if (!api.userEmail) {
            api.userEmail = eventObj.profile.email;
        }
        var datastore = {};
        _.each(eventObj.formData, function(value, key) {
            if (key.indexOf(api.datastorePattern) != -1) {
                var datakey = key.replace(api.datastorePattern, '');
                var splitdata = datakey.split('.');
                datastoreType = splitdata[0];
                if (typeof datastore[datastoreType] == 'undefined') {
                    datastore[datastoreType] = {};
                }
                datastore[datastoreType][splitdata[1]] = value;
            }
        });
        var csrfToken = $('.gigya-raas #cq_csrf_token').val();
        if (datastore) {
            var numberOfUpdates = _.size(datastore);
            var numberCompleted = 0;
            _.each(datastore, function(data, type) {
                $.post(window.location.pathname.split('.')[0] +
                    '.dscallback.json', {
                        'type': type,
                        'email': api.userEmail,
                        'data': encodeURIComponent(JSON.stringify(data)),
                        contentType: 'application/x-www-form-urlencoded;charset=utf-8',
                        ':cq_csrf_token': csrfToken
                    }).success(function(data) {
                    if (data.statusReason == 'OK') {
                        numberCompleted++;
                    }
                    if (numberCompleted == numberOfUpdates) {
                        if(cf.veevaSiteMode == "multiple"){
                            api.veevaBuId = data.data.ADDRESS.COUNTRY;
                        }
                        api.trackEvent(data.UID, '', api.veevaBuId);
                        var params = {
                            screenSet: api.config.screenSet,
                            containerID: api.config.componentId,
                            startScreen: api.config.successScreenSet
                        };
                        gigya.accounts.showScreenSet(params);
                    }
                });
            });
        }
        return false;
    };
    api.updateDataStoreOnRegistration = function(eventObj) {
        var datastore = eventObj.data.ds;
        var datastoreType;
        var datastoreValue;
        if (datastore != null) {
            var numberOfUpdates = _.size(datastore);
            var numberCompleted = 0;
            _.each(datastore, function(value, key) {
                datastoreType = key;
                datastoreValue = value;
                var csrfToken = $('.gigya-raas #cq_csrf_token').val();
                $.post(window.location.pathname.split('.')[0] + '.dscallback.json', {
                    'type': datastoreType,
                    'uid': eventObj.UID,
                    'data': encodeURIComponent(JSON.stringify(datastoreValue)),
                    contentType: 'application/x-www-form-urlencoded;charset=utf-8',
                    ':cq_csrf_token': csrfToken
                }).success(function(data) {
                    if (data.statusReason == 'OK') {
                        numberCompleted++;
                    }
                    if (numberCompleted == numberOfUpdates) {
                        var params = {
                            screenSet: api.config.screenSet,
                            containerID: api.config.componentId,
                            startScreen: api.config.successScreenSet
                        };
                        gigya.accounts.showScreenSet(params);
                    }
                });
            });
        }
    };
    api.addExternalActionLocation = function(eventObj) {
        $('.hidden-password-field input').val(new Date().getTime());
        if (api.config.formType == 'registration' && api.config.lookupValidate == true) {
            $('a.lookup-validate-trigger, .gigya-raas .gigya-input-submit').on('click', api.lookUpValidate);
            $('.gigya-raas .lookUp-validate input').on('blur', api.lookUpValidateTrigger);
        }
        api.prePopulateFields();
        if (api.config.formType == 'change-email') {
            $('.new-email-id').find('input').val('');
            $('.new-email-id-error').hide();
            $('.new-email-id input').blur(function() {
                api.validateChangeEmailId($(this));
            });
            $('.gigya-raas .gigya-input-submit').prop('disabled', true);
        }
    };
    api.validateChangeEmailId = function(param) {
        var idParams = {};
        idParams.loginID = $(param).val();
        idParams.callback = api.changeEmailValidation;
        gigya.accounts.isAvailableLoginID(idParams);
    };
    api.changeEmailValidation = function(response) {
        if (response.status == 'OK' && response.isAvailable == false && response.errorCode == 0) {
            $('.new-email-id-error').show();
            $('.gigya-raas .gigya-input-submit').prop('disabled', true);
        } else if (response.statusCode != 400 && response.errorCode == 0) {
            $('.new-email-id-error').hide();
            $('.gigya-raas .gigya-input-submit').prop('disabled', false);
        }
    };
    api.prePopulateFields = function() {
        var chooseField = api.getParameterByName('validationMethod');
        $('.gigyaraas .lookUp-prePopulate').each(function() {
            if (chooseField != null && chooseField != 'null' && chooseField != '') {
                var chooseClassName = 'validationMethod-' + chooseField;
                $('.' + chooseClassName + ' input[type=checkbox]').trigger('click');
            }
            var classPrint = $(this).attr('class').split(' ');
            for (var i = 0; i < classPrint.length; i++) {
                var matchString = /prePopulateParameter-/;
                if (matchString.test(classPrint[i])) {
                    var prePopulateAttribute = classPrint[i].split('-')[1];
                    var prePopulateValue = api.getParameterByName(prePopulateAttribute);
                    $(this).find('input').val(prePopulateValue);
                    $(this).find('select').val(prePopulateValue).attr('selected', 'selected');
                }
            }
        });
    };
    api.getParameterByName = function(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
            results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };
    api.postSubmitAction = function(eventObj) {
        if(cf.veevaSiteMode == "multiple"){
            api.veevaBuId = eventObj.data.ADDRESS.COUNTRY;
        }
        api.trackEvent(eventObj.response.UID, eventObj.data.crmid, api.veevaBuId);
        if (api.config.formType == 'profile') {
            api.raasProfileUpdate(eventObj);
            if ($('.gigyaraas .consent-update').length > 0) {
                api.putConsentData(eventObj);
            }
        } else if (api.config.formType == 'registration' && api.config.allowLogin != true) {
            api.lookUpCallBack(eventObj);
        } else if (api.config.formType == 'change-email') {
            api.changeEmail(eventObj);
        }
        api.sendEmailNotification(eventObj);
        var eventDetails = JSON.parse(Cog.Cookie.read('event-registration'));
        if (eventDetails) {
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: window.location.pathname.split('.')[0] + '.acregister.json',
                data: {
                    'sco-id': eventCookie.scoId,
                    'portal-id': eventCookie.portalId,
                    'action': 'gsk.event-register',
                    'UIDSignature': eventObj.response.UIDSignature,
                    'UID': eventObj.response.UID,
                    'signatureTimestamp': eventObj.response.signatureTimestamp
                }
            });
            Cog.Cookie.erase('event-registration');
        }
        if (api.config.resource && eventObj.response.status == 'OK' && (api.config.formType == 'registration' && api.config.autoLogin != true) && api.config.formType != 'login') {
            window.location.pathname = api.config.resource;
        }
    };
    api.sendEmailNotification = function(eventObj) {
        var csrfToken = $('.gigya-raas #cq_csrf_token').val();
        if (eventObj.response.status == 'OK') {
            $.ajax({
                type: 'POST',
                url: api.componentPath + '.notify.json',
                data: {
                    ':cq_csrf_token': csrfToken,
                    'data': eventObj.data,
                    'profile': eventObj.profile
                }
            });
        }
    };
    api.lookUpCallBack = function(eventObj) {
        var csrfToken = $('.gigya-raas #cq_csrf_token').val();
        var path = window.location.pathname.split('.')[0];
        $.ajax({
            type: 'POST',
            url: path + '.lookupcallback.json',
            async: false,
            data: {
                'UID': eventObj.response.UID,
                'isActive': api.allowLoginStatus,
                ':cq_csrf_token': csrfToken
            }
        });
    };
    api.raasProfileUpdate = function(eventObj) {
        var csrfToken = $('.gigya-raas #cq_csrf_token').val();
        var path = window.location.pathname.split('.')[0];
        var requestObj = {
            'profile': eventObj.profile,
            'data': eventObj.data,
            'UID': eventObj.response.UID,
            'UIDSignature': eventObj.response.UIDSignature,
            'signatureTimestamp': eventObj.response.signatureTimestamp
        };
        $.ajax({
            type: 'POST',
            url: path + '.gigyaprofileupdate.json',
            data: {
                'data': requestObj,
                ':cq_csrf_token': csrfToken
            }
        });
    };
    api.getAccountInfoResponse = function(response) {
        if (response.errorCode == 0) {
            Cog.fireEvent('gigyaEventHandler', 'userProfileChanged', response.profile);
        } else {
            Cog.fireEvent('gigyaEventHandler', 'userProfileChanged', {
                loggedOut: true
            });
            var csrfToken = $('.gigya-raas #cq_csrf_token').val();
            var path = window.location.pathname.split('.')[0];
            $.ajax({
                type: 'POST',
                url: path + '.gigyanotifylogin.json',
                async: false,
                data: {
                    ':cq_csrf_token': csrfToken
                }
            });
        }
    };
    api.getAccountInfo = function() {
        var $getAccountInfoResponse = api.getAccountInfoResponse;
        gigya.accounts.getAccountInfo({
            callback: $getAccountInfoResponse
        });
    };
    api.lookUpValidateTrigger = function() {
        var inputLen = $('.gigyaraas .lookUp-validate').length;
        var inputValLen = 0;
        $('.gigyaraas .lookUp-validate').each(function() {
            if ($(this).find('input').val() != '') {
                inputValLen = inputValLen + 1;
            }
        });
        if (inputValLen == inputLen) {
            api.lookUpValidate();
        }
    };
    api.lookUpValidate = function() {
        if ((api.lookUpValidationStatus == 'false' || api.lookUpValidationStatus != 'in-progress') && api.lookUpValidationStatus != 'true') {
            api.lookUpValidationStatus = 'in-progress';
            $('.gigya-raas .gigya-input-submit').prop('disabled', true);
            var lookUpData = {};
            $('.gigyaraas .lookUp-validate').each(function() {
                var classPrint = $(this).attr('class').split(' ');
                for (var i = 0; i < classPrint.length; i++) {
                    var matchString = /lookUpField-/;
                    if (matchString.test(classPrint[i])) {
                        var lookUpAttribute = classPrint[i].split('-')[1];
                        var lookUpValue = $(this).find('input').val();
                        if (typeof lookUpValue != 'undefined' && lookUpValue != null && lookUpValue != '') {
                            lookUpData[lookUpAttribute] = encodeURIComponent(lookUpValue);
                        }
                    }
                }
            });
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: window.location.pathname.split('.')[0] + '.lookupvalidate.json',
                data: lookUpData,
                success: function(data) {
                    var lookUpRecords = data.hcps;
                    if(typeof lookUpRecords != 'undefined'){
                        var lookUpLen = lookUpRecords.length;
                        if (lookUpLen == 1) {
                            $('.gigyaraas .lookUp-populate').each(function() {
                                var classPrint = $(this).attr('class').split(' ');
                                for (var i = 0; i < classPrint.length; i++) {
                                    var matchString = /lookUpField-/;
                                    if (matchString.test(classPrint[i])) {
                                        var lookUpAttribute = classPrint[i].split('-')[1];
                                        $(this).find('input').val(lookUpRecords[0][lookUpAttribute]);
                                        $(this).find('select').val(lookUpRecords[0][lookUpAttribute]).attr('selected', 'selected');
                                    }
                                }
                            });
                            $('input[name="data.crmid"]').val(lookUpRecords[0].rowId);
                            api.allowLoginStatus = true;
                        } else {
                            $('input[name="data.crmid"]').val('');
                            if (api.config.allowLogin != true) {
                                api.allowLoginStatus = false;
                            }
                        }
                    }
                    api.lookUpValidationStatus = 'true';
                    $('.gigya-raas .gigya-input-submit').prop('disabled', false);
                }
            });
        }
    };
    api.trackEvent = function(uid, crmid, buId) {
        if (typeof api.config.trackingname != 'undefined' && api.config.trackingname != null && api.config.trackingname != '') {
            Cog.fireEvent('submitEventTracker', 'eventToTrack', {
                userBuId: buId,
                userUid: uid,
                userCrmId: crmid,
                gzgEvent: api.config.gzgEvent,
                gzgscEvent: api.config.gzgscEvent,
                brandId: api.config.brandId,
                trackingName: api.config.trackingname
            });
        }
    };
    api.getConsent = function(rowId,buId){
        var inputData = {};
        inputData.rowId = rowId;
        if(cf.veevaSiteMode = "multiple"){
            inputData.buId = buId;
        }
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: window.location.pathname.split('.')[0] + '.getconsent.json',
            data: inputData,
            success: function (data) {
                var resLen = data.length;
                for(var i=0; i<resLen ; i++){
                    var consentType = data[i].consentType.replace(" ","_");
                    if(data[i].optStatus == "TRUE" || data[i].optStatus == "true" || data[i].optStatus == true){
                        $('.consentPopulateField-' + consentType + ' input[type=checkbox]').trigger("click");
                    }
                }
            }
        });
    };
    api.prepareDCRData = function(eventObj){
        dcrArray = [];
        var dcrData = {};
        $(".gigyaraas .dcr-update").each(function () {
            var classPrint = $(this).attr('class').split(' ');
            for (var i = 0; i < classPrint.length; i++) {
                var matchString = /dcrField-/;
                if (matchString.test(classPrint[i])) {
                    var dcrAttribute = classPrint[i].split('-')[1];
                    var dcrValue = $(this).find('input').val();
                    if (typeof dcrValue != 'undefined' && dcrValue != null && dcrValue != '') {
                        dcrData[dcrAttribute] = dcrValue;
                    }
                }
            }
        });
        dcrArray.push(dcrData);
    };
    api.putDCRData = function(eventObj){
        if (eventObj.response.status == "OK" && eventObj.response.errorCode == 0) {
            var csrfToken = $(".gigya-raas #cq_csrf_token").val();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: window.location.pathname.split('.') [0] + '.putdcr.json',
                data: {
                    ':cq_csrf_token': csrfToken,
                    'dcrdata': encodeURIComponent(JSON.stringify(dcrArray))
                },
                success: function (data) { }
            });
        }
    };
    Cog.registerComponent({
        name: 'gigyaraas',
        api: api,
        selector: '.gigya-raas'
    });
})(Cog.jQuery());
