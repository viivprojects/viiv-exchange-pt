/*global _, ZG, ZGjQuery, gigya */
(function ($, _) {
    'use strict';

    var api = {};

    /**
     * Create SocialShare
     *
     * @param $el {DOMElement} component element
     * @constructor
     */
    function SocialShare($el) {
        this.$el = $el;
        this.$content = $el.find('.socialShare-share');
        this.$url = $el.find('.socialShare-url');

        if (this.$url.size()) {
            this.initializeBitly();
        }
    }

    /**
     * Create object with configurations from data attributes
     *
     * @returns {object}
     */
    SocialShare.prototype.createConfig = function () {
        var config = {};

        config.title = this.$content.data('defaultmessage');
        config.url = this.$content.data('url');
        config.id = this.$content.attr('id');
        config.buttons = this.configureButtons(this.$content.data('providers'));

        return config;
    };

    /**
     * Create object with configurations from data attributes
     *
     * @param providers {string} list of providers separated with ,
     * @returns {array}
     */
    SocialShare.prototype.configureButtons = function (providers) {
        var self = this,
            buttons = [];

        _.each(providers.split(','), function (provider) {
            var button = {},
                providerName = provider.toLowerCase();

            button.provider = providerName;
            if (self.$content.data(providerName)) {
                button.iconImgUp = self.$content.data(providerName);
            }

            buttons.push(button);
        });

        return buttons;
    };

    /**
     * Gigya api initialization
     */
    SocialShare.prototype.initializeGigya = function () {
    	var shareCount = $(".socialShare-content").attr("data-share-count"); 
    	if(!shareCount) {
    		shareCount = 'none';
    	}
        if (window.gigya) {
            var action = new gigya.socialize.UserAction(),
                config = this.createConfig();

            action.setTitle(config.title);
            action.setLinkBack(config.url);

            gigya.socialize.showShareBarUI({
                shareButtons: config.buttons,
                buttonTemplate: '<span onClick="$onClick"><img src="$iconImg"></span>',
                showCounts: shareCount,
                containerID: config.id,
                userAction: action
            });
        }
    };    

    /**
     * Bitly input initialization
     */
    SocialShare.prototype.initializeBitly = function () {
        var self = this,
            $input = this.$url.find('input');

        ZeroClipboard.config({
            moviePath: '/etc/designs/zg/gzg/desktop/assets/swf/ZeroClipboard.swf',
            forceHandCursor: true,
            debug: false
        });          

        var clip = new ZeroClipboard($input);

        clip.on('load', function () {
            clip.on('dataRequested', function () {
                clip.setText($input.val());
                self.showCopyPopup();
            });
        });

        clip.on('noflash', function () {
            $input.on('click', function () {
                $input.select();
                self.showCopyPopup();
            });
        });
    };

    /**
     * Show copy confirmation popup
     */
    SocialShare.prototype.showCopyPopup = function () {
        var $popup = $('<div class="copyMessage">' + this.$url.data('message') + '<\/div>');

        this.$url.append($popup);

        $popup.css({
            "top": -$popup.innerHeight() - 6, // 6 - arrow (after pseudoelement) border width
            "margin-left": -$popup.innerWidth() / 2
        });

        setTimeout(function () {
            $popup.fadeOut(1000, function () {
                $popup.remove();
            });
        }, 2000);
    };

    api.shares = [];

    window.onGigyaServiceReady = function () {
        _.each(api.shares, function (obj) {
            obj.initializeGigya();
        });
    };

    api.loadScript = function(http, https) {  
        var protocol = window.location.protocol;

        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.src = (protocol === "https:") ? https : http;
        document.getElementsByTagName('head')[0].appendChild(script);
    };

    api.onRegister = function ($elements) {  
        $elements = $(".socialShare");  
        $elements.each(function (index) {    
            var $el = $(this),
                socialShare = new SocialShare($el),
                apiKey = $el.find('.socialShare-gigya').data('api');

            if (apiKey && index === 0) {    
                //Cog.component.loadExternalLibs.loadScript('http://cdn.gigya.com/JS/socialize.js?apikey=' + apiKey, 'https://cdns.gigya.com/JS/socialize.js?apikey=' + apiKey);
                api.loadScript('http://cdn.gigya.com/JS/socialize.js?apikey=' + apiKey, 'https://cdns.gigya.com/JS/socialize.js?apikey=' + apiKey);
            }

            api.shares.push(socialShare);

            //ie8 fix for gigya load
            setTimeout(function () {
                onGigyaServiceReady();
            }, 1000);

            $el.addClass('initialized');
        });
    };            

    Cog.registerComponent({
        name: 'socialShare',
        api: api,
        selector: '.socialShare'
        //requires: ['loadExternalLibs']
    });  

})(Cog.jQuery(), _);   