(function ($) {
	
	var api = {};
    var currentUrl; 
    var elementMap = {};
    
	api.onRegister = function (elements) {
		$elements = elements.$scope;
	    currentUrl = window.location.pathname; 
	    
	    $elements.find('.acregister-form-user input, .acregister-form-user select').each(function() {
	        var elementLabel = ''; 
	        var elementName = $(this).attr('name');     
	        $label = $("label[for='"+$(this).attr('id')+"']");       
	        if ($label.length > 0 ) {
	            elementLabel = $label.attr('for');
	        }    
	        elementMap[elementName] = elementLabel;     
        });
	    
		$elements.find('.acregister-form-user').submit(function(e) {
			e.preventDefault();
			api.submit($elements);  
		}); 	
	}; 
	
	api.submit = function(elements) {

        $(".acregister-form-user input:checkbox:checked").each(function() {
            $(this).val('true');
        });  

	  	var serializedUserData = $(elements).find('.acregister-userData').serialize();       
        var serializedEventData = JSON.stringify($(elements).find('.acregister-eventData').serializeArray());       
	  	var acUrl = $(elements).find('.acregister-container').attr('data-ac-url');
	  	var portalId = $(elements).find('.acregister-container').attr('data-portal-id');
	  	var pageUrl = currentUrl.replace('.html','.sso.json');

        var stringBuilder = ""; 
        $.each($.parseJSON(serializedEventData), function(key,value) {
         	stringBuilder = stringBuilder + "interaction-id=" + value.name + "&response=" + value.value + "&";
        }); 
         
        var serializedData = stringBuilder + serializedUserData;

	  	$.ajax({
			type : 'GET', 
			url : pageUrl,
			dataType : 'json',
			data : {},
			success : function(data) { 
				var action = 'gsk.event-register';
			  	var scoid = api.getParameterByName('sco-id');  
			  	var eventId = api.getParameterByName('eventId');
			  	var finalScoId = '';
			  	if(eventId != null){
			  	    finalScoId = eventId;
			  	}else {
			  	    finalScoId = scoid;
			  	}
			  	var formdata = 'action='+ action + '&sco-id=' + finalScoId + '&portal-id='+ portalId +'&' + serializedData + '&UID=' + data.UID + '&UIDSignature=' + data.UIDSignature + '&signatureTimestamp='+ data.signatureTimestamp;
			  	var url = currentUrl.replace('.html','.adobeconnectform.json');
                var resourcePath = api.getParameterByName('resource');                
			  	$.ajax({
			  		type:'GET',  
			  		url: url,  
			  		data:{"formdata":formdata,"acUrl":acUrl}, 
			  		success:function(data) {
                        var registrationStatus;
                        if(data.results._status.code === "ok") {
                           registrationStatus = 'success';
						   window.location = resourcePath + "&registrationStatus=" + registrationStatus;						   
                        } 
                        else {
                            var invalid = data.results._status.invalid?data.results._status.invalid:'';
                            var output = ''; 
                            var myObject = new Object();      
                            if(invalid !== "") {
	                            var invalidField = data.results._status.invalid.field?data.results._status.invalid.field:'';
	                            var subcode = data.results._status.invalid.subcode?data.results._status.invalid.subcode:'';
	                            var interactionId = data.results._status.invalid.interaction_id?data.results._status.invalid.interaction_id:'';
	                            output = data.results._status.invalid.error_message;
	                            var errorField = invalidField;    
     							$.each(elementMap, function(key, value) {
                                    if(interactionId === key){
     									errorField = value; 
                                    }
                                });        
                                myObject.field = errorField; 
                            } else {     
	 							output = data.results._status.error_message; 
	                            myObject.field = '';
                            } 
                            var errorMessage=''; 

                            _.templateSettings = {
                                interpolate : /\{\{(.+?)\}\}/g,
                                evaluate : /\{\%(.+?)\%\}/g 
                            };
                            errorMessage += _.template(output, myObject);

                            $('div.acregister-failure').prepend('<p>'+errorMessage+'</p>'); 
                        }         
			  		}
			  	});  
			} 
		}); 
	}; 
	
	api.getParameterByName = function(name){
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};
	
	Cog.registerComponent({
        name: "acregistration",
        api: api,
        selector: ".acregistration"
    });

})(Cog.jQuery());  
