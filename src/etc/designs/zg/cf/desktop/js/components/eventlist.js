(function ($) {
    
    var api = {};
    var currentElements,
        eventType,    
        eventDate,
        eventDateFormat,formdata,filterdata,eventDurationMinutes;         
    
    api.onRegister = function (elements) { 
    	var portalId = $('.eventDetails').attr("data-portal-id");
        var element = elements.$scope;
        _.forEach(element, function (data) {
            formdata = formdata?formdata:[];  
            var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
            currentElements = $(data);
            currentElements.find('.eventListContainer ul').html('<div class="loading"><img src="/etc/designs/zg/cf-gds/desktop/img/loader-white.gif">Loading...</div');
            var content = currentElements.children('.eventDetails');
            var resourcePath = content.data('resourcePath');
            var featuredEventId = content.data('featuredTags');
            var action = content.data('api-type');
            var eventUrls = '{}';
            if(!$.isEmptyObject(content.data('event-urls'))){ 
            	eventUrls = Base64.decode(content.data('event-urls'));
            }  
            var tagId = api.getParameterName('tag-id');
            config = {};
            config.eventCount = content.data('event-count');
            config.enableFilter = content.data('enable-filter');
            config.noEvent =  content.data('no-event');
            config.eventOffset = content.data('eventOffset');
            config.eventIds = content.data('eventIds');
            config.countValue = 0;
            config.eventUrls = eventUrls;
            config.eventPath = content.data('event-details');

            formdata.push({     
                name: 'portal-id',
                value: portalId
            }); 
            
            if(filterdata){
				formdata = $.merge( formdata , filterdata  );
            } else if(tagId != ''){
                formdata.push({
                    name: 'tag-id',
                    value: tagId
                });   
            }
            
            if(featuredEventId != ''){
                _.forEach(featuredEventId, function(eachFeature,index){
                    formdata.push({
                        name: 'tag-id',
                        value: eachFeature
                    }); 
                }); 
            }

            if(config.eventIds != ''){
                _.forEach(config.eventIds, function(eachId,index){
                    formdata.push({
                        name: 'filter-sco-id',
                        value: eachId
                    }); 
                });   
            }
            
            var uid,
                signatureTimestamp,
                uidSignature,
                dataAttr;
            if (element.length > 0) {  
                if (action == 'gsk.list-my-events') {
                    $.ajax({
                        type: 'GET',
                        url: resourcePath + '.sso.json',
                        success: function (data) {
                        uidSignature = data.UIDSignature;
                        signatureTimestamp = data.signatureTimestamp;
                        uid = data.UID;
                        formdata.push({
                        name: 'UID',
                        value: uid
                    });
                    formdata.push({
                        name: 'UIDSignature',
                        value: uidSignature
                    });
                    formdata.push({
                        name: 'signatureTimestamp',
                        value: signatureTimestamp
                    });
                    api.hitAdobeConnectAPI(resourcePath, formdata,currentElements,config);
                }
            });
        } 
                  else if (action == 'gsk.list-events') {
            api.hitAdobeConnectAPI(resourcePath, formdata,currentElements,config);
        }
    }
});
}; 
api.getParameterName = function (name){ 
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}; 

api.hitAdobeConnectAPI = function (resourcePath, formdata,currentElements,config) {
    var contextData = [
    ];
    contextData.push({
        name: 'currentEle',
        value: currentElements
    });
    contextData.push({
        name: 'configEle',
        value: config
    });    
    $.ajax({
        type:'GET',
        url:resourcePath + '.adobeconnectlist.json',
        data: formdata,
        context:contextData,
        success:function(data){
        var currentEleme,configEleme;
        _.forEach(this,function(eachData){
        if(eachData.name == 'currentEle'){
        currentEleme = eachData.value;
    }
           else if(eachData.name == 'configEle'){
        configEleme = eachData.value;
    } 
});
var jsonData = data['results'];
var eventArray,
    pastEventArray;
var eventList,
    pastEventList;
if (jsonData.events_list) {
    eventArray = jsonData['events_list'];
    eventList = eventArray['event'];
} 
else if (jsonData.my_current_events_list) {
    eventArray = jsonData['my_current_events_list'];
    eventList = eventArray['event'];
} 
else {
    eventList = '';
}
if (jsonData.past_events_list) {
    pastEventArray = jsonData['past_events_list'];
    pastEventList = pastEventArray['event'];
    configEleme.type='pastEvents'; 
} 
else if (jsonData.my_past_events_list) {
    pastEventArray = jsonData['my_past_events_list'];
    pastEventList = pastEventArray['event'];
    configEleme.type='pastEvents'; 
} 
else {
    pastEventList = '';
}
if (jsonData.tag_list) {
    api.taglist = jsonData.tag_list;
}
api.updatePrimaryListTemplate(currentEleme, eventList, pastEventList,configEleme,formdata);

},
    fail:function(e){

    }

});
};
api.updatePrimaryListTemplate = function (element, eventList, pastEventList,configEle,formdata) {
    var currentParentDiv = element;
    var content = currentParentDiv.children('.eventDetails');
    eventType = content.data('eventType');
    eventDateFormat = content.data('dateFormat');      
    eventDurationMinutes = content.data('durationMinutes');
    var template = '';
    template = currentParentDiv.find('.eventListTemplate').text();
    if (eventType == 'upcomingEvents') {
        api.populateTemplate(eventList, template, currentParentDiv, eventDateFormat,configEle, eventDurationMinutes);
    }
    if (eventType == 'pastEvents') {
        api.populateTemplate(pastEventList, template, currentParentDiv, eventDateFormat,configEle, eventDurationMinutes);
    }
    if (eventType == 'liveandpast') {
        api.populateTemplate(eventList, template, currentParentDiv, eventDateFormat,configEle, eventDurationMinutes);
        if(configEle.eventCount != ''){
            configEle.eventCount = configEle.eventCount - configEle.countValue;
            configEle.countValue = 0;
            configEle.eventOffset = 0;               
        }
        api.populateTemplate(pastEventList, template, currentParentDiv, eventDateFormat,configEle, eventDurationMinutes);
    }                  
    if(configEle.enableFilter==true){
        api.addFilter(api.taglist, currentParentDiv,formdata)
    }
    api.cleanUp(currentParentDiv,configEle);
    
    
};
api.formatDate = function (date, format) {
    return $.datepicker.formatDate(format, date);
};
api.populateTemplate = function (eventList, template, currentParentDiv, eventDateFormat,configEle, eventDurationMinutes) {
    var output = '';
    _.templateSettings = {
        interpolate: /\{\{(.+?)\}\}/g,
        evaluate: /\{\%(.+?)\%\}/g
    };
    
    _.forEach(eventList, function (data, index) {
        if(index >= configEle.eventOffset){
        	if(data.date_begin && !isNaN(new Date(data.date_begin).getTime())){
                var startDate = new Date(data.date_begin);
                data.date_begin = api.formatDate(startDate, eventDateFormat);
                data.time_begin = startDate.toLocaleTimeString();    
                }
                if(data.date_end && !isNaN(new Date(data.date_end).getTime())){
                var endDate = new Date(data.date_end);
                data.date_end = api.formatDate(endDate, eventDateFormat);
                data.time_end = endDate.toLocaleTimeString();     
                }  
                
                data.duration_minutes = data.duration_minutes?data.duration_minutes:'';
                data.minutes_before_launch = data.minutes_before_launch?data.minutes_before_launch:'';
				
				data.duration = data.duration?data.duration:'';
                data.hours_before_launch = data.hours_before_launch?data.hours_before_launch:'';

            if(eventDurationMinutes != null && eventDurationMinutes == true)
            {
            	var liveTimeMinutes = parseInt(data.minutes_before_launch) + parseInt(data.duration_minutes);
                
                if(data.minutes_before_launch <= 0 && liveTimeMinutes>0){
                    data.liveEvent = 'showLiveEvent'; 
                }
                else{    
                	data.liveEvent = 'hideEvent';
                } 
            }else{
            	var liveTimeHours = parseInt(data.hours_before_launch) + parseInt(data.duration);
                
                if(data.hours_before_launch <= 0 && liveTimeHours>0){
                    data.liveEvent = 'showLiveEvent'; 
                }
                else{    
                	data.liveEvent = 'hideEvent';
                } 
            } 
            
            if (data.event_category == 'live') {
                data.webinarEvent = 'showWebinarEvent';            	
            }                                                                                  
            else { 
                data.webinarEvent = 'showEventLocation';            	
            }  
            _.forEach(JSON.parse(configEle.eventUrls), function (eachUrl){
                if(data.sco_id == eachUrl.text){ 
                    data.eventDetailsPage = eachUrl.path+'.html';
                    return false;  
                }
            });
            
            if(!data.eventDetailsPage){
            	if(configEle.eventPath)
            		{
            		data.eventDetailsPage = configEle.eventPath+'.html?eventId='+data.sco_id;
            		}else{
            			data.url_path = data.url_path?data.url_path:'';
                    	if(data.url_path != '')
                    	{	
                        data.eventDetailsPage = data.url_path+'?eventId='+data.sco_id;
                    	}           			
            		}            	
                    }
            
            var dynamicTemplate = $($.trim(template));

            var temp = $("<div></div>");
            prescriptionSection = dynamicTemplate.find('.eventList-prescriptionItem').remove();
            if(data.prescribing_info_list.prescribing_info){
                $.each(data.prescribing_info_list.prescribing_info, function( index, value ) {
                    var tempSection = prescriptionSection.clone().addClass('eventList-presno'+index);
                    temp.html(tempSection);                
                    var tempText = temp.html().replace(/}}/g,index+'}}');
                    dynamicTemplate.find('.eventList-prescription').append(tempText);
                    var name = data.prescribing_info_list.prescribing_info[index]._name;
                    var url = data.prescribing_info_list.prescribing_info[index].url;
                    var fileSize = data.prescribing_info_list.prescribing_info[index].file_size;
                    data['prescribing_info_name'+index] = name?name:'';
                    data['prescribing_info_url'+index] = url?url:'';
                    data['prescribing_info_file_size'+index] = fileSize?fileSize:'';   
                });
            }
			temp.html(dynamicTemplate);
			
			var tempSpeaker = $("<span></span>");
			speakerSection = dynamicTemplate.find('.eventList-speakerItem').remove();
			dynamicTemplate.find('.eventList-speakerLabel').addClass('is-hidden');
			if(data.speakers.speaker){
                $.each(data.speakers.speaker, function( index, value ) {
                	var tempSection = speakerSection.clone().addClass('eventList-speakerItem-'+index);
                	tempSpeaker.html(tempSection);                
                    var tempText = tempSpeaker.html().replace(/}}/g,index+'}}');
                    dynamicTemplate.find('.eventList-speakerList').append(tempText);
                    var speakerName = data.speakers.speaker[index]._name;
                    if(index == 0){
						data['speaker_name'+index] = speakerName?speakerName:'';
                    }else{
						data['speaker_name'+index] = speakerName?", "+speakerName:'';
                    }
                    dynamicTemplate.find('.eventList-speakerLabel').removeClass('is-hidden');
                });
			}
			tempSpeaker.html(dynamicTemplate);
            data.prescribing_info_url = data.prescribing_info_url?data.prescribing_info_url:'';
            data.prescribing_info_name = data.prescribing_info_name?data.prescribing_info_name:'';
            data.prescribing_info_file_size = data.prescribing_info_file_size?data.prescribing_info_file_size:'';
            data.event_banner_small = data.event_banner_small?data.event_banner_small:'';
            data.event_banner_large = data.event_banner_large?data.event_banner_large:'';
            data.event_info = data.event_info?data.event_info:'';            
            data.description = data.description?data.description:'';
            data.date_begin = data.date_begin?data.date_begin:'';
            data.time_begin = data.time_begin?data.time_begin:'';
            data.date_end = data.date_end?data.date_end:'';
            data.time_end = data.time_end?data.time_end:'';
            data.eventDetailsPage = data.eventDetailsPage?data.eventDetailsPage:'';
            data.description = data.description.replace(/\\n/g,'<br>').replace(/\\/g,'');
            data.event_info = data.event_info.replace(/\\n/g,'<br>').replace(/\\/g,'');
            data.duration = data.duration?data.duration:'';
            data.duration_minutes = data.duration_minutes?data.duration_minutes:'';
            data.minutes_before_launch = data.minutes_before_launch?data.minutes_before_launch:'';
            data.index = index;
            data.speaker_name = data.speaker_name?data.speaker_name:'';
            data.language = api.getLanguageName(data.lang)?api.getLanguageName(data.lang):'';
            data.is_registration_limit_enabled = data.is_registration_limit_enabled?data.is_registration_limit_enabled:'';
            data.registration_limit = data.registration_limit?data.registration_limit:'';
            data.number_registered_users = data.number_registered_users?data.number_registered_users:'';
            data.seats_left = '';
            data.noSeatsLeft = '';
            data.seatsLeft = '';
            data.available_online_flag = '';
            if(data.is_registration_limit_enabled == "true"){
                data.seats_left = parseInt(data.registration_limit) - parseInt(data.number_registered_users);
                if(data.seats_left > 0){
                    data.noSeatsLeft = 'is-hidden';
                }else{
                    data.seatsLeft = 'is-hidden';
                }
            }else{
                data.noSeatsLeft = 'is-hidden';
                data.seatsLeft = 'is-hidden';
            }
            if(data.available_online == "false"){
                data.available_online_flag = 'is-hidden';
            }

            try{ 
                if(configEle.eventCount > configEle.countValue || configEle.eventCount === ''){

                    output += _.template(temp.html(), data);
                    output += _.template(tempSpeaker.html(), data);
                    configEle.countValue = configEle.countValue + 1;
                    
                }       
            }
            catch(e){
            }  
        } 
        
    });
    if(output!=''){
        currentParentDiv.find('.eventListContainer ul').append(output).addClass('hasEvents'); 
        currentParentDiv.find('.eventListContainer ul li:even').addClass('odd');
        currentParentDiv.find('.eventListContainer ul li:odd').addClass('even');
    }
    config.outCount = 0;
};
api.addFilter = function (filters, currentParentDiv,formdata) {
    var output = '';
    var filterContent = '';
    if(filters){
        var grouped = _.groupBy(filters.tag, function (n) {
            return n.group_type;
        });
        var renderSub = _.template(currentParentDiv.find('.event-filter-checkbox').text());
        _.forEach(grouped, function (eventgroup, groupIndex) {
            var groupNameArr = []; 
            var formatArr = [];
            var eventArr = [];
            _.forEach(eventgroup, function (event, index) {
                
                var value = ((event.tag_name).split("::"))[0];
                if($.inArray(value,groupNameArr) > 0){
                }else{
                    groupNameArr.push(value);
                    if((event.tag_name).indexOf("::") > 0){       
                        output += '<fieldset>';
                        output += '<legend class="event-group-heading">' + value + '</legend>'+'<div class="searchFilter-container">';
                        _.forEach(eventgroup, function (event, index) {
                            if(((event.tag_name).split("::"))[0] == value){
                                event.tag_name_split = ((event.tag_name).split("::"))[1];
                                output += renderSub(event);
                            }
                        });
                        output += '</div></fieldset>';
                    }
                    else if((event.tag_name).indexOf("::") < 0){
                        event.tag_name_split = event.tag_name;
                        eventArr.push(event);
                    }
                }
            });
            output += '<fieldset>';
            output += '<legend class="event-group-heading">' + 'format' + '</legend><div class="searchFilter-container">';
            _.forEach(eventArr, function (eachEvent, index) {
                output += renderSub(eachEvent);
            });
            output += '</div></fieldset>';
        });
        currentParentDiv.find('.event-filter-container form .event-filter-taggroup').html(output);
    }
    api.filter(currentParentDiv);
};
api.filter = function (currentParentDiv) {    
    _.forEach(formdata, function (filter) {
        if (filter.name == 'tag-id') {
        	currentParentDiv.find('input#' + filter.value).prop('checked', true);
        }
    });
    currentParentDiv.find('.event-filter-format,.event-filter-date,.event-filter-button').show();
    currentParentDiv.find('.event-filter-date #StartDate,.event-filter-date #EndDate').datepicker({
        dateFormat: 'yy-mm-dd'
    });
    currentParentDiv.find('a.event-filter-submit').unbind('click');
    currentParentDiv.find('a.event-filter-submit').click(function (e) {
        e.preventDefault();
        filterdata = currentParentDiv.find('.event-filter-form').serializeArray();        
		var sendVal = {};
		sendVal.$scope = $(currentParentDiv);
		api.onRegister(sendVal);
    });
};

api.cleanUp = function(currentParentDiv,config){   
    currentParentDiv.find('.eventListContainer ul').find('.loading').hide(); 
    if(!currentParentDiv.find('.eventListContainer ul').hasClass('hasEvents')){
        currentParentDiv.find('.eventListContainer ul').append(config.noEvent);
    }    
};

api.getLanguageName = function(key) {
	var isoLangs = {
		"zh" : {
			"name" : "Chinese",
			"nativeName" : "中文 (Zhōngwén), 汉语, 漢語"
		},
		"en" : {
			"name" : "English",
			"nativeName" : "English"
		},
		"fr" : {
			"name" : "French",
			"nativeName" : "français, langue française"
		},
		"de" : {
			"name" : "German",
			"nativeName" : "Deutsch"
		},
		"it" : {
			"name" : "Italian",
			"nativeName" : "Italiano"
		},
		"ja" : {
			"name" : "Japanese",
			"nativeName" : "日本語 (にほんご／にっぽんご)"
		},
		"nl" : {
			"name" : "Dutch",
			"nativeName" : "Nederlands, Vlaams"
		}
	}

	key = key.slice(0, 2);
	var lang = isoLangs[key];
	return lang ? lang.name : key;
};

    Cog.registerComponent({
        name: "eventlist",
        api: api,
        selector: ".eventlist"
    });

})(Cog.jQuery());

