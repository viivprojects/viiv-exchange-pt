(function($) {
    var api = {};  
    api.onRegister = function(element) { 
	    var $manageuserprofile = element.$scope;
	    $manageuserprofile.each(function() {
            $(this).on("click", ".managerUserProfileList li a", api.onShowUserClick);
            $(this).on("click", ".manageUserOverlayContainer .overlay-close", api.OverlayClose);
            $(this).on("click", ".manageUserOverlayContainer .overlay-bg", api.OverlayBGClose);
        }); 
    };
    api.OverlayClose = function (event, triggered) {$('.overlay-bg').hide();};
    api.OverlayBGClose = function (event, triggered) {$('.overlay-bg').hide();};
  	api.onShowUserClick = function (event, triggered) {
        event.preventDefault(); 
        var docHeight = $(document).height();
        var scrollTop = $(window).scrollTop();   
        var userID = $(this).attr("class"); 
 		$("."+userID+".manageUserOverlayContainer .overlay-bg").show().css({'height': docHeight});
        $('.manageUserOverlayContainer .overlay-content').css({'top': scrollTop + 80 + 'px'});
	 };

Cog.registerComponent({
    name: "manageuserprofile",
    api: api,
    selector: ".manageuserprofile"
});
})(Cog.jQuery());