(function($) {
    var api = {},content,currentElement;
    var options = {};
    var path = ''; 
    var surveyType, surveyID , surveyError , surveyDone;
    var counter = 0;
	var cookieValue = [];
	var csrfToken = '';
    api.onRegister = function(elements) { 
        var $poll = elements.$scope;    
        $poll.find('.pollParentDiv').each(function() {
	        currentElement = $(this).children('.pollOptionsContainer');
	        content = $(this).children('.poll-description');
	        path = content.data("providerPath");
	        surveyType = content.data("surveyType");
	        var isClosed = content.data("isClosed");
	        var pollText = content.data("pollText");
	        var optionsDisplay = content.data("optionsDisplay");
	        var surveyCodeArray = [];  
	        surveyCodeArray = content.data("surveyCode").split('-');
	        var surveyCode = surveyCodeArray[0];
	        surveyError = content.data("surveyError");
	        surveyDone = content.data("surveyDone"); 
	        if (isClosed == true) { 
	            $(this).children('.pollClosedText').html(pollText);
	        } 
	        else {
	            if ($poll.length > 0) {
	            $.get(window.location.pathname.split('.')[0]+'.token.json').success(function(json){
                    if(json.token || typeof json.token === "undefined"){
                        csrfToken = json.token;

                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            data:{"surveyCode":surveyCode,":cq_csrf_token":csrfToken},
                            context : currentElement,
                            url: path + '.getallsurveyresults.json',
                            success: function(data) {

                                var surveyFlag = 'false';
                                var element1 = this;
                                var radioOptions;
                                var p = element1.prev().data('providerPath');
                                _.forEach(data[1], function(surveys) {

                                    if(surveys.CheckOption != ''){
                                        radioOptions = surveys.CheckOption;
                                    }
                                    else if(surveys.RadioOption != ''){
                                        radioOptions = surveys.RadioOption;
                                    }

                                    options = radioOptions.split(',');
                                    if(surveys.SurveyCode == surveyCode) {
                                        element1.append('<div id=' + surveys.SurveyCode + '><div id='+ surveys.QuestionCode + '><h4>' + surveys.QuestionName + '</h4>');
                                        _.forEach(options, function(option) {
                                            if(optionsDisplay == 'radio') {
                                                element1.append('<div class="genericPollGroup"><span class="pollControl-radio">'
                                                + '<input id='+ option +' class="poll-option" type="radio" name="'+ surveys.SurveyCode +'" value="' + option + '"/></span>'
                                                + '<label for='+ option +' class = "poll-option-label">' + option) + '</label>';
                                            }
                                            else if(optionsDisplay == 'button') {
                                                element1.append('<div class="genericPollGroup"><span class="pollControl-button">'
                                                +'<button type="button" class="pollOption cfButton" name="'+ surveys.SurveyCode +'" value="' + option + '">'+option+'</button></span>');
                                            }
                                            else if(optionsDisplay == 'checkbox') {
                                                element1.append('<div class="genericPollGroup"><span class="pollControl-checkbox">'
                                                + '<input id='+ option +' class="poll-option" type="checkbox" name="'+ surveys.SurveyCode +'" value="' + option + '"/></span>'
                                                + '<label for='+ option +' class = "poll-option-label">' + option) + '</label>';
                                            }
                                        });
                                        element1.append('</div></div></div>');
                                    }
                                });
                                surveyID = element1.find('div:eq(0)').attr('id');
                                if(Cog.Cookie.read('polltoken') != null){
                                    if(Cog.Cookie.read('polltoken').indexOf(surveyID) != -1 ) {
                                    surveyFlag = 'true';
                                    }
                                }

                                if(surveyFlag == 'true') {
                                    var response = {};
                                    var percentageCount = 0;
                                    $.ajax({
                                        type: "POST",
                                        data: {
                                            "surveyCode": surveyID,
                                            ":cq_csrf_token":csrfToken
                                        },
                                        context : currentElement,
                                        dataType: "json",
                                        url: p + '.getsurveyresults.json',
                                        success: function(data) {
                                            _.forEach(data, function(eachData) {
                                                response[eachData[1].value] = eachData[2].value;
                                                percentageCount = percentageCount+parseInt(eachData[2].value,10);
                                            });

                                            element1.siblings('.pollResultsContainer:eq(0)').html('');
                                            $.each(response, function(key, value) {
                                                var percent = ((value/percentageCount)*100).toFixed(0);
                                                element1.siblings('.pollResultsContainer:eq(0)').append("<h4><span class='pollTitle grid_2'>"+ key + "</span><span class = 'pollResultValue' style=width:"+percent+"%>" + percent+"%</span></h4>");
                                            });
                                        }
                                   });
                                }
                                api.bindButtonClick();
                            }
                        });
                    }
                });


	            }
            }

         }); 

	api.bindButtonClick = function() {
	
		$poll.find('button.pollOption').click(function() {
			var selectedOption = $(this).val();
			var id = $(this).parents('.pollOptionsContainer:eq(0)').find('div:eq(0)').attr('id');
			var questionCode = $(this).parents('.pollOptionsContainer:eq(0)').find('div:eq(1)').attr('id');
			api.onPollSubmit($(this).parents('.pollOptionsContainer:eq(0)'),selectedOption,id,questionCode);
			}); 
		}

		$poll.find('button.showPollResult').click(function() { 
			var selectedOption = null,id=null,questionCode=null;
			selectedOption = $(this).prev().find('input[type=radio]:checked').val();
			id = $(this).prev().find('input[type=radio]:checked').parents('.pollOptionsContainer:eq(0)').find('div:eq(0)').attr('id'); 
			questionCode = $(this).prev().find('input[type=radio]:checked').parents('.pollOptionsContainer:eq(0)').find('div:eq(1)').attr('id'); 
			
			if(selectedOption == null || id == null) {
				id = $(this).prev().find('input[type=checkbox]:checked').parents('.pollOptionsContainer:eq(0)').find('div:eq(0)').attr('id');
				questionCode = $(this).prev().find('input[type=checkbox]:checked').parents('.pollOptionsContainer:eq(0)').find('div:eq(1)').attr('id');
				var selectedChecks = []; 
				$(this).prev().find('input[type=checkbox]:checked').each(function() {
					selectedChecks.push($(this).val());
				});
				selectedOption = selectedChecks.join(',');
			} 
		
			api.onPollSubmit($(this).prev(),selectedOption,id,questionCode); 
		});
	
	}; 

    api.onPollSubmit = function(eleme,selectedOption,id,questionCode) {

        var p = eleme.siblings('.poll-description:eq(0)').data('providerPath');
        var selectedOption = selectedOption;
        var id = id;
        var questionCode = questionCode;
        var surveyFlag = 'false';
        var currentEle = $(eleme).siblings('.pollResultsContainer');
        cookieValue = JSON.parse(Cog.Cookie.read('polltoken'));        
        var response = {};
        if(cookieValue == null) {
    		cookieValue = [];
        } 
		if(cookieValue.indexOf(id) != -1) {
			$(eleme).siblings('.pollError').html('<p>'+surveyDone+'</p>');
	        surveyFlag = 'true';
		}
		
        var savePath, surveyName;
        if (surveyType == "anonymous") {
            savePath = ".saveanonymoussurvey.json";
            surveyName = "surveyService";
        } else if (surveyType == "loggedIn") {
            savePath = ".savesurvey.json";
            surveyName = "surveyServiceLoggedIn";
        }        
        if (surveyFlag == 'false') {
            $.ajax({
                type: "POST",
                dataType: "json",
                context : currentEle,
                url: p + savePath,
                data: {
                    "surveyEntityName": surveyName,
                    "surveyCode": id,
                    "questionCode": questionCode,
                    "pollOptions": selectedOption,
                    ":cq_csrf_token":csrfToken
                },  
                success: function(data) {   

                    var resultElement = this;

					cookieValue = JSON.parse(Cog.Cookie.read('polltoken'));
                    if(cookieValue == null || cookieValue == 'undefined') {
                        cookieValue = [];
                    }
                    if(cookieValue.indexOf(id) == -1) {
                    	cookieValue.push(id);	
                    } 
	                if(selectedOption.length >0) {
	                	document.cookie = "polltoken="+JSON.stringify(cookieValue);+"; expires=-1; path=/";                	
	                }
                    var jsonString = JSON.stringify(data);					
                    if (jsonString != '{}' && jsonString.indexOf("error")!=-1) {
                    	var error = '<p>'+surveyError+'</p>';
                       $(eleme).siblings('.pollError').html(error);
                    } else {  
                        var percentageCount = 0;
                        $.ajax({
                            type: "POST",
                            data: {
                                "surveyCode": id,":cq_csrf_token":csrfToken
                            },
                            dataType: "json",
                            url: p + '.getsurveyresults.json',
                            success: function(data) {                          
                                _.forEach(data, function(eachData) {
                                    response[eachData[1].value] = eachData[2].value;
                                    percentageCount = percentageCount+parseInt(eachData[2].value,10);
                                }); 
                                resultElement.html('');
                                $.each(response, function(key, value) {
                                   var percent = ((value/percentageCount)*100).toFixed(0);
                                  resultElement.append("<h4><span class='pollTitle grid_2'>"+ key + "</span><span class = 'pollResultValue' style=width:"+percent+"%>" + percent+"%</span></h4>");
                                });

                            }
                        });
                    }
                }

            });

        }
    };

	Cog.registerComponent({
	    name: "poll",
	    api: api,
	    selector: ".poll"
	});

})(Cog.jQuery());
