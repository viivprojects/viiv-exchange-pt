(function ($) {
    var api = {};
    api.config = {};
    
    api.onRegister = function (element) {
        api.config = element.$scope.find('.user-box').data();
        Cog.addListener("gigyaEventHandler", "userProfileChanged", function (event) {
            var loggedIn = element.$scope.find(".logged-in")[0];
            var loggedOut = element.$scope.find(".logged-out")[0];
            if (event.eventData === undefined || event.eventData.loggedOut || $.isEmptyObject(event.eventData)) {
                $('.user-box.user-type-gigya').css("display", "block");
                loggedOut.style.display = "";
                loggedIn.style.display = "none";
                if(api.config.deeplinkUsername){
                    loggedOut.style.display = "none";
                    loggedIn.style.display = "";
                    var tokenUserName = $();
                    $('.user-box .logged-in p span').last().after(api.config.deeplinkUsername);
                    $('.user-box .logged-in p span, .user-box .logged-in a').remove();
                }
                return;
            }
            var profileData = {};
            profileData.firstName = '';
            profileData.title = '';
            profileData.lastName = '';
            profileData.email = '';
            profileData.speciality = '';
            
            _.assign(profileData, event.eventData);
            profileData.emailAddress = profileData.email;
            
            var oldSettings = _.templateSettings.interpolate;
            _.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
            loggedIn.innerHTML = _.template(loggedIn.innerHTML)(profileData);
            _.templateSettings.interpolate = oldSettings;
            $('.user-box.user-type-gigya').css("display", "block");
            loggedIn.style.display = "";
            loggedOut.style.display = "none";
            element.$scope.find('.logged-in form').submit(function () { 
                try {
                    gigya.accounts.logout();
                }catch (err){
                    Cog.Cookie.create('logoutGigya', 'true');
                }
            });
        });
    };
    api.init = function (elements) {
        if (elements.length > 0) {
            this.loadGigyaLibrary();
        }
    };
    
    api.loadGigyaLibrary = function () {
        if (window.loadedGigya && window.loadedGigyaJS) {
            api.getAccountInfo();
            api.bindLoginEvent();
        } else if(!window.loadedGigya) {
            window.loadedGigya = true;
            $.getScript(api.config.serviceUrl + '?apikey=' + api.config.apiKey, function () {
                window.loadedGigyaJS = true;
                api.getAccountInfo();
                api.bindLoginEvent();
                cf.getAccountInfo();
            });
        }
    };
    
    api.getAccountInfoResponse = function (response) {
        Cog.fireEvent("gigyaEventHandler", "userProfileChanged", response.profile);
    };
    
    api.getAccountInfo = function () {
        var $getAccountInfoResponse = api.getAccountInfoResponse;
        gigya.accounts.getAccountInfo({
            callback: $getAccountInfoResponse
        });
    };
    
    api.loginEventHandler = function(eventObj) {
    	var currentWindowPath = window.location.pathname.split('.')[0];
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: currentWindowPath +
            '/j_security_check?resource=' + currentWindowPath,
            data: {
            'data': eventObj
        },
               success: function(data) {
            window.location.pathname = data.redirectTarget + '.html';
        }
    });  
    
};
 
 api.bindLoginEvent = function(){    
    var $loginHandler = api.loginEventHandler;
    gigya.accounts.addEventHandlers({
        onLogin: $loginHandler
    });
};

Cog.registerComponent({
    name: "userBox",
    api: api,
    selector: ".userBox"
});

})(Cog.jQuery());
