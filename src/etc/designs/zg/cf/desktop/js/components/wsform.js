/*global _, ZG, ZGjQuery, Recaptcha */
(function ($, _) {
    'use strict';
    
    var api = {};

    /**
     * Creates the form object
     *
     * @constructor
     */
    function WsForm($el) {
        this.setEls($el);
        this.init();
        this.populate();
        this.loadCsrfToken();
    }



    WsForm.prototype = Object.create(Cog.component.form.prototype, WsForm.prototype);


    WsForm.prototype.onServerResponse = function (data) {
        var isValid = data.operationSuccessful;

        this.beenSubmitted = false;
        this.$el.find('.wsform-hint-server').hide();
        this.$el.find('.wsform-hint').removeClass('has-message');

        // check each field
        if (data.responseFields) {
            _.forEach(data.responseFields, _.bind(function (item) {
               
                if (item.errorCode) {
                    this.handleResponseFields(item, data.debug);
                }
            }, this));
        }
        if (isValid) {
            this.onSuccess();
        } else {
            this.reloadCaptcha();
            this.onServerValidationFailure(data);
        }
    }; 

    /**
     * Handle response fields errors display
     *
     * @param item {object}
     * @param debug {boolean}
     */
    WsForm.prototype.handleResponseFields = function (item, debug) {
        var errorHintSelector = '.wsform-hint.' + item.categoryCode + '.' + item.attributeCode,
            errorDescriptionSelector = '.wsform-hint-server.' + item.categoryCode + '.' + item.attributeCode,
            $errorHint = this.$el.find(errorHintSelector),
            $errorDescription = this.$el.find(errorDescriptionSelector);

        $errorHint.addClass('has-message').html(item.errorDescription);
        $errorDescription.hide().html(item.errorDescription);

        if (Boolean(debug)) {
            $errorDescription.show();
        }
    };

    /**
     * Clean serialization from element which should be removed
     *
     * @param serializedData {array} serialized array of data from form
     * @returns {array} cleaned serialization from not needed elements
     */
    WsForm.prototype.cleanSerializationSubs = function (serializedData) {
        var cleanSerialization = serializedData || this.$form.serializeArray(),
            $childrenMatcherToRemove = this.$form.find('[data-childmatcher=subscription]');

        $childrenMatcherToRemove.each(function () {
            var $this = $(this),
                i;

            for (i = 0; i < cleanSerialization.length; i++) {
                if (cleanSerialization[i].name === $this.attr('name')) {
                    cleanSerialization.splice(i, 1);
                }
            }
        });

        return cleanSerialization;
    };

    /**
     * Cleans serialization from unnecessary elements
     * removes subscriptions and channel
     *
     * @returns {*}
     */
    WsForm.prototype.cleanSerialization = function () {
        var cleanedSerialization;

        cleanedSerialization = this.cleanSerializationSubs();
        cleanedSerialization = this.cleanSerializationChannel(cleanedSerialization);

        return cleanedSerialization;
    };

    /**
     * Removes serialization value connected to channel
     *
     * @param cleanedSerialization
     * @returns {array}
     */
    WsForm.prototype.cleanSerializationChannel = function (cleanedSerialization) {
        var childrenMatcher = this.$form.find('[data-parentmatcher=subscription]').attr('name'),
            cleanedSerializationData;

        cleanedSerializationData = this.removeProperities(cleanedSerialization, childrenMatcher);

        return cleanedSerializationData;
    };

    /**
     * Gets status from DOM elements
     *
     * @returns {*[]}
     */
    WsForm.prototype.getStatus = function () {
        var $formConst = this.$form.find('.wsform-const'),
            status;

        status = [$formConst.data('unsub'), $formConst.data('sub')];

        return status;
    };

    /**
     * Custom serialization of element
     *
     * @returns {*}
     */
    WsForm.prototype.customSerialization = function () {
        var cleanedSerialization = this.cleanSerialization(),
            status = this.getStatus(),
            subscriptionData = this.getSubscriptionData(),
            rawSubscriptionKeys = [],
            updatedSerialization,
            newAttributes,
            i;

        for (i = 0; i < subscriptionData.length; i++) {
            rawSubscriptionKeys.push(this.generateKeys(subscriptionData[i]));
        }

        newAttributes = this.createAnswerKeys(rawSubscriptionKeys, status);
        updatedSerialization = cleanedSerialization.concat(newAttributes);

        return $.param(updatedSerialization);
    };

    /**
     * Creates custom answer names
     *
     * @param partials {array} partials to create new values
     * @param status {array} hardcoded values in DOM
     *
     * @returns {array}
     */
    WsForm.prototype.createAnswerKeys = function (partials, status) {
        var newKeys = [],
            newKeysTemp = [],
            statusField,
            i;

        for (i = 0; i < partials.length; i++) {
            statusField = 0;

            if (partials[i].state) {
                statusField = 1;
            }

            newKeysTemp = this.createCustomKeys(partials[i], status[statusField]);
            newKeys = newKeys.concat(newKeysTemp);
        }

        return newKeys;
    };

    /**
     * Creates custom keys from parts
     *
     * @param parts {object} parts from subscription checboxes
     * @param status {string} name of status
     * @returns {Array} new key of names
     */
    WsForm.prototype.createCustomKeys = function (parts, status) {
        var channelValue = this.generateParentValue(),
            nameEl = parts.cat + '+' + parts.attr + '+' + parts.id,
            nameElStatus = parts.cat + '+A_STATUS+' + parts.id,
            nameChannel = parts.cat + '+' + channelValue + '+' + parts.id,
            childrenMatcherValue = this.$form.find('[data-parentmatcher=subscription]').attr('value'),
            newKeys = [];

        this.addToCustomArray(nameEl, parts.id, newKeys);
        this.addToCustomArray(nameElStatus, status, newKeys);
        this.addToCustomArray(nameChannel, childrenMatcherValue, newKeys);

        return newKeys;
    };

    /**
     * Add object with name and value to array
     *
     * @param name {string}
     * @param value {string}
     * @param addTo {array} where object should be pushed
     */
    WsForm.prototype.addToCustomArray = function (name, value, addTo) {
        if (_.isArray(addTo)) {
            addTo.push({
                name: name,
                value: value
            });
        }
    };

    /**
     * Generates channel name
     *
     * @returns {string} part of channel name
     */
    WsForm.prototype.generateParentValue = function () {
        var matcherName = this.$form.find('[data-parentmatcher=subscription]').attr('name'),
            matcherNameSplit = matcherName.split('+');

        return matcherNameSplit[1];
    };

    /**
     * Removes properties from serialized array
     * duplicates that will be generated
     *
     * @param formData {array} serialization value
     * @param removeMe {string} name of url to be removed
     * @returns {array} updated array for serialization
     */
    WsForm.prototype.removeProperities = function (formData, removeMe) {
        var newData = [],
            i;

        for (i = 0; i < formData.length; i++) {
            if (formData[i].name !== removeMe) {
                newData.push(formData[i]);
            }
        }

        return newData;
    };

    /**
     * Generates dataMap from subKeys array
     *
     * @param subKeysTemp {object} array of meta data of element
     * @param value {string}
     * @returns {{cat: *, attr: *, id: *, value: *}} object/map of element meta data
     */
    WsForm.prototype.generateDataMap = function (subKeysTemp, state) {
        var dataMap = {
            'cat': subKeysTemp[0],
            'attr': subKeysTemp[1],
            'id': subKeysTemp[2],
            'state': state
        };

        return dataMap;
    };

    /**
     * Generates keys from element name and value
     *
     * @param subData {object} holds element meta data
     * @returns {object} segregated data of element
     */
    WsForm.prototype.generateKeys = function (subData) {
        var keysTemp,
            keys;

        if (subData.name) {
            keysTemp = subData.name.split('+');
            keys = this.generateDataMap(keysTemp, subData.state);
        }

        return keys;
    };

    /**
     * Gets data from subscription form elements
     *
     * @returns {Array}
     */
    WsForm.prototype.getSubscriptionData = function () {
        var subscriptionOn = [];

        this.$form.find('[data-childmatcher=subscription]').each(function () {
            var $this = $(this),
                options = {
                    value: $this.val(),
                    name: $this.attr('name'),
                    state: $this.prop('checked')
                };

            subscriptionOn.push(options);
        });

        return subscriptionOn;
    };

    /**
     * Populate form fields
     */
    WsForm.prototype.populate = function () {
        var url = this.$form.data('populate-url');

        if (!url || this.beenPopulated) {
            return false;
        }

        this.populateFields(url);
        this.beenPopulated = true;
    };

    /**
     * Gets data from url and populates form with it
     *
     * @param url {string}
     */
    WsForm.prototype.populateFields = function (url) {
        $.getJSON(url + '?ts=' + (new Date().getTime())).done(function (data) {
            $.each(data.parameters, function(i, parameter) {
                var key = parameter.key.replace(/\+/g,'\\+'),
                    value = parameter.value,
                    fieldType = parameter.fieldType,
                    $el = $('[name=' + key + ']'),
                    isChecked = false;
                switch(fieldType) {
                    case 'CHECKBOX':
                        if(value === 'Y'){
                            isChecked =   true;
                        }
                        $el.prop('checked', isChecked);
                        break;
                    case 'SELECT':
                        $el.find('option:selected').prop('selected', false);
                        $el.find('option[value=' + value + ']').prop('selected', true);
                        break;
                    case 'SUBSCRIPTIONS':
                        if(value === 'L_SUBC'){
                            isChecked =   true;
                        }
                        $el.prop('checked', isChecked);
                        break;
                    case 'SUBSCRIPTIONS_CHANNEL':
                        $el.filter('[value=' + value + ']').prop('checked', true);
                        break;
                    default:
                        $el.val(value);
                        break;
                }
            });
        });
    };

    /**
     * Handles submit click
     *
     * @param ev {object} event
     * @returns {boolean}
     */
    WsForm.prototype.onSubmit = function (ev) {
        if(!this.canSubmit()){
            return false;
        }

        this.clearValidation();

        if (!this.isClientValid() || !this.isPolicyChecked()) {
            this.onClientValidationFailure();
            this.onPolicyValidationFailure();
        } else {
            this.handleAjaxCall();
        }

        this.stopEvent(ev);
    };

    /**
     * Create custom serialization or default from jQuery
     *
     * @returns {string} serialized data from form
     */
    WsForm.prototype.getSerializationData = function () {
        var serializedData;

        if (this.$form.find('[data-parentmatcher=subscription]').length) {
            serializedData = this.customSerialization();
        } else {
            serializedData = this.$form.serialize();
        }
        return serializedData;
    };

    /**
     * Checks if policy exists and what is it's value
     *
     * @returns {*}
     */
    WsForm.prototype.isPolicyChecked = function () {
        var $policy = this.$form.find('.privacyPolicy'),
            $policyInput = true;

        if ($policy.length) {
            $policyInput = $policy.find('[type=checkbox]').prop('checked');
        }

        return $policyInput;
    };

    /**
     * Handles privacy policy validation failure
     */
    WsForm.prototype.onPolicyValidationFailure = function () {
        var $policy = this.$form.find('.privacyPolicy'),
            $policyHint = $policy.find('.wsform-hint');

        if (!this.isPolicyChecked()) {
            $policyHint.show();
        }
    };

    /**
     * Reloads captcha
     */
    WsForm.prototype.reloadCaptcha = function () {
        if (!window.Recaptcha) {
            return false;
        }

        window.Recaptcha.reload();
    };
    
    /**
     * load CSRF token
     */
    WsForm.prototype.loadCsrfToken = function () {      
        $.get(window.location.pathname.split('.')[0]+'.token.json').success(function(json){          
            $('form.wsform-form').append('<input name=":cq_csrf_token" type="hidden" value="'+json.token+'"/>');
             
        });   
    };

    api.forms = [];
	
    api.onRegister = function (elements) {
		var $forms = elements.$scope;
        $forms.each(function () {
            var $form = $(this);

            var wsform = new WsForm($forms);
            api.forms.push(wsform);
            $form.addClass('initialized');
        });
    };    
    Cog.registerComponent({
        name: 'wsForm',
        selector: '.component.wsform',
        api: api
    });
})(Cog.jQuery(),_); 