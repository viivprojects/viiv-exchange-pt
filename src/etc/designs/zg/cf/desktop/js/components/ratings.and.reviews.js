(function ($, document, _) {
    "use strict";

    var api = {},
        defaults = {
            customRatings : ['effectiveness', 'great-taste', 'value']
        };

    var userid = Cog.Cookie.read("userid");

    if (userid == null) {
        var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
        Cog.Cookie.create("userid", guid);
    };

    function RatingsAndReviews($element) {

        this.$el = $element;
        this.$htmlReviews = this.$el.find(".html-reviews");
        this.$container = this.$el.closest(".box-ratingsAndReviews-container");
        this.$header = $("#header .paragraph-header");

        this.mode = this.$htmlReviews.data("mode");

        this.offset = 0;
        this.totalResults = 0;

        this.config = {
            reviewBox : this.$htmlReviews,
            apiKey : this.$htmlReviews.data("api-key"),
            productId : this.$htmlReviews.data("product-id"),
            cookieName : this.$htmlReviews.data("cookie-name"),
            loadmorecount : this.$htmlReviews.data("load-more-count"),
            loadstartcount : this.$htmlReviews.data("load-start-count"),
            sortOrder : this.$htmlReviews.data("review-sort-order"),
            reviewDisplayServer : this.$htmlReviews.data("api-environment") + "data/reviews.json",
            reviewSubmitServer : this.$htmlReviews.data("api-environment") + "data/submitreview.json"
        };

        if(this.mode === "display") {

            this.$loadMoreButton = this.$el.find(".loadMore > a");
            this.$reviewsCounter = this.$container.find(".ratingsAndReviews-reviewsCounter");
            this.template = this.$htmlReviews.siblings(".reviews-display-template").html();

            this.displayReviews(this.config, this.config.loadstartcount);

        } else if(this.mode === "submit") {

            this.template = this.$htmlReviews.siblings(".reviews-submit-template").html();

            this.submitReview(this.config);

            if (this.isReviewSubmitted()) {
                this.showSubmissionConfirmation();
            } else {
                this.showSubmissionForm();
            }

        } else if (this.mode == "overall") {
             this.template = this.$htmlReviews.siblings(".reviews-overall-template").html();
             this.displayOverallRatings($element, this.config);
        }

        this.bindUIEvents();
    }

    RatingsAndReviews.prototype = {

        bindUIEvents: function () {

            var self = this;

            if (self.mode === "display") {
                self.$loadMoreButton.on("click", function (event) {
                    self.displayReviews(self.config, self.config.loadmorecount);
                    event.preventDefault();
                });
            }
        },

        isReviewSubmitted: function () {
            var isCookie = Cog.Cookie.read(this.config.cookieName);

            return !(isCookie == null);
        },

        showSubmissionForm: function () {
            this.$htmlReviews.find(".reviewSubmittedText").hide();
        },

        showSubmissionConfirmation: function () {
            $(document).find(".ratingsAndReviews .html-reviews[data-product-id='" + this.config.productId + "'] .review-submit-form").each(function() {
                var $element = $(this).find(".reviewSubmittedText").first(),
                    redraw = $('<div/>').html($element.html()).text();

                $element.html(redraw).show();
                $(this).find(".reviewBody").hide();
            });
        },

        displayReviews: function (config, reviewsLimit) {

            var self = this,
                limit = (reviewsLimit) ? reviewsLimit : config.loadmorecount

            $.getJSON(config.reviewDisplayServer + '?ApiVersion=5.4&Include=Authors&PassKey=' + config.apiKey +  '&Filter=ProductId:eq:' + config.productId + '&Sort=SubmissionTime:' + config.sortOrder + '&Offset=' + self.offset +  '&Limit=' + limit + '&callback=?',
                function (jsonResult) {

                    var reviewData = [],
                        model = {},
                        output;

                    if(jsonResult && jsonResult['Results']) {

                        _.each(jsonResult['Results'], function (review) {

                            var reviewTitle = review['Title'],
                                reviewRating = self.starsRatings(review['Rating'], review['RatingRange']),
                                reviewAuthorName = review['UserNickname'],
                                reviewDate = self.formatDate(review['LastModificationTime']),
                                reviewContent = review['ReviewText'];

                            reviewData.push({
                                'rating': reviewRating,
                                'title': reviewTitle,
                                'author': reviewAuthorName,
                                'date': reviewDate,
                                'desc': reviewContent
                            });
                        });

                        model['results'] = reviewData;

                        self.offset = self.offset + limit;
                        self.totalResults = jsonResult['TotalResults'];

                        self.setReviewsCounter();

                        if (self.offset >= self.totalResults) {
                            self.$loadMoreButton.hide();
                        }

                        // Render the template into a variable
                        output = _.template(self.template, model);

                        config.reviewBox.append(output);
                    }
                }
            );
        },

        submitReview: function (config) {

            var self = this,
                output = _.template(this.template),
                $reviewBox = config.reviewBox,
                $form,
                $submitButton;

            $reviewBox.append(output);

            $form = this.$el.find("form.submit-review-form").first();
            self.id = $form.attr("id").match(/[0-9]+$/g);
            $submitButton = $form.find("input[type='submit']");

            this.convertToStarsRatingInput($reviewBox);

            $reviewBox.on('submit', '#submit-review-form-'+self.id, function(event) {
                event.preventDefault();
                $submitButton.prop("disabled", true);

                var submitUrl = self.config.reviewSubmitServer,
                    userNickname = Cog.Cookie.read("usernickname") || $reviewBox.find("#review-user-"+self.id).val(),
                    params = {
                        ApiVersion: '5.4',
                        Action: 'Submit',
                        PassKey: config.apiKey,
                        ProductId: config.productId,
                        Rating: $reviewBox.find("#rating-" + self.id).val(),
                        ReviewText: $reviewBox.find("#review-text-" + self.id).val(),
                        Title: $reviewBox.find("#review-summary-" + self.id).val(),
                        UserId: Cog.Cookie.read("userid"),
                        UserNickname: userNickname
                    },
                    recommend = $reviewBox.find('input[name=review-recommend]').val(),
                    isIe8_Ie9 = (typeof new XMLHttpRequest().responseType != 'string' && window.XDomainRequest);

                submitUrl = self.config.reviewSubmitServer

                $.each (['effectiveness', 'great-taste', 'value'], function() {
                    var v = $reviewBox.find("#review-" + this + "-" + self.id).val();
                    if (!!v) {
                        params['Rating_' + this] = v;
                    }
                });

                if (typeof recommend != 'undefined') {
                    params.IsRecommended = recommend;
                }

                if (isIe8_Ie9) { //IE8 and IE9
                    submitUrl = window.location.protocol + "//" + window.location.host + $(this).attr("action");
                }

                $.ajax({
                    type: 'POST',
                    data: params,
                    url: submitUrl,
                    success: function(data) {

                        self.$el.find(".fieldWrapper .error").remove();

                        if (data['HasErrors']) {
                            var errorMessages = self.parseResults(self.$htmlReviews.siblings(".reviews-obtained-errors").html(), data);
                            _.each(errorMessages, function(error, index) {
                                self.$el.find("[data-bv-field-name='"+index+"']").append($.parseHTML(error));
                            });

                        } else {
                            self.showSubmissionConfirmation();
                            Cog.Cookie.create("gskreview" + config.productId, "true");
                            Cog.Cookie.create("usernickname", userNickname);
                            self.scrollAfterSubmit();
                        }
                    },error: function() {
                    },complete: function() {
                        $submitButton.prop("disabled", false);
                    }
                });
            });
        },

        parseResults: function($element, data) {

            var errorData = {}, model = {}, output = {};

            if (data && data['FormErrors']) {
                var formErrors = data['FormErrors'];
                if (formErrors['FieldErrors']) {
                    var errors = formErrors['FieldErrors'];

                    _.each(errors, function(error, index) {

                        var message = error['Message'];

                        errorData[index] = [];
                        errorData[index].push({
                            'message' : message
                        });

                        var code = error['Code'];
                        if ("ERROR_FORM_DUPLICATE" == code) {
                            $(".nickname").show();
                        }

                    });
                }

                _.each(errorData, function(data, index) {
                    model['results'] = errorData[index];
                    output[index] = _.template($element, model);
                })
            }

            return output;
        },

        displayOverallRatings: function($element, config) {
            var self = this;

            $.getJSON(config.reviewDisplayServer + '?ApiVersion=5.4&Include=Products&Stats=Reviews&PassKey=' + config.apiKey + '&Filter=ProductId:eq:' + config.productId + '&Limit=1', function (jsonResult) {
                var reviewData = [],
                    model = {},
                    stats = jsonResult.Includes.Products[config.productId].ReviewStatistics,
                    output;

                model.stats = stats;
                model.stars = {};
                model.stars.overall = self.starsRatings(stats.AverageOverallRating, stats.OverallRatingRange);

                $.each(defaults.customRatings, function() {
                     var r = stats.SecondaryRatingsAverages[this];
                     if (typeof r === 'undefined') {
                        return;
                     }
                     model.stars[this] = self.starsRatings(r.AverageRating, stats.OverallRatingRange);
                });

                // Render the template into a variable
                output = _.template(self.template, model);
                config.reviewBox.append(output);
            });
        },

        // Custom ratingsAndReviews functions

        setReviewsCounter: function() {
            this.$reviewsCounter.text("(" + this.totalResults + ")");
        },

        convertToStarsRatingInput: function($el) {

            var self = this,
                $reviewRatingSelect = $el.find("#rating-" + self.id),
                $starRatingList = $("<ul class='ratingHighlight' id='rating-list-" + self.id + "'></ul>"),
                defaultRating = 5,
                otherElementsToConvert = ["review-effectiveness", "review-great-taste", "review-value"];

            $reviewRatingSelect.find("option").each(function(index) {
                if (defaultRating > (index + 1)) {
                    $starRatingList.append("<li class='star star-filled'></li>");
                }
                else if (defaultRating == (index + 1)) {
                    $starRatingList.append("<li class='star star-filled selected'></li>");
                }
                else {
                    $starRatingList.append("<li class='star'></li>");
                }
            });

            $reviewRatingSelect.hide().val(defaultRating).closest(".fieldWrapper").addClass("inline-label").append($starRatingList);

            $el.on({
                mouseenter: function() {
                    $(this).prevAll().andSelf().addClass("star-filled");
                    $(this).nextAll().removeClass("star-filled");
                },
                mouseleave: function() {
                    var $selected = $(this).parent().find(".selected");
                    if ($selected.length) {
                        $selected.nextAll().removeClass("star-filled");
                        if (!$selected.is(".star-filled")) {
                            $selected.prevAll().andSelf().addClass("star-filled");
                        }
                    }
                },
                click: function() {
                    $(this).siblings().removeClass("selected");
                    $(this).addClass("selected");
                    $reviewRatingSelect.val(1 + $(this).index())
                }
            }, "#rating-list-" + self.id + " li");


            // Convert other elements

            _.each(otherElementsToConvert, function(index) {
                var $select = $el.find("#" + index + "-" + self.id),
                    ratingListId = index + "-list-" + self.id,
                    $ratingList = $starRatingList.clone().attr("id", ratingListId);

                $select.hide().val(defaultRating).closest(".fieldWrapper").addClass("inline-label").append($ratingList);

                $el.on({
                    mouseenter: function() {
                        $(this).prevAll().andSelf().addClass("star-filled");
                        $(this).nextAll().removeClass("star-filled");
                    },
                    mouseleave: function() {
                        var $selected = $(this).parent().find(".selected");
                        if ($selected.length) {
                            $selected.nextAll().removeClass("star-filled");
                            if (!$selected.is(".star-filled")) {
                                $selected.prevAll().andSelf().addClass("star-filled");
                            }
                        }
                    },
                    click: function() {
                        $(this).siblings().removeClass("selected");
                        $(this).addClass("selected");
                        $select.val(1 + $(this).index())
                    }
                }, "#" + ratingListId +" li");
            });
        },

        starsRatings: function (el, max) {
            var template = "",
                index;

            if (el && max) {
                for(index = 1; index < max + 1; index++) {
                    if (index <= el) {
                        template += '<li class="star-filled"></li>';
                    } else {
                        template += '<li class="star"></li>';
                    }
                }
                return template;
            }
        },

        formatDate: function(date) {
            return date.split('T')[0].split('-').reverse().join('/');
        },

        scrollAfterSubmit: function() {

            var offsetTop = this.$header.height() + 30; // 30 = $('#header .richText-navigation-level2').height();

            $('html, body').animate({
                scrollTop: this.$el.offset().top - offsetTop
            }, 300);
        }
    };

    api.items = [];

    api.onRegister = function ($elements) {
        $elements.each(function() {
            var item = new RatingsAndReviews($(this));
            api.items.push(item);
        });
    };

Cog.registerComponent({
    name: "bazaarVoiceRatingsAndReviews",
    api: api,
    selector: ".ratingsAndReviews"
});

})(Cog.jQuery());
