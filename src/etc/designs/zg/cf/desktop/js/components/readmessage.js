(function($,document,_) {
    var api = {}, readmessage="";  
    
    api.onRegister = function(elements) { 
	if(elements.$scope.length > 0){
	    var $readmessage = elements.$scope;
	    $readmessage.each(function() {
            $(this).on("click", ".messageResultContainer li a", api.onShowUserMessageClick);
			$(this).on("click", ".readMessageOverlayContainer .overlay-close", api.OverlayClose);
            $(this).on("click", ".readMessageOverlayContainer .overlay-bg", api.OverlayBGClose); 
        }); 
		var content = $(".readmessagaeContent");         
  		var configPath = content.data("pagePath");
        var messageSender = content.data("adminId");
        var entityName = content.data("entityName"); 
        var categoryCode = content.data("categoryCode"); 
        var successMsg = content.data("successMessage");
        var failureMsg = content.data("failureMessage");  
        var emailTemp = content.data("emailTemplate");
		$.ajax({
			type: 'POST',
            url: configPath,
            data: {"recipientId":messageSender,"selector":"search", "entityName":entityName, "categoryCode":categoryCode},
            success: function(jsonData) {   
				var template = $(".userMessagerTemplate")[0].innerHTML, output = "";
					_.templateSettings = {
						interpolate: /\{\{(.+?)\}\}/g,
						evaluate: /\{\%(.+?)\%\}/g
					}; 
					_.forEach(jsonData, function (data) { 
                         var messageID = data['A_MS_MESSAGEID'];
						 var recpID = data['A_MS_RECIPIENTID'];
                         var sendID = data['A_MS_SENDERID']; 
						 var readStatus = data['A_MS_READSTATUS'];
                         if(readStatus=="Yes"){
							var readStatusClassName="readMessageDone";
                         }
                         var messageTitle = messageID.split('-'),messgageTitleStCase=""; 
                       	 for(var i=0;i<messageTitle.length;i++){
							 messgageTitleStCase = messgageTitleStCase+" "+ messageTitle[i].substr(0,1).toUpperCase()+messageTitle[i].substr(1);
						 } 
                        output += _.template(template, {"messageid":messageID,"messageTitle":messgageTitleStCase,"recpID":recpID,"senderID":sendID,"readStatus":readStatusClassName}); 
					 });
					 $('.messageResultContainer ul.messageList').html(output);
            },
            dataType: 'json'
        }); 
}
    };
	api.OverlayClose = function (event, triggered) { 
        $('.overlay-bg').hide();
    };
    api.OverlayBGClose = function (event, triggered) { 
        $('.overlay-bg').hide();
    };
	api.onShowUserMessageClick = function (event, triggered) { 
		event.preventDefault();  
        var content = $(".readmessagaeContent");
        var emailTemp = content.data("emailTemplate");
        var messageID = emailTemp+"/"+$(this).attr("id")+"/_jcr_content/template.html";  
        var docHeight = $(document).height();
        var scrollTop = $(window).scrollTop();   
        var userID = $(this).attr("id");  
        var recpId = $(this).data("recpId");
        var senderId = $(this).data("senderId");

		 $.ajax({
			 url: messageID, 
			 success: function(data) { 
					$(".readMessageOverlayContainer .overlay-bg .overlayContainerDiv").html(data);
					$(".readMessageOverlayContainer .overlay-bg").show().css({'height': docHeight});
        			$(".readMessageOverlayContainer .overlay-content").css({'top': scrollTop + 80 + 'px'});  

			 },
			 dataType: 'html'
		 });  
         api.updateMessageRead(userID, recpId, senderId);  
	 };	 

	 api.updateMessageRead = function (userID, recpId, senderId) { 
		 var content = $(".readmessagaeContent");
         var configPath = content.data("pagePath");
         var entityName = content.data("entityName");
         var categoryCode = content.data("categoryCode"); 
         var today = new Date();
         var dd = today.getDate();
         var mm = today.getMonth()+1; //January is 0
         var yyyy = today.getFullYear();
         var hours = today.getHours();
         var minutes = today.getMinutes();
         var seconds = today.getSeconds();
         today = yyyy + "-" + mm + "-" + dd + " " + hours + ":" + minutes + ":" + seconds;
         
         $.ajax({
			type: 'POST',
            url: configPath,
            data: {"senderId": senderId, "recipientId": recpId, "messageId": userID, "selector":"update", "entityName":entityName, "categoryCode":categoryCode, "readStatus":"Yes", "readStatusDate":today},
            success: function(data) {},
            dataType: 'html'
        });
	 };	 
Cog.registerComponent({
    name: "readmessage",
    api: api,
    selector: ".readmessage"

});	 
 
})(Cog.jQuery(),document,_); 
