/**
 * Accordion
 */

(function($, _) {
	"use strict";

	var api = {};

	var state = "desktop";

	var config = {
		slideDuration: 300,
		defaultHeight: 300,
		slideWidth: 40,
		breakpoint: 768
	};

	function Accordion($el, options) {
		this.$el = $el;
		this.options = options;

		this.initialize();
	}

	Accordion.prototype = {
		initialize: function() {
			this.bindUIEvents();

			this.$slides = this.$el.find(".accordion-slide");
			this.$slidesContents = this.$el.find(".accordion-content-wrapper");

			if (this.options.horizontal) {
				$(window).on("resize", _.throttle(function() {
					this.resizeView();
				}.bind(this), 100));
				this.resizeView();
			}

			this.$el.find(".accordion-head.is-active").trigger("click", ["true"]);
		},

		setSlideDimensions: function() {
			var slideContentWidth = this.$el.outerWidth() - (this.$slides.size() - 1) * (config.slideWidth - 3),
				maxHeight = 300;

			this.$slidesContents.each(function() {
				$(this).css("width", slideContentWidth);

				var contentHeight = $(this).children(".accordion-content")[0].scrollHeight;

				if (contentHeight > maxHeight) {
					maxHeight = contentHeight;
				}
			});

			this.$el.find(".accordion-slide.is-active").css("width", slideContentWidth);

			if (maxHeight > config.defaultHeight) {
				_.each(this.$slides, function(el) {
					$(el).css("height", maxHeight);
				});
			}
		},

		restoreSlideDimensions: function() {
			_.each(this.$slidesContents, function(el) {
				$(el).css("height", "0");
			});

			_.each(this.$slides, function(el) {
				$(el).css("height", "auto");
				this.hideSlide($(el));
			}, this);
		},

		resizeView: function() {
			var currentState,
				windowWidth = $(window).width();

			if (windowWidth >= config.breakpoint) {
				this.setSlideDimensions();
				currentState = "desktop";
			} else {
				currentState = "mobile";
			}

			if (currentState !== state) {
				switch (currentState) {
					case "mobile":
						this.restoreSlideDimensions();
						break;
				}

				state = currentState;
			}
		},

		showSlide: function($slide) {
			var $slideContent = $slide.children(".accordion-content-wrapper");

			if (this.options.horizontal && state === "desktop") {
				$slide.addClass("is-active");
				$slide.animate({
					width: this.$el.outerWidth() - (this.$slides.size() - 1) * (config.slideWidth - 3)
				}, config.slideDuration);
			} else {
				$slideContent.css("height", 0);
				$slide.addClass("is-active");

				$slideContent.animate({
					height: $slideContent[0].scrollHeight
				}, config.slideDuration);
			}
		},

		hideSlide: function($slide) {
			var $slideContent = $slide.children(".accordion-content-wrapper");

			if (this.options.horizontal && state === "desktop") {
				$slide.animate({
					width: config.slideWidth
				}, config.slideDuration, function() {
					$slide.removeClass("is-active");
				});
			} else {
				$slideContent.animate({
					height: 0
				}, config.slideDuration, function() {
					$slide.removeClass("is-active");
				});
			}
		},

		bindUIEvents: function() {
			var self = this;

			this.$el.on("click", ".accordion-head", function() {
				var $slide = $(this).parents(".accordion-slide");

				if (self.options.exclusive) {
					_.each($slide.siblings(".is-active"), function(el) {
						self.hideSlide($(el));
					});
				}

				if (!$slide.hasClass("is-active")) {
					self.showSlide($slide);
				} else {
					self.hideSlide($slide);
				}
			});

			this.$el.on("click", ".accordion-head a", function(evt) {
				evt.preventDefault();
			});
		}
	};

	api.onRegister = function(scope) {
		var $el = scope.$scope,
			options = {};

		if ($el.hasClass("accordion-horizontal")) {
			options.horizontal = true;
			options.exclusive = true;
		}

		if ($el.hasClass("accordion-exclusive")) {
			options.exclusive = true;
		}

		new Accordion($el, options);
	};

	Cog.registerComponent({
		name: "accordion",
		api: api,
		selector: ".accordion"
	});

})(Cog.jQuery(), _);
