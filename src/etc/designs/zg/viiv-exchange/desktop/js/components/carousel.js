/**
 * Carousel
 */

/*global Modernizr, Hammer*/

(function($) {
	"use strict";

	var api = {};

	function Carousel($el, options) {
		this.options = options;

		this.$el = $el;
		this.$slideList = $el.find(".carousel-slides");
		this.$slideNav = $el.find(".carousel-nav");
		this.$slideControls = $el.find(".carousel-nav-item");
		this.$slides = this.$slideList.find(".carousel-slide");

		this.width = this.$slideList.width();
		$(window).resize(_.throttle(function() {
			this.width = this.$slideList.width();
		}.bind(this), 150));

		this.initialize();
	}

	Carousel.prototype = {
		initialize: function() {
			this.$slides.hide().first().show();

			if (this.options.delay > 0 && this.options.autoRotate) {
				this.timer = setTimeout(function() {
					this.showSlide(1);
				}.bind(this), this.options.delay * 1000);
			}

			this.bindUIEvents();

			if (!this.options.status.isLoaded()) {
				$(window).on("load", this.setHeight.bind(this));
			} else {
				this.setHeight();
			}
		},

		setHeight: function() {
			var maxHeight = 0;

			this.$el.css("height", "auto");

			if (this.options.status.isAuthor()) {
				return;
			}

			this.$slides.each(function() {
				var height = $(this).height();
				maxHeight = height > maxHeight ? height : maxHeight;
			});

			if (this.options.height && this.options.height <= maxHeight) {
				maxHeight = this.options.height;
			}

			if (this.$el.hasClass("carousel-side-arrows")) {
				if (Modernizr.csstransforms) {
					var transformHeight = maxHeight - 30;
					this.$el.find(".carousel-nav-item").each(function() {
						$(this).css({
							transform: "translateY(" + transformHeight + "px)",
							display: "inline-block"
						});
					});
				} else {
					this.$el.find(".carousel-nav-item").each(function() {
						$(this).css({
							top: "92%",
							display: "inline-block"
						});
					});
				}
			}

			this.$slideList.css("height", maxHeight);
			this.$slides.css("height", maxHeight);
		},

		showSlide: function(nextIndex) {
			clearTimeout(this.timer);

			var current = this.$slides.filter(".is-active"),
				dir = 1,
				currentIndex = current.index();

			nextIndex = (nextIndex < 0) ? (this.$slides.length - 1) : ((nextIndex >= this.$slides.length) ? 0 : nextIndex);
			dir = nextIndex - currentIndex;

			if (nextIndex !== currentIndex && !this.animating) {
				this.$slides.stop(true, true);
				this.animating = true;
				this.animate[this.options.transition].call(this, current, this.$slides.eq(nextIndex), dir);

				this.$slideControls.removeClass("is-active").eq(nextIndex).addClass("is-active");
			}

			if (this.options.delay > 0 && this.options.autoRotate) {
				this.timer = setTimeout(function() {
					this.showSlide(nextIndex + 1);
				}.bind(this), this.options.delay * 1000);
			}
		},

		animate: {
			fade: function(current, next) {
				var self = this;

				current.removeClass("is-active").fadeOut(function() {
					$(this).removeClass("is-active");
				});

				next.fadeIn(function() {
					$(this).addClass("is-active");
					self.animating = false;
				});
			},

			slide: function(current, next, direction) {
				var left = 0;

				current.css("z-index", "1");

				if (direction > 0) {
					next.show().css("left", this.width);
					left = -this.width;
				} else {
					next.show().css("left", -this.width);
					left = this.width;
				}

				this.$slideList.animate({left: left}, function() {
					this.$slideList.css("left", 0);
					next.css({
						"z-index": "2",
						"left": 0
					});
					current.hide();

					current.removeClass("is-active");
					next.addClass("is-active");

					this.animating = false;
				}.bind(this));
			}
		},

		bindUIEvents: function() {
			var hammer = new Hammer(this.$el[0], {
				touchAction: "pan-y"
			});

			this.$slideNav.on("click", "a", function(evt) {
				evt.preventDefault();

				var $parent = $(evt.target).parent(),
					index = this.$slideControls.index($parent);

				if ($parent.is(".carousel-nav-prev,.carousel-nav-next")) {
					index = this.$slides.filter(".is-active").index();
					index = $parent.hasClass("carousel-nav-prev") ? index - 1 : index + 1;
				}

				this.showSlide(index);
			}.bind(this));

			hammer.on("swipeleft swiperight", function(evt) {
				var index = this.$slides.filter(".is-active").index();

				index = (evt.type === "swipeleft") ? index - 1 : index + 1;

				this.showSlide(index);
			}.bind(this));
		}
	};

	api.onRegister = function(scope) {
		var $carousel = scope.$scope,
			$slides = $carousel.find(".carousel-slides");

		new Carousel($carousel, {
			height: $slides.data("height"),
			autoRotate: $slides.data("rotate"),
			delay: $slides.data("delay"),
			transition: $carousel.hasClass("transition-slide") ? "slide" : "fade",
			status: this.external.status
		});
	};

	Cog.registerComponent({
		name: "carousel",
		api: api,
		selector: ".carousel",
		requires: [
			{
				name: "utils.status",
				apiId: "status"
			}
		]
	});
})(Cog.jQuery());
