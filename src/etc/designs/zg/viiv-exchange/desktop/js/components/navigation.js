/**
 * Navigation
 */

(function($) {
	"use strict";

	var api = {};
	var slideDuration = 600;

	/**
	 * Nav class
	 * @param $nav navigation wrapper
	 * @constructor
	 */
	function Nav($nav) {
		this.$nav = $nav;
		this.bindUIEvents();
	}

	Nav.prototype.bindUIEvents = function() {
		this.$nav.on("click", ".navigation-mobile-menu", function(evt) {
			evt.preventDefault();
			var $nav = $(this).next(".navigation-root");
			$nav.slideToggle(600).toggleClass("is-open");
		});

		if (!this.$nav.is(".navigation-sidebar") && !this.$nav.is(".navigation-fat")) {
			this.$nav.on("click", ".navigation-item-decoration", function(evt) {
				evt.preventDefault();

				$(this).parent().siblings(".navigation-root")
					.stop(true)
					.slideToggle(slideDuration);
			});
		} else if (this.$nav.is(".navigation-sidebar")) {
			this.$nav.on("mouseenter mouseleave", ".has-children:not(.has-active-child)", function() {
				$(this).children(".navigation-root")
					.stop(true)
					.slideToggle(slideDuration);
			});

			this.$nav.on("touchend", ".navigation-item-decoration", function(evt) {
				$(this).parent().siblings(".navigation-root")
					.stop(true)
					.slideToggle(slideDuration);

				evt.preventDefault();
			});
		}
	};

	api.onRegister = function(scope) {
		new Nav(scope.$scope);
	};

	Cog.registerComponent({
		name: "navigation",
		api: api,
		selector: ".navigation"
	});

	return api;
}(Cog.jQuery()));
