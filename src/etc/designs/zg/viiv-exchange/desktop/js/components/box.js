/**
 * Box
 */

(function($, _) {
	"use strict";

	var api = {};

	function Box($el, options) {
		this.$el = $el;
		this.options = options;

		this.initialize();
	}

	Box.prototype = {
		initialize: function() {
			if (this.options.equalHeight) {
				$(window).on("load", this.makeEqualHeight.bind(this));
				$(window).on("resize", _.throttle(this.makeEqualHeight.bind(this), 150));
				this.makeEqualHeight();
			}
		},

		makeEqualHeight: function() {
			function getHeight(component) {
				return $(component).outerHeight();
			}

			var components = this.$el.find("> .component-content > .content").children(".component"),
				maxHeight = 0;

			_.each(components, function(component) {
				$(component).css("height", "auto");
			});

			maxHeight = _.max(_.map(components, getHeight));

			_.each(components, function(component) {
				$(component).css("height", maxHeight);
			});
		}
	};

	api.onRegister = function(scope) {
		var $el = scope.$scope,
			options = {};

		if ($el.hasClass("box-equal-height")) {
			options.equalHeight = true;
		}

		new Box($el, options);
	};

	Cog.registerComponent({
		name: "box",
		api: api,
		selector: ".box"
	});
}(Cog.jQuery(), _));